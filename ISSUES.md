# Known issues of the Skydreamer app
## 04/05/2017

### Swipe Cards (./src/components/containers/SwipeCardLayout.js)

- [ ] Get data from API for creating a card: price, photo, flight dates, nation name, location/event name, who liked it
- [ ] When the user clicks on the background image of the Swipe Cards section, he should be redirected to the gallery section (./src/components/containers/TravelAttractionFeed.js). That already happens, but the first Router.js' TabIcon at the bottom of the screen should remain selected
- [ ] The avatars in the bottom left of this section should be rendered one atop the other. Right now, the green border seem to overflow in a wrong manner
- [x] For some strange reason we don't seem to be able to add borderRadius to the image overlay in this section. Any help is really appreciated.
- [ ] Basing on the current image on the stack, the user should be redirected to the proper gallery section


### Profile/Buddies (./src/components/containers/ProfileLayout.js)

- [ ] Get data from API for showing the profile: name, location, age, tags, description, friends in group
- [ ] Show edit button if you are in your own profile, show invite to group and direct message if you are in another person profile
- [ ] We need to fix the general layout
- [x] We need to fix the style of the TopBar
- [ ] When the user clicks on "Settings", another screen pops up, which is fine, but for some strange reason it doens't get dismountend even if we call Actions.pop() in the onBackPress method of TopBarTitle, which is the TopBar of ProfileOptionsLayout.js. The same thing of the TabIcon that should remain selected applies also here.
- [ ] We should create a Buddies component with animated and linked circular bubbles which represent the avatars of other users. OpenGL or Lottie might be involved here.
- [ ] Click on group member to go to their profile

### ProfileOptionsLayout (./src/components/containers/ProfileOptionsLayout.js)
- [ ] Send search settings to the API
- [ ] Enable notification toggling for messages, match and booking

### LikedCards (./src/components/containers/LikedCardsLayout.js)
- [ ] Get data from API for creating the liked cards: photo, nation name, location/event name
- [ ] Fix bottom border issue
- [x] Remove heart icon
- [ ] Add link to the gallery of each location
- [ ] Delete card when clicking on x icon
- [ ] Send data to API when deleting card

### Gallery (./src/components/TravelAttractionFeed)
- [ ] Get data from API for creating a gallery element: title, photo, price ecc...

### Chat  
- [ ] Add active session switch to each chat
- [ ] Pin on top the active chat
- [ ] Get group data from API when switching session

### Search Options (./src/components/containers/SessionGradient.js)
- [ ] Fix layout issues
- [ ] Send the data to the API

### Match Screen (./src/components/containers/MatchLayout.js)
- [ ] Fix animation placement
- [ ] Fix layout issues
- [ ] Add actions to the button to keep swiping/ go to chat
- [ ] Send the data to the API

### Info layout (./src/components/containers/InfoLayout.js)
- [ ] Add parallax-ish effect for the map and description
- [ ] Fix the map
- [ ] Get directions data from google maps and render polyline over map

### Session Gradient (./src/components/containers/SessionGradient.js)
- [x] Send the session settings to the API
- [ ] We need to be able to render the Map in the background as per our mockup. We were already able to make the map scroll horizontally with a custom parallax-ish animation, but we didn't manage to figure out how to deal with its z-index. Basically the best we could do was making the map render over our blank text, which wasn't really noticeable, but still far from perfection.
- [ ] Before the user tries to click on the "next" button in the last screen, we should validate the groupname and show the user an error if it doesn't respect some parameters set by us (e.g.: number of chars < 3 etc.)
