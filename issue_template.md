When submitting an issue please make sure to do minimal debugging first, so you can provide some context on the issue that's being reported. Please make sure to use the proper type tag, (IE: "Bug", "Project") before submitting the task. Do NOT assign difficulty tags to your own issue. 

Please provide the following information:

 * Branch: (ie. layout)
 * Operating System: (ie. Android 6.0.0)
 * Date of issue: (ie. 5/21/2017)

If this issue is a bug, please provide a detailed description of how to reproduce the issue, as-well as any screenshots or video if required, as-well as a description of what is expected.

** Description Here **

------


FOR CONTRIBUTORS [Erase this]: 
 * DO NOT self-assign more than one task at any given time. 
 * DO NOT start a task without self assigning it.
 * DO NOT start on a task assigned to someone else.
 * DO NOT reassign a task without talking to the person assigned.
 * DO NOT work on anything other than the task assigned. Finish the task, then create a task for whatever you're going to work on next. Nothing should be done to the application without it being a task that you're assigned.
 
