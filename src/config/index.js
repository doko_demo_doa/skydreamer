/**
* @providesModule skydreamer/config
*/

export { default as colors } from './colors';
export { default as dims } from './dims';
export { default as config } from './config';
export { default as gradient } from './gradient';
