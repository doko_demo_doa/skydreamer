import React, { PropTypes } from 'react';
import Svg, {
  LinearGradient,
  Text,
  TextInput,
  Defs,
  Stop,
} from 'react-native-svg';
import {
  View,
} from 'react-native';
const Tags = ({ text, size, width, fontWeight, startColor, endColor, style }) => (
  <View style={style}>
    <Svg height={size} width={width}>
      <Defs>
        <LinearGradient id="grad" x1="0" y1="0" x2={width} y2="0">
          <Stop offset="0" stopColor={startColor} stopOpacity="1" />
          <Stop offset="1" stopColor={endColor} stopOpacity="1" />
        </LinearGradient>
      </Defs>
      <TextInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        placeholderStyle={{ fontFamily: 'Poppins-Regular', fontSize: 18 }}
        autoCorrect={false}
        style={inputStyle}
        value={value}
        onChangeText={onChangeText}
        placeholderTextColor={placeholderColor || 'white'}
        underlineColorAndroid={underlineColor || 'white'}
      />
      <Text fontSize={size} fill="url(#grad)" fonWeight={fontWeight} >{text}</Text>
    </Svg>
  </View>
);
GradientText.defaultProps = {
  style: {},
};
GradientText.propTypes = {
  text: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
  width: PropTypes.number.isRequired,
  fontWeight: PropTypes.number.isRequired,
  startColor: PropTypes.string.isRequired,
  endColor: PropTypes.string.isRequired,
  style: PropTypes.any,
};
export default Tags;
