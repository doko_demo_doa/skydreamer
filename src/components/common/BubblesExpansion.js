import React, { Component } from 'react';
import {
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    alignItems: 'center',
  },
  bubble: {
    position: 'absolute',
    flex: 1,
  },
});

/**
 * TODO: refactor it using Animated and scaling transform
 */
export default class BubblesExpansion extends Component {

  static propTypes = {
    top: PropTypes.number,
    color: PropTypes.string,
    numBubbles: PropTypes.number,
    maxDiameter: PropTypes.number,
    speed: PropTypes.number,
    duration: PropTypes.number,
  };

  static defaultProps = {
    top: 150,
    color: '#e61f16',
    numBubbles: 4,
    maxDiameter: 500,
    speed: 10,
    duration: 1000,
  }

  state = {
    bubbles: [],
  };

  componentDidMount() {
    const {
      numBubbles,
      duration,
      speed,
    } = this.props;

    for (i = 0; i < numBubbles; i++) {
      this.expandingBubblesTimeout = setTimeout(() => {
        this.createExpandingBubble(i);
      }, i * duration);
    }

    this.timer = setInterval(() => {
      this.updateExpandingBubble();
    }, speed);
  }

  /**
   * React complains if we don't manually stop the timeouts/interval before the
   * component is unmounted
   */
  componentWillUnmount() {
    clearTimeout(this.expandingBubblesTimeout);
    clearInterval(this.timer);
  }

  createExpandingBubble(key) {
    const { bubbles } = this.state;

    const bubble = {
      bubbleKey: bubbles.length + 1,
      diameter: 0,
      opacity: 0.5,
    };

    this.setState({
      bubbles: [...bubbles, bubble],
    });
  }

  updateExpandingBubble() {
    const {
      bubbles,
    } = this.state;
    const {
      maxDiameter,
    } = this.props;

    const newBubbles = bubbles.map(({ diameter }, i) => {
      const newDiameter = (diameter > maxDiameter ? 0 : diameter + 2);
      const centerOffset = (maxDiameter - newDiameter) / 2;
      const opacity = Math.abs((newDiameter / maxDiameter) - 1);

      const newBubble = {
        bubbleKey: i + 1,
        diameter: newDiameter,
        opacity: (opacity > 0.5 ? 0.5 : opacity),
        centerOffset,
      };

      return newBubble;
    });

    this.setState({
      bubbles: newBubbles,
    });
  }

  render() {
    const {
      bubbles,
    } = this.state;
    const {
      top,
      color,
      maxDiameter,
    } = this.props;

    return (
      <View style={[styles.container, { top }]}>
        <View
          style={[
            styles.bubbleContainer, {
              width: maxDiameter,
              height: maxDiameter,
            }]}
        >
          {bubbles.map(({ bubbleKey, diameter, opacity, centerOffset }) => (
            <View
              key={bubbleKey}
              style={[
                styles.bubble, {
                  backgroundColor: color,
                  width: diameter,
                  height: diameter,
                  borderRadius: diameter / 2,
                  top: centerOffset,
                  left: centerOffset,
                  opacity,
                },
              ]}
            />
          ))}
        </View>
      </View>
    );
  }
}
