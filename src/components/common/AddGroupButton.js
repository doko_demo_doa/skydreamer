import React from 'react';
import {
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    paddingHorizontal: 5,
  },
  icon: {
    backgroundColor: '#FDFFF8',
    width: 35,
    height: 35,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: '#aaa2c0',
    alignItems: 'center',
    justifyContent: 'center',

  },
});

const AddGroupButton = ({ onTouch, size }) => (
  <View style={styles.container}>
    <View style={styles.icon}>
      <Icon
        name="plus"
        size={15}
        color="#aaa2c0"
      />
    </View>
  </View>
  );

AddGroupButton.propTypes = {
  size: PropTypes.number,
};

AddGroupButton.defaultProps = {
  size: 15,
  withBorder: false,
};

export default AddGroupButton;
