import React, { Component, PropTypes } from 'react';
import styled from 'styled-components/native';
import { colors, dims } from 'app/config';
import { IconWrapper } from './';

const {
  background,
  idleIcon,
  selectedIcon,
} = colors.topMenu;

const Container = styled.View`
  height: 50;
  width: 100%;
  flex-direction: row;
  backgroundColor: ${colors.topMenu.background};
  alignItems: center;
  justifyContent: center;
`;

export default class TopMenu extends Component {

  static propTypes = {
    setSelectedLayout: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    console.log('TopMenu props', props);
  }

  state = {
    active: 0,
    nameList: [
      'plane',
      'envira',
      'bed',
      'cutlery',
    ],
  };

  onPress = (i) => {
    const { nameList } = this.state;
    console.log(`Pressed ${nameList[i]}`);
    this.setState({
      active: i,
    });
    this.props.setSelectedLayout(nameList[i]);
  }

  renderIconWrapper = () => {
    const {
      nameList,
      active,
    } = this.state;
    return (
      nameList.map((name, i) =>
        <IconWrapper
          key={`TopMenu_${i}`}
          name={name}
          onPress={() => this.onPress(i)}
          color={active === i ? selectedIcon : idleIcon}
        />,
      )
    );
  }

  render() {
    return (
      <Container>
        {this.renderIconWrapper()}
      </Container>
    );
  }
}
