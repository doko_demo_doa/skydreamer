import React, { PropTypes } from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styled from 'styled-components/native';

const iconSize = 30;

const IconTouchableContainer = styled(TouchableOpacity)`
  flex: 1;
`;

const IconContainer = ({ name, color, onPress }) => (
  <IconTouchableContainer onPress={onPress}>
    <Icon
      name={name}
      color={color}
      size={iconSize}
    />
  </IconTouchableContainer>
);

IconContainer.propTypes = {
  name: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default IconContainer;
