import React, { Component, PropTypes } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import {
  Select,
  Option,
} from 'react-native-chooser';
import Icon from 'react-native-vector-icons/FontAwesome';

import {
  Row,
} from 'skydreamer/components/common';
import Scaling from 'skydreamer/Scaling';


const styles = Scaling.newStylesheet({
  dropDownMenuContainer: {

  },
  dropDownMenu: {
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    width: 120,
    height: 35,
  },
  selector: {
    borderWidth: 0,
  },
});

export default class DropDownMenu extends Component {
  renderOptions = () =>
    this.props.options.map((option, i) =>
      <Option key={`dropDownOption${i}`}>{option}</Option>,
    );

  render() {
    const {
      options,
      defaultOption,
      onSelect,
      style,
    } = this.props;
    return (
      <View style={[styles.dropDownMenu, style]}>
        <Row>
          <Select
            style={styles.selector}
            defaultText={defaultOption}
            onSelect={onSelect}
          >
            {this.renderOptions()}
          </Select>
          <Icon
            name="angle-down"
          />
        </Row>

      </View>
    );
  }
}
