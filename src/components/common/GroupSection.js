import React, { PropTypes } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

import { Avatar } from 'skydreamer/components/common';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    margin: 10,
    flexDirection: 'row',
  },
});

const GroupSection = ({ avatarUris, style, individualStyle, size }) => {
  console.log('GroupSection');
  console.log(avatarUris, style, individualStyle, size);
  return (
    <View style={[styles.container, style]}>
      {
        avatarUris.map((uri, i) => (
          <Avatar
            key={i}
            uri={uri}
            size={size}
            style={individualStyle}
          />
        ))
      }
    </View>
  );
}

GroupSection.propTypes = {
  avatarUris: PropTypes.arrayOf(PropTypes.string).isRequired,
};
GroupSection.defaultProps = {
  size: 'small',
};

export default GroupSection;
