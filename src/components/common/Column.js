import React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  column: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
});

const Column = ({ style, children }) => (
  <View style={[styles.column, style]}>
    {children}
  </View>
);

Column.propTypes = {
  style: PropTypes.any,
  children: PropTypes.node.isRequired,
};

Column.defaultProps = {
  style: {},
};

export default Column;
