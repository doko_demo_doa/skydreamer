import React, { PropTypes } from 'react';
import {
  TouchableOpacity,
  View,
  Text,
} from 'react-native';
import styled from 'styled-components/native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from 'skydreamer/utils/icoMoonConfig.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);
import { Actions } from 'react-native-router-flux';
import { Row } from '../';
import LinearGradient from 'react-native-linear-gradient';

const Content = styled.View`
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 50;
  padding: 15;
  elevation: 10 ;
`;
import Scaling from 'skydreamer/Scaling';


const TouchableIcon = styled(TouchableOpacity)`
  position: absolute;
  top: 8;
  left: 10;
`;

const styleRow = {
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: 50,
};

const styles = Scaling.newStylesheet({
  text: {
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'Poppins',
    color: '#fff',
  },
});

const TopBarTitle = ({ onBackPress, title, colors, hideBack }) => (
  <Row style={[styleRow]}>
    <LinearGradient
      colors={colors}
      style={{ flex: 1 }}
    >
      <Content>
        {!hideBack &&
          <TouchableIcon
            onPress={onBackPress}
            activeOpacity={0.6}
          >
            <Icon
              name="navigate_before"
              size={35}
              color="#ffffff"
            />
          </TouchableIcon>
        }
        <Text style={styles.text}>{title.toUpperCase()}</Text>
      </Content>
    </LinearGradient>
  </Row>
);

TopBarTitle.propTypes = {
  onBackPress: PropTypes.func,
  title: PropTypes.string,
  hideBack: PropTypes.bool,
  styleContainer: View.propTypes.style,
};

TopBarTitle.defaultProps = {
  onBackPress: () => { Actions.pop(); },
  title: 'Title',
  colors: ['#EC504C', '#EC504C'],
  hideBack: false,
  styleContainer: { elevation: 8 },
};

export default TopBarTitle;
