import React, { Component, PropTypes } from 'react';
import { TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';
import { colors, dims } from 'skydreamer/config';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from 'skydreamer/utils/icoMoonConfig.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

const iconSize = 30;

const {
  background,
  idleIcon,
  selectedIcon,
} = colors.TopBarGallery;

const Container = styled.View`
  height: 50;
  width: 100%;
  flex-direction: row;
  background-color: ${background};
  align-items: center;
  justify-content: center;
`;

const IconTouchableContainer = styled(TouchableOpacity)`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const IconContainer = ({ name, color, onPress }) => (
  <IconTouchableContainer onPress={onPress}>
    <Icon
      name={name}
      color={color}
      size={iconSize}
    />
  </IconTouchableContainer>
);


export default class TopBarGallery extends Component {

  static propTypes = {
    setSelectedLayout: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    console.log('TopBarGallery props', props);
  }

  state = {
    active: 0,
    nameList: [
      'plane',
      'mountain-range',
      'local_hotel',
      'local_offer',
    ],
  };

  onPress = (i) => {
    const { nameList } = this.state;
    console.log(`Pressed ${nameList[i]}`);
    this.setState({
      active: i,
    });
    this.props.setSelectedLayout(nameList[i]);
  }

  renderIconWrapper = () => {
    const {
      nameList,
      active,
    } = this.state;
    return (
      nameList.map((name, i) => (
        <IconTouchableContainer
          onPress={() => this.onPress(i)}
          key={`TopBarGallery_${i}`}
        >
          <Icon
            name={name}
            color={active === i ? selectedIcon : idleIcon}
            size={iconSize}
          />
        </IconTouchableContainer>
      ))
    );
  }

  render() {
    return (
      <Container>
        {this.renderIconWrapper()}
      </Container>
    );
  }
}
