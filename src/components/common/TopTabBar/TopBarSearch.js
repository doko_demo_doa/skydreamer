import React, { PropTypes } from 'react';
import styled from 'styled-components/native';
import { Row } from '../';
import { noop } from 'skydreamer/utils';
import { Actions } from 'react-native-router-flux';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from 'skydreamer/utils/icoMoonConfig.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);

import {
  colors,
  dims,
} from 'skydreamer/config';
TouchableOpacity;
import {
  TouchableOpacity,
} from 'react-native';

const Container = styled.View`
  width: 100%;
  height: ${dims.topTabBar + 20};
  elevation: 8;
  padding: 15;
  position: absolute;
  top: 0;
  left: 0;
`;

const ArrowContainer = styled.View`
  flex: 1;
`;

const IconContainerLeft = styled.View`
  flex: 1;
  align-items: flex-start;
`;

const IconContainerRight = styled.View`
  flex: 1;
  align-items: flex-end;
`;


const TopBarSearch = ({ onBackPress, onSearchPress, onMenuPress }) => (
  <Container>
    <Row style={{ justifyContent: 'space-between' }}>
      <IconContainerLeft>
        <TouchableOpacity
          onPress={() => { Actions.searchOptions(); }}
        >
          <Icon
            name="slider"
            size={50}
            style={{ marginRight: 10, top: -10, color: '#020201', transform: [{ rotate: '-90deg' }] }}
          />
        </TouchableOpacity>
      </IconContainerLeft>
      <IconContainerRight>
        <Icon
          name="search"
          size={25}
          style={{ marginLeft: 10, color: '#020201' }}

        />
      </IconContainerRight>
    </Row>
  </Container>
);

TopBarSearch.propTypes = {
  onBackPress: PropTypes.func,
  onSearchPress: PropTypes.func,
  onMenuPress: PropTypes.func,
};

TopBarSearch.defaultProps = {
  onBackPress: noop,
  onSearchPress: noop,
  onMenuPress: noop,
};

export default TopBarSearch;
