import React, { PropTypes } from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import styled from 'styled-components/native';
import { Row } from '../';
import Scaling from 'skydreamer/Scaling';
import {
  colors,
  dims,
} from 'skydreamer/config';

const Container = styled.View`
  width: 100%;
  height: 50;
  position: absolute;
  top: 0;
  left: 0;
  background-color: #fbf8f7;
`;

const styles = Scaling.newStylesheet({
  tab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 3,
    borderLeftWidth: 0.5,
    borderColor: '#d9d6e8',
  },
  activeTab: {
    borderRightColor: '#d9d6e8',
    borderBottomColor: '#5cb94d',
    backgroundColor: colors.secondaryColor,
  },
  text: {
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'Poppins',
    color: '#000',
  },
  activeText: {
    color: '#fff',
  },
});

const TopBarProfileBuddies = ({ isOnProfile }) => (
  <Container>
    <Row>
      <TouchableOpacity
        style={[styles.tab, isOnProfile ? styles.activeTab : {}]}
        onPress={() => Actions.profile()}
      >
        <Text style={[styles.text, isOnProfile ? styles.activeText : {}]}>PROFILE</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.tab, !isOnProfile ? styles.activeTab : {}]}
        onPress={() => Actions.buddies()}
      >
        <Text style={[styles.text, !isOnProfile ? styles.activeText : {}]}>BUDDIES</Text>
      </TouchableOpacity>
    </Row>
  </Container>
);

TopBarProfileBuddies.propTypes = {
  isOnProfile: PropTypes.bool,
};

TopBarProfileBuddies.defaultProps = {
  isOnProfile: true,
};

export default TopBarProfileBuddies;
