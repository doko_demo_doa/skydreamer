import React from 'react';
import {
  View,
  Image,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    paddingHorizontal: 5,
  },
  tinyAvatar: {
    width: 25,
    height: 25,
    borderRadius: 26,
  },
  smallAvatar: {
    width: 35,
    height: 35,
    borderRadius: 26,
  },
  mediumAvatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  defaultAvatar: {
    width: 80,
    height: 80,
    borderRadius: 40,
  },
  border: {
    borderColor: 'red',
    borderWidth: 2,
  },
});

const Avatar = ({ uri, size, style = {}, withBorder }) => {
  console.log('uri@Avatar', uri);
  return (
    <View style={styles.container}>
      <View style={styles.avatar}>
        <Image
          style={[
            styles[`${size}Avatar`],
            withBorder && styles.border,
            style,
          ]}
          resizeMode="cover"
          source={{ uri }}
        />
      </View>
    </View>
  );
};

Avatar.propTypes = {
  uri: PropTypes.string.isRequired,
  size: PropTypes.oneOf([
    'tiny',
    'small',
    'default',
    'medium',
  ]),
  withBorder: PropTypes.bool,
};

Avatar.defaultProps = {
  size: 'default',
  withBorder: false,
};

export default Avatar;
