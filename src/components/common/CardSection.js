import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  containerStyle: {
    padding: 5,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    backgroundColor: 'transparent',
    position: 'relative',
  },
});

const CardSection = ({ children, style }) => (
  <View style={[styles.containerStyle, style]}>
    {children}
  </View>
);

CardSection.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CardSection;
