import React, { PropTypes } from 'react';
import {
  TouchableOpacity,
  Text,
  StyleSheet,
} from 'react-native';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  itemText: {
    color: '#fff',
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
  },
});

const ContextMenuItem = ({ title, onPress }) => (
  <TouchableOpacity onPress={onPress}>
    <Text style={styles.itemText}>{title}</Text>
  </TouchableOpacity>
);

ContextMenuItem.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default ContextMenuItem;
