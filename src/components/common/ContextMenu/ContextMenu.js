import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  View,
  Animated,
  Easing,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { colors, dims, gradient } from 'skydreamer/config';
import {
  ContextMenuDivider,
  ContextMenuItem,
  Triangle,
} from './';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  layout: {
    position: 'absolute',
    left: -7.5,
    right: 0,
    top: 15,
    width: 175,
  },
  triangleContainer: {
    top: 5,
  },
  itemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 5,
  },
});

export default class ContextMenu extends Component {

  static propTypes = {
    menu: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string,
      onPress: PropTypes.func,
    })),
  }

  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0.1);
  }

  state = {
    active: false,
    animated: false,
  };

  componentDidMount() {
    this.animate();
  }

  componentWillUnmount() {
    /* at this moment this is useless */
    this.reset();
  }

  animate = () => {
    Animated.timing(this.animatedValue, {
      toValue: 1,
      duration: 150,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(() => {
      this.setState({
        active: true,
      });
    });
  }

  onPressItem = (onPress) => {
    onPress();
    this.reset();
  }

  reset = () => {
    /* at this moment this is useless */
    Animated.timing(this.animatedValue, {
      toValue: 0,
      duration: 150,
      easing: Easing.bounce,
      useNativeDriver: true,
    }).start();
  }

  renderItems = () => {
    const { menu } = this.props;
    console.log('menu', menu);
    const items = menu.map((item, i) => {
      const { title, onPress } = item;
      if (i < menu.length - 1) {
        return (
          <View
            key={`ContextMenuItem${i}`}
            style={{ alignItems: 'center' }}
          >
            <ContextMenuItem
              title={title}
              onPress={onPress}
            />
            <ContextMenuDivider />
          </View>
        );
      }
      return (
        <ContextMenuItem
          key={`ContextMenuItem${i}`}
          title={title}
          onPress={onPress}
        />
      );
    });
    return items;
  }

  render() {
    const animatedStyle = {
      opacity: this.animatedValue,
    };
    const { buttonWarm } = colors.gradients;
    const { start, end } = gradient.buttonWarm;
    const Items = this.renderItems();
    console.log('Items', Items);

    return (
      <Animated.View>
        <Animated.View style={[styles.triangleContainer, animatedStyle]}>
          <Triangle
            direction="up"
            width={20}
            height={10}
            color={buttonWarm[2]}
          />
        </Animated.View>
        <Animated.View style={[styles.layout, animatedStyle]}>
          <LinearGradient
            colors={buttonWarm}
            start={start}
            end={end}
            style={styles.itemContainer}
          >
            {this.renderItems()}
          </LinearGradient >
        </Animated.View>
      </Animated.View>
    );
  }
}
