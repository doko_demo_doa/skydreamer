import React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import { colors } from 'skydreamer/config';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  divider: {
    backgroundColor: colors.contextMenu.divider,
    height: 2,
    width: '90%',
  },
});

const ContextMenuDivider = () => (
  <View style={styles.divider} />
);

export default ContextMenuDivider;
