import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  triangle: {
    width: 0,
    height: 0,
    backgroundColor: 'transparent',
    borderStyle: 'solid',
  },
});

export default class Triangle extends Component {
  static propTypes = {
    direction: PropTypes.oneOf(['up', 'right', 'down', 'left']),
    width: PropTypes.number,
    height: PropTypes.number,
    color: PropTypes.string,
    style: PropTypes.string,
  };

  static defaultProps = {
    direction: 'up',
    width: 0,
    height: 0,
    color: 'white',
  };

  borderStyles = () => {
    if (this.props.direction === 'up') {
      return {
        borderTopWidth: 0,
        borderRightWidth: this.props.width / 2.0,
        borderBottomWidth: this.props.height,
        borderLeftWidth: this.props.width / 2.0,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: this.props.color,
        borderLeftColor: 'transparent',
      };
    } else if (this.props.direction === 'right') {
      return {
        borderTopWidth: this.props.height / 2.0,
        borderRightWidth: 0,
        borderBottomWidth: this.props.height / 2.0,
        borderLeftWidth: this.props.width,
        borderTopColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: this.props.color,
      };
    } else if (this.props.direction === 'down') {
      return {
        borderTopWidth: this.props.height,
        borderRightWidth: this.props.width / 2.0,
        borderBottomWidth: 0,
        borderLeftWidth: this.props.width / 2.0,
        borderTopColor: this.props.color,
        borderRightColor: 'transparent',
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
      };
    } else if (this.props.direction === 'left') {
      return {
        borderTopWidth: this.props.height / 2.0,
        borderRightWidth: this.props.width,
        borderBottomWidth: this.props.height / 2.0,
        borderLeftWidth: 0,
        borderTopColor: 'transparent',
        borderRightColor: this.props.color,
        borderBottomColor: 'transparent',
        borderLeftColor: 'transparent',
      };
    }
    console.error(`Triangle wrong direction.
                  ${this.props.direction} is invalid.
                  Must be one of: ['up', 'right', 'down', 'left']`);
    return {};
  }

  render() {
    const borderStyles = this.borderStyles();
    return (
      <View style={[styles.triangle, borderStyles, this.props.style]} />
    );
  }
}
