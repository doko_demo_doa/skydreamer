import React, { PropTypes } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  title: {
    letterSpacing: 10, // iOS only for the moment
    fontSize: 14,
    textAlign: 'center',
    color: '#dd9b99',
  },
  activeTab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightColor: '#d9d6e8',
    borderBottomColor: '#5cb94d',
    borderBottomWidth: 2,
    borderRightWidth: 0.5,
  },
  inactiveTab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#d9d6e8',
    borderBottomWidth: 2,
    borderLeftWidth: 0.5,
  },
  activeText: {
    color: '#000000',
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'Poppins',
  },
  inactiveText: {
    color: '#c2c4db',
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'Poppins',
  },
  topRow: {
    height: 50,
    backgroundColor: '#fbf8f7',
    flexDirection: 'row',
  },
});

const TabBar = ({ active }) => (
  <View style={styles.topRow}>
    <TouchableOpacity style={styles.activeTab} onPress={this.onPressChat}>
      <Text style={styles.activeText}>PROFILE </Text>
    </TouchableOpacity>
    <TouchableOpacity style={styles.inactiveTab} onPress={this.onPressSwipeMore}>
      <Text style={styles.unactiveText}>BUDDIES</Text>
    </TouchableOpacity>
  </View>

);

TabBar.propTypes = {
  active: PropTypes.string.isRequired,
};

export default TabBar;
