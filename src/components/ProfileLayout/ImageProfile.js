

import React, { PropTypes } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
} from 'react-native';
import {
  colors,
  dims,
} from 'skydreamer/config';
import Scaling from 'skydreamer/Scaling';

import LinearGradient from 'react-native-linear-gradient';

const styles = Scaling.newStylesheet({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  profilePhoto: {
    paddingTop: 290,
    width: dims.SCREEN_WIDTH,
    height: dims.SCREEN_HEIGHT / 2,
  },
  groupContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  text: {
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#fff',
    paddingLeft: 30,
  },
  country: {
    fontSize: 16,
  },
  city: {
    fontSize: 36,
    fontWeight: 'bold',
  },
  linearGradient: {
    flex: 1,
  },
});

const ImageProfile = ({ image }) => (
  <View style={styles.container}>
    <LinearGradient
      colors={['rgba(0,0,0,0)', 'rgba(0,0,0,1)']}
      style={styles.linearGradient}
    >
      <Image
        style={styles.profilePhoto}
        source={{ uri: image }}
        resizeMode="cover"
      />
    </LinearGradient>
  </View>
);

ImageProfile.propTypes = {
  image: PropTypes.string.isRequired,
};

export default ImageProfile;
