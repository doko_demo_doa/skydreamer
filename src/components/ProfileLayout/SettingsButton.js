import React, { PropTypes } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  settingsContainer: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingVertical: 8,
    flexDirection: 'row',
  },
  settings: {
    fontFamily: 'Poppins',
    fontSize: 15,
    color: '#39b25b',
  },
});

const SettingsButton = ({ style, onPress }) => (
  <TouchableOpacity
    onPress={onPress}
    style={[styles.settingsContainer]}
  >
    <Icon
      name="gear"
      size={15}
      style={{ top: 2, paddingHorizontal: 5 }}
      color="#39b25b"
    />
    <Text style={styles.settings}>Settings</Text>

  </TouchableOpacity>
);

SettingsButton.propTypes = {
  style: PropTypes.object,
  onPress: PropTypes.func.isRequired,
};

SettingsButton.defaultProps = {
  style: {},
};

export default SettingsButton;
