import React, { PropTypes } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  toolbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
    backgroundColor: '#fbf8f7',
  },
});

const ToolBar = () => (
  <View style={styles.toolbar}>
    <TouchableOpacity
      onPress={() => Alert.alert(
        'Info',
        info,
      )}
    >
      <Icon
        name="cog"
        color="#a19fae"
        size={20}
      />
    </TouchableOpacity>
    <TouchableOpacity
      onPress={() => Alert.alert(
        'Info',
        info,
      )}
    >
      <Icon
        name="paper-plane-o"
        color="#a19fae"
        size={20}
      />
    </TouchableOpacity>
  </View>
);

ToolBar.propTypes = {
  active: PropTypes.string.isRequired,
};

export default ToolBar;
