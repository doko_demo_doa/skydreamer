import React, { PropTypes } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from 'skydreamer/utils/icoMoonConfig.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);
import LinearGradient from 'react-native-linear-gradient';
import Scaling from 'skydreamer/Scaling';
const gradientStart = { x: 0, y: 0 };
const gradientEnd = { x: 1, y: 1 };

const styles = Scaling.newStylesheet({
  infoContainer: {
    padding: 5,
    borderRadius: 200,
  },
});

const ActionButton = ({ color, size, style, onPress, icon, colorStart, colorEnd }) => (
  <LinearGradient colors={[colorStart, colorEnd]} start={gradientStart} end={gradientEnd} style={[styles.infoContainer, style]}>
    <TouchableOpacity onPress={onPress} style={{ padding: 6}}>
      <Icon
        name={icon}
        size={size}
        color={color}
      />
    </TouchableOpacity>
  </LinearGradient>

);

ActionButton.propTypes = {
  color: PropTypes.string.isRequired,
  colorStart: PropTypes.string.isRequired,
  colorEnd: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
  style: PropTypes.object,
  onPress: PropTypes.func.isRequired,
};

ActionButton.defaultProps = {
  style: {},
  icon: 'pencil',
  colorStart: '#39b25b',
  colorEnd: '#87e72a',


};

export default ActionButton;
