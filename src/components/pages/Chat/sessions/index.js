import React from 'react';
import ListItem from './ListItem';
import { connect } from 'react-redux';
import { TopBarSearch } from '../../../common/TopTabBar';
import { dims } from 'skydreamer/config';
import deepEqual from 'deep-equal';
import {
    FlatList,
    View,
    Dimensions,
} from 'react-native';

const { width, height } = Dimensions.get('window');

class SessionList extends React.Component {
  static renderNavigationBar = props => (
    <TopBarSearch />
    )

  constructor(props) {
    super(props);
    this.convertSessions = this.convertSessions.bind(this);
    const sessions = this.convertSessions(props.sessions);
    this.state = { sessions: sessions || [] };
  }

  componentWillReceiveProps(nextProps) {
    const newSessions = this.convertSessions(nextProps.sessions);
    if (newSessions) this.setState({ sessions: newSessions });
  }

  convertSessions(sessions) {
    if (this._cache == null || !deepEqual(this._cache, sessions)) {
      this._cache = sessions;

      const arr = [];
      for (key in sessions) {
        const session = sessions[key];
        if (!session) continue;
        arr.push({ ...session, key });
      }

      return arr.sort((a, b) => {
        const aValue = a.lastMessage ? a.lastMessage.timestamp : a.created_at;
        const bValue = b.lastMessage ? b.lastMessage.timestamp : b.created_at;
        return (a - b) * -1;
      });
    }

    return null;
  }

  render() {
    const { cursors, messages } = this.props;
    const { sessions } = this.state;

    let _height = height;
    _height -= dims.topTabBar;
    _height -= 50; // Bottom tab bar height
    _height -= 50; // Additional padding

    return (
      <View style={{ flex: 1, backgroundColor: '#fff8f7', paddingTop: dims.topTabBar + 10 }}>
        <View elevation={1} style={{ backgroundColor: 'white', borderRadius: 8, marginHorizontal: 20, height: _height }}>
          <FlatList
            data={sessions}
            renderItem={({ item }) => (
              <ListItem session={item} cursor={cursors[item.key]} messages={messages[item.key]} dispatch={this.props.dispatch} />
                        )}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  sessions: state.channels.channels,
  cursors: state.channels.channelCursors,
  messages: state.channels.channelMessages,
});

export default connect(
    mapStateToProps,
)(SessionList);
