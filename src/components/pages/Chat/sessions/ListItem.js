import React from 'react';
import { channelActions } from 'skydreamer/actions';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
} from 'react-native';

export default class ListItem extends React.Component {

  openChat = (key, title, folder, photo, lastLogin) => {
    const { messages, cursor } = this.props;

    if (!messages || messages.length === 0) {
      console.warn('Refreshing chat:', messages);
      this.props.dispatch(channelActions.fetchMessagesFirebase(key, cursor));
    }

    this.props.dispatch(channelActions.openChat(key, title, folder, photo, lastLogin));
  }


  render() {
    const { session } = this.props;
    const { photo, type, key, first_name, last_name, group_name, lastMessage } = session;
    const { objData } = lastMessage;

    const previewText = objData.value || '';

    const title = type === 'single' ? (`${first_name} ${last_name}`) : group_name;
    const folder = type === 'single' ? 'profile' : 'groups';

    return (
      <TouchableOpacity style={styles.container} onPress={() => this.openChat(key, title, folder, photo, 0)}>
        <View style={styles.avatarContainer}>
          <Image source={{ uri: `https://storage.skydreamer.io/${folder}/${photo}` }} style={styles.avatarImage} />
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.nameLabel}>{title}</Text>
          <Text numberOfLines={1} style={styles.previewLabel}>{previewText}</Text>
        </View>
        <View style={styles.actionContainer}>
          <Text style={styles.timeLabel}>12:30 PM</Text>
          <Text>...</Text>
        </View>
      </TouchableOpacity>
    );
  }
}


const styles = {
  container: {
    height: 80,
    marginTop: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
  },
  avatarContainer: {
    height: 80,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatarBorder: {
    width: 70,
    height: 70,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatarImage: {
    width: 64,
    height: 64,
    borderRadius: 32,
    resizeMode: 'cover',
  },
  actionContainer: {
    width: 80,
    height: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
  timeLabel: {
    paddingBottom: 8,
    fontSize: 10,
    color: '#b6b6b6',
  },
  infoContainer: {
    paddingHorizontal: 6,
    flex: 1,
    justifyContent: 'center',
  },
  previewLabel: {
    color: '#b6b6b6',
    fontSize: 14,
  },
  nameLabel: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black',
  },
};
