export * from './HolderSelfText';
export * from './HolderOtherText';
export * from './HolderOtherPhoto';
export * from './HolderSelfPhoto';
export * from './HolderOtherTyping';
