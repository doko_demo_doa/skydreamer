import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class HolderOtherTyping extends Component {

  render() {
    const { rowContainer, textViewStyle, textStyle } = styles;

    return (
      <View style={rowContainer}>
        <View style={{ flex: 0.15 }} />
        <View style={textViewStyle}>
          <Text style={[textStyle, { borderRadius: 50 }]}>
            <Icon name="ios-more" size={34} color="#EC514D" />
          </Text>
        </View>
      </View>
    );
  }
}

const styles = {
  onlineUserSign: {
    backgroundColor: '#51CA31',
    width: 8,
    height: 8,
    borderRadius: 50,
    position: 'absolute',
    right: 8,
    top: 5,
    borderColor: '#FFF8F6',
    borderWidth: 1,
  },
  profileImage: {
    backgroundColor: 'transparent',
    alignSelf: 'center',
    marginTop: 2,
    width: 34,
    height: 34,
    borderRadius: 50,
  },
  textStyle: {
    fontSize: 15,
    fontFamily: 'Poppins-Light',
    backgroundColor: '#FBE9E7',
    padding: 2,
    paddingLeft: 15,
    paddingRight: 15,
    flexWrap: 'wrap',
    color: '#1C1918',
  },
  textViewStyle: {
    flex: 0.85,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginRight: 20,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    flexWrap: 'wrap',
  },
};

export { HolderOtherTyping };
