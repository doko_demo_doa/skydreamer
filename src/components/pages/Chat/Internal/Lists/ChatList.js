// TODO: #11 - remove ListView
import React, { Component } from 'react';
import { RefreshControl, View, TouchableOpacity, ListView, Text, Alert } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Spinner } from '../../../../common';
import { channelActions } from 'skydreamer/actions';
import firebase from 'skydreamer/firebase';

import {
  HolderOtherText,
  HolderSelfText,
  HolderOtherPhoto,
  HolderSelfPhoto,
  HolderOtherTyping,
} from '../ViewHolder';

let authUserId = null;

const ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2,
});

class ChatList extends Component {

  constructor(props) {
    super(props);
    this.sortMessages = this.sortMessages.bind(this);
    this.renderMessage = this.renderMessage.bind(this);
    this.fetchPreviousMessages = this.fetchPreviousMessages.bind(this);

    const user = firebase.auth().currentUser;
    if (user != null) {
      authUserId = user.uid;
    }


    this.state = {
      refreshing: false,
      rendered: false,
      listViewHeight: 0,
      messages: this.sortMessages(props),
    };

    this.state.dataSource = ds.cloneWithRows(this.state.messages);
  }

  componentWillReceiveProps(nextProps) {
    const messages = this.sortMessages(nextProps);
    if (this._lastMessages != null && messages.length === 0) return;
    this._lastMessages = messages;
    this.setState({ messages, dataSource: ds.cloneWithRows(messages), refreshing: nextProps.isLoadingMessages });
  }

  sortMessages(props) {
    return props.messages.sort((a, b) => a.timestamp - b.timestamp);
  }

  fetchPreviousMessages() {
    this.props.isFullySynced;
    const { channelKey, isLoadingMessages, isFullySynced, messageCursor } = this.props;
    if (isLoadingMessages) return;
    if (isFullySynced) return;
    this.setState({ refreshing: true });
    this.props.dispatch(channelActions.fetchMessagesFirebase(channelKey, messageCursor));
  }

    /*
    renderMessage(message) {
        return <Text>{message.objData.value}</Text>
    }
    */

  renderMessage(message, sectionID, index) {
    const { uid } = firebase.auth().currentUser;
    const { messages } = this.state;
    const { usersTyping } = this.props;
    let isUsersTyping = false;
    for (key in usersTyping) {
      if (isUsersTyping) continue;
      if (key === uid) continue;
      const value = usersTyping[key];
      if (value == null) continue;
      isUsersTyping = value;
      if (value) break;
    }

      // Alert.alert(JSON.stringify(messages, null, 2));
      // Alert.alert(typeof messages);
      // Alert.alert(JSON.stringify(messages));

    let hiddenProfile = false;
    let moreSpace = false;
    let squareCorner = false;
    let semiSquareCornerUp = false;
    let semiSquareCornerDown = false;

    const previus = messages[Number(index) - 1];
    const current = messages[Number(index)]; // message;
    const next = messages[Number(index) + 1];

      // return <Text>{JSON.stringify(current) + `   ------------------------------------    `}</Text>

      /*
      console.log('renderRow: previus:::', previus);
      console.log('renderRow: current:::', current);
      console.log('renderRow: next:::', next);
      */
    if (previus !== undefined && next !== undefined) {
      if ((current.userid !== previus.userid) && (current.userid === next.userid)) {
        semiSquareCornerDown = true;
      } else if ((current.userid === previus.userid) && (current.userid === next.userid)) {
        squareCorner = true;
      } else if ((current.userid === previus.userid) && (current.userid !== next.userid)) {
        semiSquareCornerUp = true;
      }

      if (current.userid !== previus.userid) {
        moreSpace = true;
      }
    }

    if (previus !== undefined) {
      if (current.userid === previus.userid) {
        hiddenProfile = true;
      }

      if (next === undefined) {
        if (current.userid === previus.userid) {
          semiSquareCornerUp = true;
        }
      }
    } else if (next !== undefined) {
      if (current.userid === next.userid) {
        semiSquareCornerDown = true;
      }
    }

    if (next === undefined && previus !== undefined) {
      if (current.userid !== previus.userid) {
        moreSpace = true;
      }
    }

    const { objData, timestamp } = message;
    const photo = `${current.userid}.jpg`;

    console.log('type@objData', type);

    const { type, value } = objData;
    const id = index;

    let typeMessage = null;
    let titleStr = null;
    let subtitleStr = null;

    if (type === 'text') {
      if (authUserId === current.userid) {
        typeMessage = 'SelfText';
      } else {
        typeMessage = 'OtherText';
      }
    } else if (type === 'photo') {
      const { title, subtitle } = objData;
      titleStr = title;
      subtitleStr = subtitle;

      if (authUserId === current.userid) {
        typeMessage = 'SelfPhoto';
      } else {
        typeMessage = 'OtherPhoto';
      }
    }
      // SelfPhoto

    const getComponent = () => {
      switch (typeMessage) {
        case 'DateSeparator':
          return (
            <HolderOtherText
              id={id}
              photoSrc={photo}
              timestamp={timestamp}
              text={value}
              hiddenProfile={hiddenProfile}
              moreSpace={moreSpace}
              squareCorner={squareCorner}
              semiSquareCornerUp={semiSquareCornerUp}
              semiSquareCornerDown={semiSquareCornerDown}
              key={index}
            />
          );
        case 'OtherText':
          return (
            <HolderOtherText
              id={id}
              photoSrc={photo}
              timestamp={timestamp}
              text={value}
              hiddenProfile={hiddenProfile}
              moreSpace={moreSpace}
              squareCorner={squareCorner}
              semiSquareCornerUp={semiSquareCornerUp}
              semiSquareCornerDown={semiSquareCornerDown}
              key={index}
            />
          );
        case 'SelfText':
          return (
            <HolderSelfText
              id={id}
              photoSrc={photo}
              timestamp={timestamp}
              text={value}
              hiddenProfile={hiddenProfile}
              moreSpace={moreSpace}
              squareCorner={squareCorner}
              semiSquareCornerUp={semiSquareCornerUp}
              semiSquareCornerDown={semiSquareCornerDown}
              key={index}
            />
          );
        case 'SelfPhoto':
          return (
            <HolderSelfPhoto
              id={id}
              photoSrc={photo}
              timestamp={timestamp}
              url={value}
              title={titleStr}
              subtitle={subtitleStr}
              hiddenProfile={hiddenProfile}
              moreSpace={moreSpace}
              squareCorner={squareCorner}
              semiSquareCornerUp={semiSquareCornerUp}
              semiSquareCornerDown={semiSquareCornerDown}
              key={index}
            />
          );
        case 'OtherPhoto':
          return (
            <HolderOtherPhoto
              id={id}
              photoSrc={photo}
              timestamp={timestamp}
              url={value}
              title={titleStr}
              subtitle={subtitleStr}
              hiddenProfile={hiddenProfile}
              moreSpace={moreSpace}
              squareCorner={squareCorner}
              semiSquareCornerUp={semiSquareCornerUp}
              semiSquareCornerDown={semiSquareCornerDown}
              key={index}
            />
          );
        default:
          return {};
      }
    };


    if (index == 0 && messages.length > 20 && !this.props.isFullySynced) {
      return (<View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ fontSize: 12, color: '#7a7a7a', marginVertical: 15 }}>PULL DOWN TO LOAD OLDER MESSAGES</Text>
        { getComponent() }
      </View>);
    } else if (index == 0) {
      return (<View>
        <View style={{ marginTop: 30 }} />
        { getComponent() }
      </View>);
    } else if (index == messages.length - 1 && isUsersTyping) {
      return (<View>
        <View>
          { getComponent() }
        </View>
        <View>
          <HolderOtherTyping />
        </View>
      </View>);
    }
    return getComponent();

      //* /
  }

  render() {
    if (this.props.isInitialLoading) {
      return <Spinner />;
    }
    const { messages } = this.state;
    return (<ListView
      enableEmptySections
      ref="messageList"
      style={{ flex: 1 }}
      onLayout={(event) => {
        const { y } = event.nativeEvent.layout;
        this.setState({
          listViewHeight: y,
        });
      }}
      onContentSizeChange={() => {
        this.refs.messageList.scrollTo({ y: this.refs.messageList.getMetrics().contentLength - this.state.listViewHeight });
      }}
      dataSource={this.state.dataSource}
      renderRow={(message, sectionID, index) => this.renderMessage(message, sectionID, index)}
      refreshControl={
        <RefreshControl refreshing={this.state.refreshing} onRefresh={this.fetchPreviousMessages} />
          }
    />);
  }
}

const mapStateToProps = state => ({
  messages: state.channels.channelMessages[state.channels.selected] || [],
  usersTyping: state.channels.channels[state.channels.selected].users_writing || {},
  isInitialLoading: state.channels.channelsFetching[state.channels.selected] || false,
  isLoadingMessages: state.channels.channelsLoading[state.channels.selected] || false,
  isFullySynced: state.channels.channelsSynced[state.channels.selected] || false,
  messageCursor: state.channels.channelCursors[state.channels.selected] || null,
});


export default connect(mapStateToProps)(ChatList);
