import React, { PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';
import Scaling from 'skydreamer/Scaling';
import {
  GradientText,
  GroupSection,
} from 'skydreamer/components/common';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from 'skydreamer/utils/icoMoonConfig.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);
const styles = Scaling.newStylesheet({
  container: {
    flex: 1,
    paddingBottom: 10,
  },
  iconsRow: {
    padding: 15,
    flex: 1,
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  groupContainer: {
    margin: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  nation: {
    fontSize: 16,
    color: '#FFF',
    fontFamily: 'Poppins-SemiBold',
  },
  location: {
    fontSize: 25,
    fontFamily: 'Poppins-SemiBold',
    color: '#FFF',
  },
  textContainer: {
    paddingLeft: 15,
    paddingBottom: 5,
  },
  image: {
    height: 160,
  },
});

const onRemoveCardPressed = (removeCard, item) => {
  removeCard(item);
};

const LikedCard = (item) => {
  const { id, location, nation, image, text, color, primaryGradient, secondaryGradient, removeCard, avatarUris } = item;
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.container}
        activeOpacity={0.6}
        onPress={() => Actions.gallery()}
      >
        <Image
          style={styles.image}
          source={{ uri: image }}
          resizeMode="cover"
          borderRadius={10}
        >
          <LinearGradient
            colors={['rgba(0,0,0,0)', 'rgba(0,0,0,0.5)']}
            style={[{ borderBottomLeftRadius: 10, borderBottomRightRadius: 10, flex: 1 }]}
          >
            <View style={styles.iconsRow}>
              <TouchableOpacity onPress={() => onRemoveCardPressed(removeCard, item)}>
                <Icon
                  name="close"
                  size={25}
                  color="#ffffff"
                  style={styles.dismiss}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.nation}>{nation}</Text>
              <Text style={styles.location}>{location}</Text>
              <GroupSection
                style={styles.groupContainer}
                size="tiny"
                avatarUris={avatarUris}
              />
            </View>
          </LinearGradient>
        </Image>
      </TouchableOpacity>
    </View>
  );
};

LikedCard.propTypes = {
  location: PropTypes.string.isRequired,
  nation: PropTypes.string.isRequired,
  image: PropTypes.string,
  avatarUris: PropTypes.arrayOf(PropTypes.string).isRequired,
  primaryGradient: PropTypes.string,
  secondaryGradient: PropTypes.string,
  removeCard: PropTypes.func,
};

export default LikedCard;
