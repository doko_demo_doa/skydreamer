import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Actions } from 'react-native-router-flux';
import Scaling from 'skydreamer/Scaling';
import {
  GradientText,
  GroupSection,
  Icon,
} from 'skydreamer/components/common';

const styles = Scaling.newStylesheet({
  iconsRow: {
    flex: 1,
    padding: 10,
    justifyContent: 'flex-end',
    flexDirection: 'row',
  },
  groupContainer: {
    margin: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  nation: {
    fontSize: 16,
    color: '#FFF',
    fontFamily: 'Poppins-SemiBold',
  },
  text: {
    fontSize: 16,
    color: '#FFF',
    fontFamily: 'Poppins-SemiBold',
  },
  location: {
    fontSize: 25,
    fontFamily: 'Poppins-SemiBold',
    color: '#FFF',
  },
  textContainer: {
    paddingLeft: 15,
    paddingBottom: 5,
  },
  image: {
    borderRadius: 10,
  },
  tag: {
    borderBottomRightRadius: 5,
    borderTopLeftRadius: 10,
    alignItems: 'center',
    marginLeft: 250,
  },
});

const onRemoveCardPressed = (removeCard, item) => {
  removeCard(item);
};

const MatchedCard = (item) => {
  const { id, location, nation, image, text, color, primaryGradient, secondaryGradient, removeCard } = item;
  return (
    <View style={{ flex: 1, paddingBottom: 20 }}>
      <TouchableOpacity
        style={styles.container}
        activeOpacity={0.6}
        onPress={() => Actions.resume()}
      >
        <LinearGradient
          colors={[primaryGradient, secondaryGradient]}
          style={{ borderRadius: 10, padding: 5 }}
        >
          <Image
            style={styles.image}
            source={{ uri: image }}
            resizeMode="cover"
            borderRadius={10}
          >
            <LinearGradient
              colors={['rgba(0,0,0,0)', 'rgba(0,0,0,0.5)']}
              style={{ borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}
            >
                <View style={styles.iconsRow}>
                  <TouchableOpacity onPress={() => onRemoveCardPressed(removeCard, item)}>
                    <Icon
                      name="close"
                      size={25}
                      color="#ffffff"
                      style={styles.dismiss}
                    />
                  </TouchableOpacity>
                </View>
              <View style={styles.textContainer}>
                <Text style={styles.nation}>{nation}</Text>
                <Text style={styles.location}>{location}</Text>
              </View>
              <View style={{ backgroundColor: color, borderBottomRightRadius: 5, borderTopLeftRadius: 10, alignItems: 'center', marginLeft: 250 }}>
                <Text style={styles.text}>{text}</Text>
              </View>
            </LinearGradient>
          </Image>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
};

export default MatchedCard;
