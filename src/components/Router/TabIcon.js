import React, { Component, PropTypes } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Animated,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import swipeIconSelected from 'skydreamer/images/navBar/swipeActive.png';
import cardIconSelected from 'skydreamer/images/navBar/likedCardsActive.png';
import chatIconSelected from 'skydreamer/images/navBar/chatActive.png';
import groupIconSelected from 'skydreamer/images/navBar/groupActive.png';

import swipeIcon from 'skydreamer/images/navBar/swipe.png';
import cardIcon from 'skydreamer/images/navBar/likedCards.png';
import chatIcon from 'skydreamer/images/navBar/chat.png';
import groupIcon from 'skydreamer/images/navBar/group.png';

const styles = {
  icon: {
    width: 28,
    height: 28,
    resizeMode: 'contain',
    marginBottom: 8
  },
  textStyle: {
    color: '#fff',
    alignSelf: 'center',
    fontFamily: 'NotoSans-Light',
    fontSize: 12,
  },
  textActiveStyle: {
    color: '#fff',
    alignSelf: 'center',
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 12,
  },
  bubble: {
    width: 25,
    height: 25,
    backgroundColor: '#FF6D00',
    position: 'absolute',
    borderWidth: 1.5,
    borderColor: '#fff',
    borderRadius: 30,
    right: 7,
    marginTop: 5,
    elevation: 8,
    transform: [{ scale: this.springValue }],
  },
  bubbleText: {
    color: '#fff',
    alignSelf: 'center',
    fontFamily: 'NotoSans-SemiBold',
    fontSize: 14,
    marginTop: 0,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20
  },
};

export default class TabIcon extends Component {

  static propTypes = {
    notificationNumber: PropTypes.number,
    iconName: PropTypes.string.isRequired,
  };

  springValue = new Animated.Value(0.3);

  componentDidMount() {
    this.spring();
  }
  renderImage() {
    const {
      selected,
      iconName,
    } = this.props;

    if (selected) {
      switch (iconName) {
        case 'swipe':
          return (
            <Image style={styles.icon} source={swipeIconSelected} />
          );
        case 'card':
          return (<Image style={styles.icon} source={cardIconSelected} />);
        case 'chat':
          return (<Image style={styles.icon} source={chatIconSelected} />);
        case 'group':
          return (<Image style={styles.icon} source={groupIconSelected} />);
      }
    } else {
      switch (iconName) {
        case 'swipe':
          return (<Image style={styles.icon} source={swipeIcon} />);
        case 'card':
          return (<Image style={styles.icon} source={cardIcon} />);
        case 'chat':
          return (<Image style={styles.icon} source={chatIcon} />);
        case 'group':
          return (<Image style={styles.icon} source={groupIcon} />);
      }
    }
  }

  setStyle = (parent) => {
    const { text } = this.props;
    const { iconActiveStyle, iconStyle } = styles;
    return (
      parent.state.selected === text ?
        iconActiveStyle :
        iconStyle
    );
  }

  setTextStyle = (parent, text) => {
    const { textActiveStyle, textStyle } = styles;
    if (parent.state.selected === text) {
      return textActiveStyle;
    }
    return textStyle;
  }

  setBubbleStyle = (num) => {
    if (num > 99) {
      return {
        width: 38,
      };
    }
    return {};
  }

  setBubbleMarginStyle = (parent, text) => {
    if (parent.state.selected !== text) {
      return { marginTop: 10 };
    }
    return {};
  }

  spring = () => {
    this.springValue.setValue(1.5);
    Animated.spring(
      this.springValue,
      {
        toValue: 1,
        friction: 1,
      },
    ).start();
  }

  render() {
    const {
      selected,
      notificationNumber,
      iconName,
      borderColor,
    } = this.props;
    const { bubbleText } = styles;

    return (
      <View style={[styles.container, selected ? { borderBottomWidth: 3, borderColor } : {}]}>
        {this.renderImage()}
        {this.props.notificationNumber > 0 &&
          <Animated.View
            style={[
              {
                width: 20,
                height: 20,
                backgroundColor: '#EC514C',
                position: 'absolute',
                borderRadius: 30,
                right: 15,
                elevation: 3,
                shadowColor: '#000',
                shadowOffset: { width: 2, height: 2 },
                shadowOpacity: 0.2,
                marginTop: 5,
                transform: [{ scale: this.springValue }],
              },
              this.setBubbleStyle(notificationNumber),
            ]}
          >
            <Text style={bubbleText}>{notificationNumber}</Text>
          </Animated.View>
        }
      </View>
    );
  }
}
