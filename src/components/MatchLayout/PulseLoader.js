import React, { Component } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native';
import { dims } from 'skydreamer/config';
import Pulse from './Pulse';

export default class PulseLoader extends Component {

  static propTypes = {
    interval: React.PropTypes.number,
    size: React.PropTypes.number,
    pulseMaxSize: React.PropTypes.number,
    avatar: React.PropTypes.string.isRequired,
    avatarBackgroundColor: React.PropTypes.string,
    pressInValue: React.PropTypes.number,
    pressDuration: React.PropTypes.number,
    borderColor: React.PropTypes.string,
    backgroundColor: React.PropTypes.string,
    getStyle: React.PropTypes.func,
  };

  static defaultProps = {
    interval: 2000,
    size: dims.SCREEN_WIDTH * 0.75,
    pulseMaxSize: dims.SCREEN_WIDTH * 1.75,
    avatar: undefined,
    avatarBackgroundColor: 'white',
    pressInValue: 0.8,
    pressDuration: 150,
    pressInEasing: Easing.ease,
    pressOutEasing: Easing.ease,
    borderColor: '#D8335B',
    backgroundColor: '#ED225B55',
    getStyle: undefined,
  };

  state = {
    circles: [],
  };

  counter = 1;
  interval = null;
  anim = new Animated.Value(1);

  componentDidMount() {
    this.setCircleInterval();
  }

  setCircleInterval = () => {
    this.interval = setInterval(this.addCircle, this.props.interval);
    this.addCircle();
  }

  addCircle = () => {
    this.setState({
      circles: [...this.state.circles, this.counter],
    });
    this.counter++;
  }

  onPressIn = () => {
    Animated.timing(this.anim, {
      toValue: this.props.pressInValue,
      duration: this.props.pressDuration,
      easing: this.props.pressInEasing,
      useNativeDriver: true,
    }).start(() => clearInterval(this.interval));
  }

  onPressOut = () => {
    Animated.timing(this.anim, {
      toValue: 1,
      duration: this.props.pressDuration,
      easing: this.props.pressOutEasing,
      useNativeDriver: true,
    }).start(this.setCircleInterval);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const {
      size,
      avatar,
      avatarBackgroundColor,
      interval,
    } = this.props;

    return (
      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          position: 'absolute',
          top: dims.SCREEN_HEIGHT / 2 - size / 2 - 20,
        }}
      >
        {this.state.circles.map(circle => (
          <Pulse
            key={circle}
            {...this.props}
          />
				))}

        <TouchableOpacity
          activeOpacity={1}
          onPressIn={this.onPressIn}
          onPressOut={this.onPressOut}
          style={{
            transform: [{
              scale: this.anim,
            }],
          }}
        >
          <Image
            source={{ uri: avatar }}
            style={{
              width: size,
              height: size,
              borderRadius: size / 2,
            }}
            resizeMode="cover"
          />
        </TouchableOpacity>
      </View>
    );
  }
}
