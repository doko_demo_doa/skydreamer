import React, { PropTypes } from 'react';
import Svg, {
  LinearGradient,
  Image,
  Defs,
  Rect,
  Stop,
} from 'react-native-svg';
import { dims } from 'skydreamer/config';

const ImageGradient = ({ backgroundUri }) => (
  <Svg height={dims.SCREEN_HEIGHT} width={dims.SCREEN_WIDTH}>
    <Defs>
      <LinearGradient id="grad" x1="50%" y1="0" x2="0" y2="50%">
        <Stop offset="0" stopColor="#e7a32b" stopOpacity="3" />
        <Stop offset="1" stopColor="#e61f16" stopOpacity="3" />
      </LinearGradient>
    </Defs>
    <Rect x="0" y="0" width="100%" height="100%" fill="url(#grad)" />
    <Image width="100%" height="100%" opacity="0.3" href={{ uri: backgroundUri }} />
  </Svg>
);

ImageGradient.propTypes = {
  backgroundUri: PropTypes.string.isRequired,
};

export default ImageGradient;
