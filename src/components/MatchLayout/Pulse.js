import React from 'react';
import { View, StyleSheet, Animated, Easing, Dimensions } from 'react-native';
import { dims } from 'skydreamer/config';

const styles = StyleSheet.create({
  circleWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: dims.SCREEN_WIDTH / 2,
    top: dims.SCREEN_HEIGHT / 4.2,
  },
  circle: {
    borderWidth: 4 * StyleSheet.hairlineWidth,
  },
});

export default class Pulse extends React.Component {

  anim = new Animated.Value(0);

  componentDidMount() {
    Animated.timing(this.anim, {
      toValue: 1,
      duration: this.props.interval * 3,
      easing: Easing.in,
      useNativeDriver: true,
    })
		.start();
  }

  render() {
    const {
      size,
      pulseMaxSize,
      borderColor,
      backgroundColor,
      getStyle,
    } = this.props;

    const scaleInterpolation = this.anim.interpolate({
      inputRange: [0, 1],
      outputRange: [0.35, 1.75],
    });

    const opacityInterpolation = this.anim.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 0],
    });

    return (
      <View
        style={[styles.circleWrapper, {
          width: pulseMaxSize,
          height: pulseMaxSize,
          marginLeft: -pulseMaxSize / 2,
          marginTop: -pulseMaxSize / 2,
        }]}
      >
        <Animated.View
          style={[styles.circle, {
            borderColor,
            backgroundColor,
            width: pulseMaxSize,
            height: pulseMaxSize,
            transform: [{
              scale: scaleInterpolation,
            }],
            borderRadius: pulseMaxSize / 2,
            opacity: opacityInterpolation,
          }, getStyle && getStyle(this.anim)]}
        />
      </View>
    );
  }
}
