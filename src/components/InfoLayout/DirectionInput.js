import React, { PropTypes } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
} from 'react-native';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  inputStyle: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 5,
    marginRight: 15,
    marginLeft: 15,
    fontSize: 15,
    flex: 1,
    fontFamily: 'Poppins-Regular',
  },
  containerStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

const DirectionInput = (props) => {
  const {
    value,
    onChangeText,
    placeholder,
    placeholderColor,
    underlineColor,
  } = props;
  const { inputStyle, containerStyle } = styles;

  return (
    <View style={containerStyle}>
      <TextInput
        multiline={false}
        placeholder={placeholder}
        placeholderStyle={{ fontFamily: 'Poppins-Regular', fontSize: 18 }}
        autoCorrect={false}
        style={inputStyle}
        value={value}
        onChangeText={onChangeText}
        placeholderTextColor={placeholderColor || 'white'}
        underlineColorAndroid={underlineColor || 'white'}
      />
    </View>
  );
};

DirectionInput.propTypes = {
  value: PropTypes.string.isRequired,
  onChangeText: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  secureTextEntry: PropTypes.string,
  placeholderColor: PropTypes.string,
  underlineColor: PropTypes.string,
};

export default DirectionInput;
