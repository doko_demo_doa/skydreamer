import React, { PropTypes } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import {
  colors,
  dims,
} from 'skydreamer/config';

import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  image: {
    width: dims.SCREEN_WIDTH,
    height: 200,
  },
  textContainer: {
    top: 60,
    backgroundColor: 'rgba(0,0,0,0)',
  },
  text: {
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#fff',
    paddingLeft: 30,
  },
  country: {
    fontSize: 16,
  },
  city: {
    fontSize: 36,
    fontWeight: 'bold',
  },
  linearGradient: {
    top: 40,
    flex: 1,
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 5,
  },
});


const DestinationImage = ({ image, city, country }) => (
  <TouchableOpacity
    style={styles.container}
    activeOpacity={0.6}
    onPress={() => Actions.gallery()}
  >
    <Image
      style={styles.image}
      source={{ uri: image }}
      resizeMode="cover"
    >
      <LinearGradient
        colors={['rgba(0,0,0,0)', 'rgba(0,0,0,1)']}
        style={styles.linearGradient}
      >
        <View style={styles.textContainer}>
          <Text style={[styles.text, styles.country]}>{country}</Text>
          <Text style={[styles.text, styles.city]}>{city}</Text>
        </View>
      </LinearGradient>
    </Image>
  </TouchableOpacity>
);

DestinationImage.propTypes = {
  city: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  uri: PropTypes.string,
};

export default DestinationImage;
