import React, { PropTypes } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native';
import Scaling from 'skydreamer/Scaling';
import {
  colors,
  dims
} from 'skydreamer/config';
import {
  Row
} from 'skydreamer/components/common';

const styles = Scaling.newStylesheet({
  container: {
    paddingHorizontal: 5,
    flexDirection: 'row',
  },
  rating: {
    fontFamily: 'Poppins',
  },
  header: {
    color: '#000000',
    fontFamily: 'Poppins',
    fontSize: dims.titleFontSize,
    paddingBottom: 5,
  },
  label: {
    color: '#000000',
    fontFamily: 'Poppins',
    fontSize: dims.labelFontSize,
  },
  text: {
    color: colors.secondaryText,
    fontFamily: 'Poppins',
    fontSize: dims.labelFontSize,
  },
  section: {
    flex: 1,
    padding: 25,
    paddingRight: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  row:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  }
});

const PaymentDetails = ({ uri, payment, currency, type, name, email }) => {
  return (
    <View style={styles.section}>
      <Text style={styles.header}>Payment Details</Text>
      <View style={styles.row}>
        <Text style={styles.label}>Payment</Text>
        <Text style={styles.text}>{payment}{currency} {type}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Name</Text>
        <Text style={styles.text}>{name}</Text>
      </View>
      <View style={styles.row}>
      <Text style={styles.label}>Email</Text>
        <Text style={styles.text}>{email}</Text>
      </View>
    </View>
  );
};

PaymentDetails.propTypes = {
  uri: PropTypes.string.isRequired,
};


export default PaymentDetails;
