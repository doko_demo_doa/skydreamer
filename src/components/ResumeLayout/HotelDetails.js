import React, { PropTypes } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native';
import Scaling from 'skydreamer/Scaling';
import {
  colors,
  dims
} from 'skydreamer/config';
import {
  Row
} from 'skydreamer/components/common';

const styles = Scaling.newStylesheet({
  container: {
    paddingHorizontal: 5,
    flexDirection: 'row',
  },
  roomContainer: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  rating: {
    fontFamily: 'Poppins',
  },
  header: {
    color: '#000000',
    fontFamily: 'Poppins',
    fontSize: dims.titleFontSize,
    paddingBottom: 5,
  },
  label: {
    color: '#000000',
    fontFamily: 'Poppins',
    fontSize: dims.labelFontSize,
  },
  text: {
    color: colors.secondaryText,
    fontFamily: 'Poppins',
    fontSize: dims.labelFontSize,
  },
  section: {
    flex: 1,
    padding: 25,
    paddingRight: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  row:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  }
});

const HotelDetails = ({ uri, checkin, checkout, rooms }) => {
  console.log('uri', uri);
  return (
    <View style={styles.section}>
      <Text style={styles.header}>Booking Details</Text>
      <View style={styles.row}>
        <Text style={styles.label}>Check-in</Text>
        <Text style={styles.text}>{checkin}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Check-out</Text>
        <Text style={styles.text}>{checkout}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Rooms</Text>
          <View style={styles.roomContainer}>
            {
              rooms.map((uri, i) => (
                <Text key={i} style={styles.text}>{uri}</Text>
              ))
            }
          </View>
      </View>
    </View>
  );
};

HotelDetails.propTypes = {
  uri: PropTypes.string.isRequired,
};


export default HotelDetails;
