import React, { PropTypes } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity
} from 'react-native';
import Scaling from 'skydreamer/Scaling';
import {
  colors,
  dims
} from 'skydreamer/config';
import {
  Row
} from 'skydreamer/components/common';

const styles = Scaling.newStylesheet({
  container: {
    paddingHorizontal: 5,
    flexDirection: 'row',
  },
  rating: {
    fontFamily: 'Poppins',
  },
  header: {
    color: '#000000',
    fontFamily: 'Poppins',
    fontSize: dims.titleFontSize,
    paddingBottom: 5,
  },
  label: {
    color: '#000000',
    fontFamily: 'Poppins',
    fontSize: dims.labelFontSize,
  },
  text: {
    color: colors.secondaryText,
    fontFamily: 'Poppins',
    fontSize: dims.labelFontSize,
  },
  section: {
    flex: 1,
    padding: 25,
    paddingRight: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  row:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  }
});

const FlightDetails = ({ uri, isArrival, departureAirport, arrivalAirport, flightCode, date, boarding, departure, gate, seat, travelClass }) => {
  console.log('uri', uri);
  return (
    <View style={styles.section}>
      <Text style={styles.header}>{ isArrival ? 'Arrival' : 'Departure'} Details</Text>
      <View style={styles.row}>
        <Text style={styles.label}>Departure</Text>
        <Text style={styles.text}>{departureAirport}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Arrival</Text>
        <Text style={styles.text}>{arrivalAirport}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Flight code</Text>
        <Text style={styles.text}>{flightCode}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Date</Text>
        <Text style={styles.text}>{date}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Boarding</Text>
        <Text style={styles.text}>{boarding}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Departure</Text>
        <Text style={styles.text}>{departure}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Gate</Text>
        <Text style={styles.text}>{gate}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Seat</Text>
        <Text style={styles.text}>{seat}</Text>
      </View>
      <View style={styles.row}>
        <Text style={styles.label}>Travel class</Text>
        <Text style={styles.text}>{travelClass}</Text>
      </View>
    </View>
  );
};

FlightDetails.propTypes = {
  uri: PropTypes.string.isRequired,
};


export default FlightDetails
