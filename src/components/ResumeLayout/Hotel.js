import React, { PropTypes } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    paddingHorizontal: 5,
    flexDirection: 'row',
  },
  image: {
    backgroundColor: 'transparent',
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  textContainer: {
    paddingLeft: 20,
  },
  name: {
    fontSize: 25,
    fontFamily: 'Poppins',
    color: '#29cce3',
  },
  rating: {
    fontFamily: 'Poppins',
  },

});

const Hotel = ({ uri, name, rating, onPress, onPhotoPress }) => {
  console.log('uri', uri);
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.avatar} onPress={onPhotoPress}>
        <Image
          style={styles.image}
          resizeMode="cover"
          source={{ uri }}
        />
      </TouchableOpacity>
      <TouchableOpacity style={styles.textContainer} onPress={onPress}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.rating}>{rating} stars</Text>
      </TouchableOpacity>
    </View>
  );
};

Hotel.propTypes = {
  uri: PropTypes.string.isRequired,
};


export default Hotel;
