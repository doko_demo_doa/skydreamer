import React, { PropTypes } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Switch,
} from 'react-native';
import {
  colors,
  dims,
} from 'skydreamer/config';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  text: {
    fontFamily: 'Poppins',
    color: '#918d9c',
    fontSize: dims.labelFontSize,
  },
});

class Setting extends React.Component {
  state = {
    value: true,
  };
  constructor(props) {
    super(props);
    this.state = { value: props.value };
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}> {this.props.text} </Text>
        <Switch
          onValueChange={value => this.setState({ value })}
          style={{ marginBottom: 5 }}
          onTintColor="#C6FFAC"
          thumbTintColor={colors.profileColor}
          value={this.state.value}
        />
      </View>
    );
  }
}

Setting.propTypes = {
  text: PropTypes.string.isRequired,
};


export default Setting;
