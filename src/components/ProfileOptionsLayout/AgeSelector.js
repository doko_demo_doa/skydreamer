import React, { PropTypes } from 'react';
import { View, StyleSheet, Text, Switch } from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import Scaling from 'skydreamer/Scaling';
import {
  colors,
  dims
} from 'skydreamer/config';

const styles = Scaling.newStylesheet({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
  },
  multiSliderContainer: {
    flex: 1,
    alignSelf: 'center',
    bottom: -20,
  },
  text: {
    fontFamily: 'Poppins',
    color: '#918d9c',
    fontSize: dims.labelFontSize,
  },
  age: {
    fontFamily: 'Poppins-Regular',
    color: colors.profileColor,
    fontSize: dims.labelFontSize,
    paddingHorizontal: 5,
  },
});

class AgeSelector extends React.Component {
  state = {
    value: true,
    minAge: '18',
    maxAge: '25',
  };
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
    };
  }
  sliderOneValuesChangeStart = () => {
    this.setState({
      sliderOneChanging: true,
    });
  }

  sliderOneValuesChange = (values) => {
    const newValues = [0];
    newValues[0] = values[0];
    this.setState({
      sliderOneValue: newValues,
    });
  }

  sliderOneValuesChangeFinish = () => {
    this.setState({
      sliderOneChanging: false,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.text}> Age</Text>
          <View style={{ flexDirection: 'row', paddingLeft: 200 }}>
            <Text style={styles.age}>18-25</Text>
          </View>
        </View>
        <View style={styles.multiSliderContainer}>
          <MultiSlider
            values={[1, 7]}
            trackStyle={{ backgroundColor: '#f8cbb2' }}
            selectedStyle={{ backgroundColor: colors.profileColor }}
            markerStyle={{ backgroundColor: colors.profileColor }}
            unselectedStyle={{ backgroundColor: 'silver' }}
            sliderLength={260}
          />
        </View>

      </View>
    );
  }
}

AgeSelector.propTypes = {
  text: PropTypes.string,
};

export default AgeSelector;
