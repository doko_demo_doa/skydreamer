import React, { PropTypes } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

import { Avatar } from 'skydreamer/components/common';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const BottomSection = ({ avatarUris }) => (
  <View style={styles.container}>
    {
      avatarUris.map((uri, i) => (
        <Avatar
          key={i}
          uri={uri}
          size="small"
        />
      ))
    }
  </View>
);

BottomSection.propTypes = {
  avatarUris: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default BottomSection;
