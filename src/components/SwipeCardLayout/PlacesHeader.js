import React, { PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    marginBottom: 20,
  },
  text: {
    textAlign: 'center',
    color: '#d88e8c',
    fontSize: 13,
    fontWeight: 'bold',
  },
});

const PlacesHeader = ({ number }) => (
  <View style={styles.container}>
    <Text style={styles.text}>{number} PLACES</Text>
  </View>
);

PlacesHeader.propTypes = {
  number: PropTypes.number.isRequired,
};

export default PlacesHeader;
