import React, { PureComponent, PropTypes } from 'react';
import {
  View,
  Image,
  Animated,
  Easing,
  StyleSheet,
} from 'react-native';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    paddingHorizontal: 5,
  },
  avatar: {
    backgroundColor: '#FFFFFF',
    borderRadius: 60,
    padding: 2,
    zIndex: 0,
  },
  smallAvatar: {
    width: 35,
    height: 35,
    borderRadius: 26,
    zIndex: 1,
  },
  defaultAvatar: {
    width: 80,
    height: 80,
    borderRadius: 40,
    zIndex: 1,
  },
  border: {
    borderColor: 'red',
    borderWidth: 2,
  },
});

export default class Avatar extends PureComponent {

  static propTypes = {
    uri: PropTypes.string.isRequired,
    size: PropTypes.oneOf(['default', 'small']),
    withBorder: PropTypes.bool,
  };

  static defaultProps = {
    size: 'default',
    withBorder: false,
  };

  opacityValue = new Animated.Value(1);

  /**
   * @nocoldiz:
   *
   * Questo è il fulcro dell'animazione per gestire l'opacità del bordo dell'
   * avatar, per il momento forse è un po' grezza come procedura ma fa il suo
   * dovere.
   *
   * Ora come ora ci sono due problemi:
   * - l'avatar sopra alla scritta '4 friends' in SwipeCardLayout risulta avere
   * una colorazione verde per oltre metà della sua lunghezza alla sua destra
   * - animando l'opacità della vista padre dell'immagine, chiaramente anche
   * l'immagine stessa blinka, e questo behaviour probabilmente non è quello
   * che vogliamo. Più probabilmente sarebbe più interessante applicare un
   * bordo SVG da animare, che dici?
   *
   * In ogni caso, per ora il lampeggio rispetta quelli che credo siano i
   * requisiti per un MVP.
   */
  blink = () => {
    const fadeIn = Animated.timing(this.opacityValue, {
      toValue: 1,
      duration: 500,
      easing: Easing.elastic(2),
      useNativeDriver: true,
    });
    const fadeOut = Animated.timing(this.opacityValue, {
      toValue: 0,
      duration: 250,
      easing: Easing.linear,
      useNativeDriver: true,
    });
    Animated.sequence([
      fadeOut,
      fadeIn,
      fadeOut,
      fadeIn,
      fadeOut,
      fadeIn,
    ]).start();
  }

  // Penseremo poi a creare la struttura per triggerare quando necessario il
  // lampeggio del bordo dell'avatar
  componentDidMount() {
    { /*    this.blink();
    */ }
  }

  render() {
    const {
      uri,
      size,
      withBorder,
    } = this.props;
    return (
      <View style={styles.container}>
        <Animated.View
          style={[styles.avatar, {
            opacity: this.opacityValue,
          }]}
        >
          <Image
            style={[
              styles[`${size}Avatar`],
              withBorder && styles.border, // inutilizzato al momento
            ]}
            resizeMode="cover"
            source={{ uri }}
          />
        </Animated.View>
      </View>
    );
  }
}
