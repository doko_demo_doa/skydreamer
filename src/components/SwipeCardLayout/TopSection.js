import React, { PropTypes } from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    paddingHorizontal: 20,
    margin: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const TopSection = ({ onPress }) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={onPress}>
      <Icon
        name="md-options"
        size={20}
        color="#000"
      />
    </TouchableOpacity>
    <TouchableOpacity onPress={onPress}>
      <Icon
        name="md-search"
        size={20}
        color="#000"
      />
    </TouchableOpacity>

  </View>
);
export default TopSection;
