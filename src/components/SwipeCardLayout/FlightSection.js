import React, { PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { GradientText } from 'skydreamer/components/common';


import Icon from 'react-native-vector-icons/FontAwesome';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  /**
   * This wrapper style is meant to handle the common leftPadding of every Text
   * line without altering the width of the container provided by flex
   */
  wrapper: {
    paddingLeft: 25,
    paddingVertical: 10,
  },
  flightContainer: {
    paddingLeft: 25,
  },
  row: {
    flexDirection: 'row',
  },
  iconDeparture: {
    transform: [
      { rotate: '45deg' },
      { translateX: -1 },
      { translateY: 2 },
    ],
  },
  iconReturn: {
    transform: [{ rotate: '-135deg' }],
  },
  dateRow: {
    flexDirection: 'row',
    paddingVertical: 5,
  },
  label: {
    color: '#a19eab',
    fontSize: 10,
    paddingLeft: 10,
  },
  price: {
    color: '#ee7a8f',
    fontSize: 30,
  },
  moneySymbol: {
    color: '#f77832',
    fontSize: 16,
    paddingTop: 5,
  },
});

const FlightSection = ({ departureDate, returnDate, moneySymbol, price }) => (
  <View style={styles.container}>
    <View style={styles.wrapper}>
      <Text style={styles.label}>Flight</Text>
      <View style={styles.row}>
        <Text
          text={String(price)}
          style={styles.price}
        >
        {String(price)}
        </Text>
        <Text style={styles.moneySymbol}>{moneySymbol}</Text>
        <View style={styles.flightContainer}>
          <View style={styles.dateRow} >
            <Icon
              name="plane"
              color="#a49dbc"
              style={styles.iconDeparture}
            />
            <Text style={styles.label}>{departureDate}</Text>
          </View>
          <View style={styles.dateRow} >
            <Icon
              name="plane"
              color="#a49dbc"
              style={styles.iconReturn}
            />
            <Text style={styles.label}>{returnDate}</Text>
          </View>
        </View>
      </View>
    </View>
  </View>
);

FlightSection.propTypes = {
  departureDate: PropTypes.string.isRequired,
  returnDate: PropTypes.string.isRequired,
  moneySymbol: PropTypes.string,
  price: PropTypes.number.isRequired,
};

FlightSection.defaultProps = {
  moneySymbol: '$',
};

export default FlightSection;
