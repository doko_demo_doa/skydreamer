import React, { PropTypes } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  infoContainer: {
    padding: 5,
    borderRadius: 100,
  },
});

const InfoButton = ({ color, size, style, onPress }) => (
  <TouchableOpacity
    onPress={onPress}
    style={[styles.infoContainer, style]}
  >
    <Icon
      name="ios-information-circle-outline"
      size={size}
      color={color}
    />
  </TouchableOpacity>
);

InfoButton.propTypes = {
  color: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
  style: PropTypes.object,
  onPress: PropTypes.func.isRequired,
};

InfoButton.defaultProps = {
  style: {},
};

export default InfoButton;
