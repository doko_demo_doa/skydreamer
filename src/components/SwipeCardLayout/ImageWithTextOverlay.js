import React, { PropTypes } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    alignItems: 'center',
    borderRadius: 10,
  },
  image: {
    paddingTop: 240,
    width: 320,
    height: 340,
  },
  textContainer: {
    backgroundColor: 'rgba(0,0,0,0)',
  },
  text: {
    backgroundColor: 'rgba(0,0,0,0)',
    color: '#fff',
    paddingLeft: 30,
  },
  country: {
    fontSize: 16,
  },
  city: {
    fontSize: 36,
    fontWeight: 'bold',
  },
  linearGradient: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
    borderRadius: 5,
  },
});

/*
var styles = Scaling.newStylesheet({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  groupAvatarContainer: {
    position: 'relative',
    borderRadius: 24,
    overflow: 'hidden',
  },
  groupAvatarBorderRadiusFix: {
    position: 'absolute',
    top: -30,
    right: -30,
    bottom: -10,
    left: -10,
    borderRadius: 29,
    borderWidth: 10,
    borderColor: '#fff',
  },
});
*/

const ImageWithTextOverlay = ({ image, city, country, onPress }) => (
  <TouchableOpacity
    style={styles.container}
    activeOpacity={0.6}
    onPress={onPress}
  >
    <Image
      style={styles.image}
      source={{ uri: image }}
      resizeMode="cover"
      borderRadius={15}
    >
      <LinearGradient
        colors={['rgba(0,0,0,0)', 'rgba(0,0,0,1)']}
        style={styles.linearGradient}
      >
        <View style={styles.textContainer}>
          <Text style={[styles.text, styles.country]}>{country}</Text>
          <Text style={[styles.text, styles.city]}>{city}</Text>
        </View>
      </LinearGradient>
    </Image>
  </TouchableOpacity>
);

ImageWithTextOverlay.propTypes = {
  city: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  uri: PropTypes.string,
  onPress: PropTypes.func.isRequired,
};

export default ImageWithTextOverlay;
