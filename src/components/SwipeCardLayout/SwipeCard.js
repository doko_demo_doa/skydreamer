import React, { PropTypes } from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

import {
  ImageWithTextOverlay,
  FlightSection,
  FriendsLikedSection,
  InfoButton,
} from './';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  row: {
    flexDirection: 'row',
  },
});

const SwipeCard = ({
  city,
  country,
  departureDate,
  returnDate,
  likedNumber,
  likedThumbUris,
  moneySymbol,
  price,
}) => (
  <View style={styles.container}>
    <ImageWithTextOverlay
      city={city}
      country={country}
    />
    <View
      style={{
        alignItems: 'center',
      }}
    >
      <InfoButton
        color="#d88e8c"
        size={20}
        style={{
          position: 'absolute',
          borderRadius: 15,
          width: 30,
          height: 30,
          zIndex: 10,
          top: -15,
          backgroundColor: '#FFF',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      />
    </View>
    <View style={styles.row}>
      <FlightSection
        moneySymbol={moneySymbol}
        price={price}
        departureDate={departureDate}
        returnDate={returnDate}
      />
      <View
        style={{
          width: 2,
          height: 80,
          backgroundColor: '#eeeff6',
          alignItems: 'center',
          justifyContent: 'flex-end',
        }}
      />
      <FriendsLikedSection
        uri={likedThumbUris[0]}
        likedNumber={likedNumber}
      />
    </View>
  </View>
);

SwipeCard.propTypes = {
  city: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
  departureDate: PropTypes.string.isRequired,
  returnDate: PropTypes.string.isRequired,
  likedNumber: PropTypes.number.isRequired,
  likedThumbUris: PropTypes.arrayOf(PropTypes.string).isRequired,
  moneySymbol: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
};

export default SwipeCard;
