import React, { PropTypes } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Scaling from 'skydreamer/Scaling';

import { Avatar } from './';

const LIKE_ICON_DIM = 15;

const styles = Scaling.newStylesheet({
  container: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  wrapper: {
    paddingRight: 25,
    paddingVertical: 10,
  },
  row: {
    flexDirection: 'row',
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    color: '#a19eab',
    fontSize: 12,
  },
});

const FriendsLikedSection = ({ likedNumber, uri }) => (
  <View style={styles.container}>
    <View style={styles.wrapper}>
      <Avatar uri={uri} size="small" />
      <View style={styles.row} >
        <Icon
          name="ios-thumbs-up-outline"
          color="#ec514c"
          size={LIKE_ICON_DIM}
          style={{ paddingHorizontal: 5, left: -LIKE_ICON_DIM }}
        />
        <Text style={[styles.label, { left: -LIKE_ICON_DIM }]}>{likedNumber} friends</Text>
      </View>
    </View>
  </View>
);

FriendsLikedSection.propTypes = {
  likedNumber: PropTypes.number.isRequired,
  uri: PropTypes.string.isRequired,
};

export default FriendsLikedSection;
