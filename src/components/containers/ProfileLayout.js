import React, { PureComponent } from 'react';
import {
  View,
  ScrollView,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from 'skydreamer/utils/icoMoonConfig.json';
const Icon = createIconSetFromIcoMoon(icoMoonConfig);
import moment from 'moment';
import {
  GradientText,
  Row,
  Column,
  GroupSection,
} from 'skydreamer/components/common';
import { TopBarProfileBuddies } from 'skydreamer/components/common/TopTabBar';
import {
  ImageProfile,
  ActionButton,
  ToolBar,
  Tags,
  SettingsButton,
} from 'skydreamer/components/ProfileLayout';
import {
  colors,
  dims,
} from 'skydreamer/config';
import { Container, Avatar } from '../common';
import Scaling from 'skydreamer/Scaling';


const styles = Scaling.newStylesheet({
  scrollView: {
    flex: 1,
  },
  scrollViewContentContainer: {
    paddingTop: 20,
    paddingBottom: dims.topTabBar * 3,
  },
  name: {
    color: '#000000',
    fontFamily: 'Poppins-SemiBold',
    fontSize: dims.headerFontSize,
  },
  settings: {
    fontFamily: 'Poppins',
    fontSize: 16,
    color: '#39b25b',
  },
  secondaryText: {
    fontFamily: 'Poppins',
    color: '#a9a8b5',
    fontSize: 16,
  },
  groupContainer: {
    padding: 2,
    flex: 1,
    marginVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  section: {
    flex: 1,
    alignItems: 'flex-start',
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  secondarySection: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  groupSection: {
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollContainer: {
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
});

export default class ProfileLayout extends PureComponent {

  static renderNavigationBar = (props) => {
    console.log('ProfileLayout.renderNavigationBar', props);
    return (
      <TopBarProfileBuddies isOnProfile />
    );
  }

  state = {
    email: 'guilherme@skydreamer.io',
    image: 'https://static.pexels.com/photos/160699/girl-dandelion-yellow-flowers-160699.jpeg',
    places: 14,
    miles: 10,
    tags: '#beach #party #adventure',
    description: 'I like laughing, dogs, lots of food, beer, outdoor activities and adventures.',
    status: '1',
    lastAccess: '0',
    id_group: null,
    avatarUris: [
      'https://randomuser.me/api/portraits/women/10.jpg',
      'https://randomuser.me/api/portraits/men/12.jpg',
      'https://randomuser.me/api/portraits/women/13.jpg',
    ],
    updated_at: 1466793006,
    created_at: 1466793006,
    user_id: '1',
    id_facebook: null,
    registration_id: null,
    latitude: null,
    longitude: null,
    birthday: 655516800,
    name: 'Michelle',
    gender: 'Female',
    city: 'Amsterdam',
    country: '2',
    cellphone: '(34) 99308-2415',
    pointing: '0',
  }

  getAge(dateString) {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = moment().get('year') - moment(dateString).get('year');
    const m = moment().get('month') - moment(dateString).get('month');
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  render() {
    return (
      <Container>
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.scrollViewContentContainer}
        >
          <View style={styles.scrollContainer}>
            <ImageProfile
              image={this.state.image}
            />
            <View
              style={{
                alignItems: 'center',
              }}
            >
              <ActionButton
                color="#FFF"
                size={20}
                icon="group_add"
                style={{
                  position: 'absolute',
                  borderRadius: 40,
                  width: 45,
                  height: 45,
                  zIndex: 10,
                  top: -20,
                  right: 65,
                  backgroundColor: '#39b25b',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => { alert('pressed'); }}
              />
              <ActionButton
                color="#FFF"
                size={20}
                icon="pencil"
                style={{
                  position: 'absolute',
                  borderRadius: 40,
                  width: 45,
                  height: 45,
                  zIndex: 10,
                  top: -20,
                  right: 15,
                  backgroundColor: '#39b25b',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                onPress={() => { Actions.profileEdit(); }}
              />
            </View>
            <View style={styles.groupContainer}>
              { this.state.avatarUris.map((uri, index) => <Image
                key={index}
                source={{ uri }}
                style={{ width: 44, height: 44, borderRadius: 22, marginHorizontal: 3, borderWidth: 3, borderColor: 'rgb(233, 242, 255)' }}
                resizeMode="cover"
              />) }
            </View>
            <View style={styles.section}>
              <Row>
                <Text style={styles.name}>{this.state.name}</Text>
                <SettingsButton
                  onPress={() => {
                    Actions.profileOptions();
                  }}
                />
              </Row>
              <Row>
                <Text
                  style={styles.secondaryText}
                >{this.getAge(this.state.birthday)} yrs {this.state.city}
                </Text>
                <Icon
                  style={{ marginLeft: 8 }}
                  name="map-marker"
                  size={15}
                  color="#a9a8b5"
                />
              </Row>
              <Row style={{ paddingVertical: 5 }}>
                <Tags
                  text={this.state.tags}
                  size={20}
                  width={250}
                  fontWeight="500"
                  startColor="#22ae64"
                  endColor="#d2d01f"
                />
              </Row>
              <Row style={{ paddingVertical: 32 }}>
                <Text
                  style={styles.secondaryText}
                >{this.state.description}
                </Text>
              </Row>
            </View>
            <View style={styles.secondarySection}>
              <Column>
                <GradientText
                  text={this.state.places}
                  size={40}
                  width={45}
                  fontWeight="500"
                  startColor="#22ae64"
                  endColor="#d2d01f"
                />
                <Text>places</Text>
              </Column>
              <Column>
                <GradientText
                  text={this.state.miles}
                  size={40}
                  width={45}
                  fontWeight="500"
                  startColor="#22ae64"
                  endColor="#d2d01f"
                />
                <Text>miles</Text>
              </Column>
            </View>
          </View>
        </ScrollView>
      </Container>
    );
  }
}
