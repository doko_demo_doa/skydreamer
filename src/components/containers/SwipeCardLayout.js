import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SwipeCards from 'react-native-swipe-cards';
import {
  swipeCardActions,
  groupActions,
  galleryActions,
} from 'skydreamer/actions';
import { TopBarSearch } from '../common/TopTabBar';
import Icon from 'react-native-vector-icons/Ionicons';
import styled from 'styled-components/native';
import {
  colors,
  dims,
} from 'skydreamer/config';
import Scaling from 'skydreamer/Scaling';

import {
  ImageWithTextOverlay,
  FlightSection,
  BottomSection,
  TopSection,
  FriendsLikedSection,
  InfoButton,
  PlacesHeader,
} from 'skydreamer/components/SwipeCardLayout';

const Container = styled.View`
  flex: 1;
  background-color: #fff8f7;
`;

const styles = Scaling.newStylesheet({
  buttons: {
    width: 80,
    height: 80,
    borderWidth: 10,
    borderColor: '#e7e7e7',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40,
  },
  buttonSmall: {
    width: 50,
    height: 50,
    borderWidth: 10,
    borderColor: '#e7e7e7',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 25,
  },
  dateContainer: {
    flex: 1,
    flexDirection: 'row',
  },
  row: {
    flexDirection: 'row',
  },
});

class SwipeCardLayout extends Component {

  static renderNavigationBar = (props) => {
    console.log('SwipeCardLayout.renderNavigationBar', props);
    return (
      <TopBarSearch />
    );
  }

  componentDidMount() {
    const {
      swipeCardActions,
      groupActions,
    } = this.props;

    /*
    swipeCardActions.startCardsFetch({
      country: 'it',
      origin: 'VCE',
      destination: 'AMS',
      outbound_date: '2017-06',
      currency: 'EUR',
      budget: 900,
      travel_id: 5,
    });
    */

    /**
     * This one doesn't necessarily have to be called here, atm I've only used it
     * for test purposes
     */
    // groupActions.getUserGroupsListFetch();
  }

  handleImagePress = () => {
    console.log('handleImagePress@SwipeCardLayout');
    this.props.galleryActions.startGetGalleryPanoramaPostsFetch({
      card_id: 1,
      number: 3,
      offset: 0,
    });

    Actions.gallery();
  }

  card = (cardData) => {

    console.log('cardData', cardData);

    const {
      title,
      subtitle,
      flight_details,
      friends,
      id,
      type,
      uuid,
      image,
      userVotes,
    } = cardData;

    const {
      price,
      departDate,
      returnDate,
    } = flight_details;

    return (
      <View>
        <ImageWithTextOverlay
          image={image}
          city={title}
          country={subtitle}
          onPress={this.handleImagePress}
        />
        <View style={styles.row}>
          <FlightSection
            moneySymbol={cardData.moneySymbol}
            price={price}
            departureDate={departDate}
            returnDate={returnDate}
          />
          <View
            style={{
              width: 2,
              height: 80,
              backgroundColor: '#eeeff6',
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}
          />
          {
            !!userVotes &&
              <FriendsLikedSection
                uri={userVotes.friend_pic}
                likedNumber={userVotes.count}
              />
          }
        </View>
        <View
          style={{
            alignItems: 'center',
          }}
        >
          {/*! cardData.hideInfo ? <Text style={{ color: 'red' }}>AAAA</Text> : null */}
          <InfoButton
            color={colors.mainColor}
            size={20}
            style={{
              position: 'absolute',
              borderRadius: 15,
              width: 30,
              height: 30,
              top: -95,
              zIndex: 10,
              backgroundColor: '#FFF',
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={Actions.info}
          />
        </View>
      </View>
    );
  }

  handleYup = (card) => {
    console.log('card', card);
    console.log(`Yup for ${card.city}`);
    Actions.match();
  }

  handleNope = (card) => {
    console.log('card', card);
    console.log(`Nope for ${card.city}`);
  }

  noMore = () => (
    <View style={styles.card} >
      <Text>No More Cards</Text>
    </View>
  );

  yup = () => {
    console.log('yup', this.refs.swiper);
    this.refs.swiper.goToNextCard();
  }

  nope = () => {
    console.log('nope', this.refs.swiper);
    this.refs.swiper.goToNextCard();
  }

  render() {
    const {
      upsertLoading,
      sessions,
      swipeCard,
      selectedSession,
    } = this.props;
    const {
      isCardListLoading,
      cardList,
      cardError,
    } = swipeCard;

    console.log('selectedSession', selectedSession);
    console.log('sessions', sessions);

  console.log('cardList', cardList);

    const selected = { group_name: 'SKYDREAMER' };
    // const selected = sessions[selectedSession];
    // if(!selected) return <ActivityIndicator />;

    // if (upsertLoading || isCardListLoading) return <ActivityIndicator />;

    return (
      <Container>
        <SwipeCards
          ref={'swiper'}
          cards={cardList || []}
          containerStyle={{ backgroundColor: '#f7f7f7', alignItems: 'center' }}
          renderCard={this.card}
          renderNoMoreCards={this.noMore}
          yupStyle={{ backgroundColor: '#ffffff' }}
          yupTextStyle={{
            color: '#000000',
            fontSize: 60,
            fontFamily: 'Poppins',
            justifyContent: 'flex-start',
          }}
          yupText="LIKED"
          nopeText="UNLIKED"
          handleYup={this.handleYup}
          handleNope={this.handleNope}
        />
        <View style={{ position: 'absolute', top: Scaling.vertical(46), left: 0, right: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent' }}>
          <TouchableOpacity style={{ paddingVertical: 4, paddingHorizontal: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', backgroundColor: 'white', borderWidth: 1, borderColor: 'rgb(255, 103, 103)', borderRadius: 12 }}>
            <Text style={{ color: 'rgb(255, 103, 103)' }}>{selected.group_name}</Text>
            <Icon name="ios-arrow-down" size={24} style={{ marginLeft: 15 }} color="rgb(255, 103, 103)" />
          </TouchableOpacity>
        </View>

        <View style={{ position: 'absolute', bottom: Scaling.vertical(70), left: 0, right: 0, alignItems: 'center', justifyContent: 'center', backgroundColor: 'transparent', flexDirection: 'row' }} />
      </Container>
    );
  }
}

mapStateToProps = ({ swipeCard, auth, channels }) => ({
  swipeCard,
  upsertLoading: auth.upsertLoading,
  idToken: auth.idToken,
  sessions: channels.channels,
  selectedSession: 'viyYL3CQhQoeEI5BGZROOiJgzR3m',
});

mapDispatchToProps = dispatch => ({
  swipeCardActions: bindActionCreators(swipeCardActions, dispatch),
  groupActions: bindActionCreators(groupActions, dispatch),
  galleryActions: bindActionCreators(galleryActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SwipeCardLayout);
