import React, { PureComponent } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import {
  colors,
  dims,
} from 'skydreamer/config';
import {
  Setting,
  AgeSelector,
} from 'skydreamer/components/ProfileOptionsLayout';
import {
  GroupSection,
} from 'skydreamer/components/common';
import {
  Label,
} from 'skydreamer/components/SearchOptionsLayout';
import {
  GradientText,
  Row,
} from 'skydreamer/components/common';
import { TopBarTitle } from 'skydreamer/components/common/TopTabBar';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    marginVertical: dims.topTabBar, // height of TopBar
  },
  deleteProfile: {
    fontFamily: 'Poppins',
    color: '#c18989',
    fontSize: dims.chatFontSize,
  },
  header: {
    color: '#000000',
    paddingBottom: 15, // Remove this
    fontFamily: 'Poppins',
    fontSize: dims.chatFontSize,
  },
  groupName: {
    color: '#000000',
    paddingBottom: 15,
    fontFamily: 'Poppins-SemiBold',
    fontSize: dims.headerFontSize,
  },
  label: {
    fontFamily: 'Poppins',
    color: '#918d9c',
    fontSize: 15,
  },
  section: {
    flex: 1,
    padding: 25,
    paddingRight: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  scrollContainer: {
    backgroundColor: '#fff',
    borderRadius: 10,
    flexDirection: 'column',
  },
  groupSection: {
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logout: {
    fontFamily: 'Poppins',
    color: colors.profileColor,
    fontSize: dims.titleFontSize,
  },

});

export default class GroupOptionsLayout extends PureComponent {

  static renderNavigationBar = props => (
    <TopBarTitle title="Group Options" colors={colors.gradients.profileSection} />
  );

  state = {
    friends: 4,
    notifications: true,
    private: false,
    avatarUris: [
      'https://randomuser.me/api/portraits/men/10.jpg',
      'https://randomuser.me/api/portraits/men/12.jpg',
      'https://randomuser.me/api/portraits/men/13.jpg',
    ],
  }

  render() {
    const {
      departurePriceRatios,
      returnPriceRatios,
      departureMonth,
      returnMonth,
      weekStart,
      weekEnd,
      transferTypeOptions,
    } = this.state;

    return (
      <View
        style={styles.container}
      >
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.section}>
            <Row>
              <Text style={styles.groupName}>Springbreak</Text>
              <Icon
                name="pencil"
                color="#00"
                size={25}
                style={{
                  paddingHorizontal: 10,
                  paddingTop: 4,
                }}
              />
            </Row>
            <Text style={styles.header}>Friends ({this.state.friends})</Text>
            <GroupSection style={styles.groupSection} size="medium" avatarUris={this.state.avatarUris} />
            <Text style={styles.header}>Settings</Text>
            <Setting text="Disable notifications" value={this.state.notifications} />
            <Setting text="Make group private" value={this.state.private} />
          </View>
          <View style={styles.section}>
            <TouchableOpacity style={{ flexDirection: 'row' }}>
              <Icon
                name="sign-out"
                color={colors.profileColor}
                size={20}
                style={{
                  paddingRight: 5,
                  top: 5,
                }}
              />
              <Text style={styles.logout}>Leave group</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
