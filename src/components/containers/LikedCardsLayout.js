import React, { PureComponent } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { get as _get, find as _find, without as _without } from 'lodash';
import {
  colors,
  dims,
} from 'skydreamer/config';
import {
  TopBarTitle,
} from 'skydreamer/components/common/TopTabBar';
import {
  LikedCard,
  MatchedCard,
} from 'skydreamer/components/LikedCardsLayout';
import Scaling from 'skydreamer/Scaling';

const CARD_TYPES = {
  LIKED: 'LIKED',
  MATCHED: 'MATCHED',
};

const styles = Scaling.newStylesheet({
  header: {
    color: '#000000',
    padding: 3, // Remove this
    fontFamily: 'Poppins',
    fontSize: dims.chatFontSize,
  },
  label: {
    fontFamily: 'Poppins',
    color: '#918d9c',
    fontSize: 15,
  },
});

export default class LikedCardsLayout extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      avatarUris: [
        'https://randomuser.me/api/portraits/men/10.jpg',
        'https://randomuser.me/api/portraits/men/11.jpg',
        'https://randomuser.me/api/portraits/men/12.jpg',
      ],
      cardsConfig: [],
    };
  }

  componentDidMount() {
    this.setState({
      cardsConfig: this.getCardConfig(),
    });
  }

  static renderNavigationBar = props => (
    <TopBarTitle title="Liked Cards" hideBack colors={colors.gradients.likedcardsSection} />
    );

  render() {
    const { cardsConfig } = this.state;
    return (
      <FlatList
        contentContainerStyle={{ marginTop: dims.topTabBar + 20, marginBottom: dims.topTabBar, paddingHorizontal: dims.mainContainerPadding }}
        data={cardsConfig}
        renderItem={this.renderItem}
        _keyExtractor={this.keyExtractor}
      />
    );
  }

  keyExtractor = item => item.id;

  renderItem = ({ item }) => {
    const type = _get(item, 'type');
    switch (type) {
      case CARD_TYPES.MATCHED:
        return <MatchedCard {...item} />;
      case CARD_TYPES.LIKED:
        return <LikedCard {...item} />;
      default:
        return null;
    }
  }

  removeCard = (item) => {
    const { id, type } = item;
    if (type === CARD_TYPES.LIKED) {
      this.shouldRemoveCard(id);
      return;
    }
    Alert.alert(
      'Confirmation',
      'Are you sure you want to delete this card ?',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'OK', onPress: () => this.shouldRemoveCard(id) },
      ],
      { cancelable: true },
    );
  }

  shouldRemoveCard = (id) => {
    const element = _find(this.state.cardsConfig, { id });
    const updatedCardsConfig = _without(this.state.cardsConfig, element);
    this.setState({
      cardsConfig: updatedCardsConfig,
    });
  }

  getCardConfig = () => {
    const cardsConfig = [
      {
        id: 123,
        type: CARD_TYPES.LIKED,
        location: 'Palermo',
        nation: 'Italy',
        image: 'http://www.artribune.com/wp-content/uploads/2016/11/Palermo-1.jpg',
        avatarUris: this.state.avatarUris,
        removeCard: this.removeCard,
      },
      {
        id: 124,
        type: CARD_TYPES.MATCHED,
        location: 'London',
        nation: 'United Kingdom',
        text: 'Booked',
        color: '#e7a32b',
        image: 'https://media.timeout.com/images/103042848/image.jpg',
        primaryGradient: '#e61f16',
        secondaryGradient: '#e7a32b',
        removeCard: this.removeCard,
      },
      {
        id: 125,
        type: CARD_TYPES.MATCHED,
        location: 'Santo Domingo',
        nation: 'Dominican Rep',
        color: '#186be7',
        text: 'Liked',
        image: 'https://cdn.getyourguide.com/niwziy2l9cvz/GSQ6Mre2aaK42aSUyYeme/ee5c81d58448ecbf50f4818e374f4784/mykonos-1112x630__2_.jpg',
        primaryGradient: '#11d5c6',
        secondaryGradient: '#186be7',
        removeCard: this.removeCard,
      },
      {
        id: 126,
        type: CARD_TYPES.LIKED,
        location: 'Palermo',
        nation: 'Italy',
        image: 'http://www.artribune.com/wp-content/uploads/2016/11/Palermo-1.jpg',
        avatarUris: this.state.avatarUris,
        removeCard: this.removeCard,
      },
    ];
    return cardsConfig;
  }
}
