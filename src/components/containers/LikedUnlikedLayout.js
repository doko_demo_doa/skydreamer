import React, { PureComponent } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import {
  colors,
  dims,
} from 'skydreamer/config';
import {
  Setting,
  AgeSelector,
} from 'skydreamer/components/ProfileOptionsLayout';
import {
  Label,
} from 'skydreamer/components/SearchOptionsLayout';
import {
  DestinationImage,
  FlightHeader,
  Hotel,
} from 'skydreamer/components/ResumeLayout';
import {
  GradientText,
  Row,
  GroupSection,
} from 'skydreamer/components/common';
import { TopBarTitle } from 'skydreamer/components/common/TopTabBar';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    marginTop: dims.topTabBar,
    paddingBottom: dims.topTabBar,
  },
  header: {
    color: '#000000',
    alignSelf: 'flex-start',
    fontFamily: 'Poppins',
    fontSize: dims.titleFontSize,
    paddingBottom: 5,
  },
  label: {
    fontFamily: 'Poppins',
    color: '#918d9c',
    fontSize: 15,
  },
  section: {
    flex: 1,
    padding: 25,
    paddingRight: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  scrollContainer: {
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  groupContainer: {
    margin: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default class LikedUnlikedLayout extends PureComponent {

  static renderNavigationBar = props => (
    <TopBarTitle title="Resume" colors={colors.gradients.likedcardsSection} />
  );

  state = {
    liked: [
      'https://randomuser.me/api/portraits/men/10.jpg',
      'https://randomuser.me/api/portraits/women/10.jpg',
    ],
    unliked: [
      'https://randomuser.me/api/portraits/women/14.jpg',
    ],
  }

  render() {
    return (
      <View
        style={styles.container}
      >
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.section}>
            <Text style={styles.header}>Liked</Text>
            <GroupSection style={styles.groupContainer} size="medium" avatarUris={this.state.liked} />
          </View>
          <View style={styles.section}>
            <Text style={styles.header}>Unliked</Text>
            <GroupSection style={styles.groupContainer} size="medium" avatarUris={this.state.unliked} />
          </View>
        </ScrollView>
      </View>
    );
  }
}
