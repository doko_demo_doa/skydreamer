import React, { PureComponent } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import {
  colors,
  dims,
} from 'skydreamer/config';
import {
  Setting,
  AgeSelector,
} from 'skydreamer/components/ProfileOptionsLayout';
import {
  Label,
} from 'skydreamer/components/SearchOptionsLayout';
import {
  FlightDetails,
  PaymentDetails,
  HotelDetails,
} from 'skydreamer/components/ResumeLayout';
import {
  GradientText,
  Row,
  GroupSection,
} from 'skydreamer/components/common';
import { TopBarTitle } from 'skydreamer/components/common/TopTabBar';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  section: {
    flex: 1,
    padding: 25,
    paddingRight: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  scrollContainer: {
    marginVertical: dims.topTabBar, // height of TopBar
    backgroundColor: '#fff',
    borderRadius: 10,
    flexDirection: 'column',
  },
});

export default class ResumeLayout extends PureComponent {

  static renderNavigationBar = props => (
    <TopBarTitle title="Lexington Hotel" colors={colors.gradients.likedcardsSection} />
  );
  state = {
    rooms: [
      'Single room',
      'Double room',
      'Single room',
    ]
  };
  render() {
    return (
      <ScrollView style={styles.scrollContainer}>
        <HotelDetails uri="" checkin="15/05/2017" checkout="18/05/2017" rooms={this.state.rooms}/>
        <FlightDetails uri=""  departureAirport="Schipol-AMS" arrivalAirport="Malpensa-AMS" flightCode="LK1203" date="13/02/2016" boarding="19:30" departure="20:00" gate="4" seat="14A" travelClass="economy" />
        <FlightDetails uri="" isArrival departureAirport="Malpensa-AMS" arrivalAirport="Schipol-AMS" flightCode="LK1284" date="17/02/2016" boarding="12:30" departure="13:00" gate="3" seat="12A" travelClass="economy" />
        <PaymentDetails uri="" payment="300" currency="$" name="Jhon Smith"  email="jhonsmith@gmail.com"/>
      </ScrollView>
    );
  }
}
