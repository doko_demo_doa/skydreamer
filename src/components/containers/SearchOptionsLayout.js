import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  Image,
  AsyncStorage,
  Slider,
  Switch,
} from 'react-native';
import styled from 'styled-components/native';
import { connect } from 'react-redux';
import { offlineActions } from 'skydreamer/actions';
import { saveToLocalStorage } from 'skydreamer/utils';
import { Actions } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import Bar from 'skydreamer/images/sliderBlack.png';
import CheckBox from 'react-native-checkbox';
import { TopBarTitle } from 'skydreamer/components/common/TopTabBar';
import {
  Label,
} from 'skydreamer/components/SearchOptionsLayout';
import {
  Topic,
  MonthModal,
  AirportModal,
  TopicModal,
} from 'skydreamer/components/SessionGradientElements';
import {
  colors,
  dims,
} from 'skydreamer/config';
import Scaling from 'skydreamer/Scaling';
import {
  GradientText,
  Row,
  NavBar,
  Icon,
} from 'skydreamer/components/common';
import { SessionGradientStyle } from 'skydreamer/styles';
import { getCurrentMonth } from 'skydreamer/utils';

import InspirationActive from 'skydreamer/images/topics/inspiration.png';
import PackageActive from 'skydreamer/images/topics/package.png';
import InspirationInactive from 'skydreamer/images/topics/inspiration-disabled.png';
import PackageInactive from 'skydreamer/images/topics/package-disabled.png';

const ModalToggler = styled.TouchableOpacity`
  width: ${dims.SCREEN_WIDTH * 0.9};
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const Center = styled.View`
  justify-content: center;
  align-items: center;
`;

const styles = Scaling.newStylesheet({
  container: {
    flex: 1,
    marginTop: dims.topTabBar, // height of TopBar
  },
  section: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  scrollContainer: {
    backgroundColor: '#fff',
    borderRadius: 10,
    flexDirection: 'column',
  },
  price: {
    color: '#FB1300',
    fontSize: 30,
    fontFamily: 'Poppins',
    justifyContent: 'flex-start',
  },
  slider: {
    width: dims.SCREEN_WIDTH * 0.75,
    marginLeft: dims.mainContainerPadding,
    marginRight: dims.mainContainerPadding,
    zIndex: 1,
  },
  bar: {
    height: 10,
    width: dims.SCREEN_WIDTH * 0.75,
    marginLeft: dims.mainContainerPadding,
    marginRight: dims.mainContainerPadding,
  },
  labelContainer: {
    width: dims.SCREEN_WIDTH * 0.75,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: dims.mainContainerPadding,
    marginRight: dims.mainContainerPadding,
  },
  sliderLabel: {
    color: colors.mainColor,
    fontSize: dims.subtitleFontSize,
    fontFamily: 'Poppins-Regular',
    fontWeight: 'bold',
  },
  header: {
    color: '#232',
    alignSelf: 'flex-start',
    fontFamily: 'Poppins',
    fontSize: dims.titleFontSize,
    paddingLeft: 15,
  },
  textValue: {
    color: '#320edc',
    fontSize: 40,
    fontFamily: 'Poppins-Light',
    justifyContent: 'flex-start',
  },
  row: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 15,
  },
  upperTitle: {
    color: '#232',
    fontSize: 26,
    fontFamily: 'Poppins-Light',
    textAlign: 'left',
    marginBottom: 0,
    paddingBottom: 0,
  },
});

console.log('Topic', Topic);
class SearchOptionsLayout extends Component {
  static renderNavigationBar = props => (
    <TopBarTitle title="Search Options" colors={colors.gradients.swipecardsSection} />
  );

  state = {
    value: 120,
    weekends: 'false',
    daysString: 'days', // can be one day or days
    monthSelected: getCurrentMonth(),
    topicsSelected: [
      'Inspiration',
    ],
    likedThumbUris: [
      'http://www.nationallawjournal.com/image/EM/NLJ/Bjarne-Tellman-Article-201702032138.jpg',
      'https://organicthemes.com/demo/profile/files/2012/12/profile_img.png',
      'http://cdn.business2community.com/wp-content/uploads/2014/04/profile-picture.jpg',
    ],
    checkboxIsDaySelected: false,
    checked: false,
    isModalDateVisible: false,
    isModalAirportVisible: false,
    isModalTopicVisible: false,

    airportQuery: '',
    airportSelected: 'Malpensa',
    airportCodeSelected: 'MXP',
    airports: [
      'MXP Malpensa 4km',
      'LIN Linate 6km',
      'BGY Bergamo 20km',
      'VBS Brescia 30km',
      'VRN Verona 112km',
      'TSF Treviso 189km',
      'VCE Venezia 221km',
    ],

    selectedTopic: '',
    selectedTopicDescription: '',
    topics: [
      {
        name: 'Inspiration',
        info: 'We are showing you all the best Destinations you could visit across Europe!',
        imageActive: InspirationActive,
        imageInactive: InspirationInactive,
        selected: true,
      }, {
        name: 'Package',
        info: 'We show you Package deals of Hotel+Flight and group Package deals from Groupon!',
        imageActive: PackageActive,
        imageInactive: PackageInactive,
        selected: false,
      },
    ],
  };

  componentDidMount() {
    /*
    const airports = this.getAirports(this.props.nearestAirportList);
    this.setState({
      airports,
    });
    */
  }

  getAirports = (nearestAirportList) => {
    const airports = (nearestAirportList || []).map(airport => airport.name);
    return airports;
  }

  componentWillReceiveProps(nextProps) {
    console.log('nextProps@SearchOptionsLayout', nextProps);
    if (this.props.isNearestAirportLoading != nextProps.isNearestAirportLoading) {
      const airports = getAirports(nextProps.nearestAirportList)
      this.setState({
        airports,
      });
    }
  }

  renderTopics = () =>
    this.state.topics.map(({ name, info, imageActive, imageInactive, selected }, i) =>
      <Topic
        key={`topic_${i}`}
        index={i}
        name={name}
        info={info}
        imageActive={imageActive}
        imageInactive={imageInactive}
        selected={selected}
        onPress={this.onTopicPress}
        onLongPress={this.onTopicLongPress}
        textColor="#777"
      />
    );

  onTopicPress = (index) => {
    this.setState(({ topics }) => {
      const newTopics = [...topics];
      const newTopic = newTopics[index];
      newTopic.selected = !newTopic.selected;
      return {
        topics: newTopics,
      };
    });
  }

  onTopicLongPress = (index) => {
    console.log('onLongPress', index);
    this.setState(({ topics }) => {
      const newTopics = [...topics];
      const newTopic = newTopics[index];
      return {
        selectedTopic: newTopic.name,
        selectedTopicDescription: newTopic.info,
      };
    });
    this.toggleTopicModal();
  }

  onAirportChange = ({ position }) => {
    const {
      nearestAirportList,
      offlineActions,
    } = this.props;


    const departure_airport_id = nearestAirportList[position].id;
    offlineActions.setSessionGradientOffline({
      departure_airport_id,
    });
    saveToLocalStorage('@SessionGradient:departure_airport_id', departure_airport_id);
  }

  onMonthChange = ({ position, data }) => {
    const departure = moment().month(position).unix();
    this.props.offlineActions.setSessionGradientOffline({
      departure,
    });
    saveToLocalStorage('@SessionGradient:departure', departure);
    saveToLocalStorage('@SessionGradient:type', 'month');
  }

  onValueChange = (value) => {
    this.setState({
      value,
    });
    this.props.offlineActions.setSessionGradientOffline({
      price_amount: value,
    });
    saveToLocalStorage('@SessionGradient:price_amount', value);
  }

  calculateStep = () =>
    (this.state.value < 200) ? 5 : 10;

  toggleMonthModal = () => {
    this.setState((prevState) => ({
      isModalDateVisible: !prevState.isModalDateVisible,
    }));
  }

  toggleAirportModal = () => {
    this.setState((prevState) => ({
      isModalAirportVisible: !prevState.isModalAirportVisible,
    }));
  }

  toggleTopicModal = () => {
    this.setState((prevState) => ({
      isModalTopicVisible: !prevState.isModalTopicVisible,
    }));
  }

  setAirportSelected = (airportSelectedString) => {
    const v = airportSelectedString.split(' ');
    const airportCodeSelected = v[0];
    const airportSelected = v[1];

    this.setState({
      airportCodeSelected,
      airportSelected,
    });
  }

  onMonthChange = (monthSelected) => {
  //  const month = moment().month(position).unix();
    this.setState({
      monthSelected,
    });
    /*
    this.props.offlineActions.setSessionGradientOffline({
      monthSelected,
    });
    saveToLocalStorage('@SessionGradient:departure', monthSelected);
    saveToLocalStorage('@SessionGradient:type', 'month');
    */
  }

  onSwitchChange = (checkboxIsDaySelected) => {
    console.log('checkboxIsDaySelected', checkboxIsDaySelected);
    this.setState({
      checkboxIsDaySelected,
    });
  }

  onCheckboxChange = (checked) => {
    this.setState({
      checked: !checked,
    });
  }

  onChangeAirportQuery = (airportQuery) => {
    this.setState({
      airportQuery,
    });
  }

  render() {
    const {
      departurePriceRatios,
      returnPriceRatios,
      departureMonth,
      returnMonth,
      weekStart,
      weekEnd,
      transferTypeOptions,
      value,
      likedThumbUris,
      isModalDateVisible,
      isModalAirportVisible,
      isModalTopicVisible,
      monthSelected,
      checkboxIsDaySelected,
      checked,
      airportQuery,
      airports,
      airportCodeSelected,
      airportSelected,
      selectedTopic,
      selectedTopicDescription,
    } = this.state;

    const step = this.calculateStep();

    console.log('this.props@SearchOptionsLayout', this.props);
    console.log('likedThumbUris', likedThumbUris);
    console.log('airports', airports);

    return (
      <View
        style={styles.container}
      >
        <MonthModal
          isVisible={isModalDateVisible}
          toggleModal={this.toggleMonthModal}
          setMonthSelected={this.onMonthChange}
        />
        <AirportModal
          isVisible={isModalAirportVisible}
          defaultQueryValue={airportQuery}
          onChangeText={this.onChangeAirportQuery}
          airports={airports}
          toggleModal={this.toggleAirportModal}
          setAirportSelected={this.setAirportSelected}
        />
        <TopicModal
          toggleModal={this.toggleTopicModal}
          isVisible={isModalTopicVisible}
          selectedTopic={selectedTopic}
          selectedDescription={selectedTopicDescription}
        />

        <ScrollView
          style={styles.scrollContainer}
          automaticallyAdjustContentInsets={false}
        >
          <View style={styles.section}>
            <Text style={styles.header}>Flight price</Text>
            <Text style={styles.price}>{value}$</Text>
            <Slider
              value={value}
              step={step}
              style={styles.slider}
              minimumValue={35}
              maximumValue={1000}
              thumbTintColor={colors.mainColor}
              onValueChange={this.onValueChange}
              minimumTrackTintColor={colors.mainColor}
              maximumTrackTintColor={colors.mainColor}
            />
            <View style={styles.labelContainer}>
              <Text style={styles.sliderLabel}>
                35$
              </Text>
              <Text style={styles.sliderLabel}>
                1000$
              </Text>
            </View>
          </View>
          <View style={styles.section}>
            <View style={styles.row}>
              <Text style={[styles.header, { paddingLeft: 0 }]}>Date</Text>
              <Center style={{ flexDirection: 'row' }}>
                <Text>Months</Text>
                <Switch
                  onTintColor="red"
                  tintColor="orange"
                  thumbTintColor="red"
                  value={checkboxIsDaySelected}
                  onValueChange={this.onSwitchChange}
                />
                <Text>Days</Text>
              </Center>
            </View>
            <ModalToggler onPress={this.toggleMonthModal}>
              <Text style={styles.textValue}>{monthSelected.toUpperCase()}</Text>
              {/*
              <GradientText
                text={monthSelected.toUpperCase()}
                size={36}
                width={dims.SCREEN_WIDTH*.6}
                fontWeight="500"
                startColor="#320edc"
                endColor="#19baf7"
              />
              */}
              <Icon
                name="chevron-down"
                size={30}
                style={{ left: 10 }}
                color="#19baf7"
              />
            </ModalToggler>
            <Center>
              <CheckBox
                style={{ flex: 1, padding: 10 }}
                label="Weekends only"
                checked={checked}
                onChange={this.onCheckboxChange}
              />
            </Center>
          </View>
          <View style={styles.section}>
            <Text style={styles.header}>Airport</Text>
            <ModalToggler onPress={this.toggleAirportModal}>
              <Text style={styles.textValue}>{airportCodeSelected}</Text>
              {/*
              <GradientText
                text={airportCodeSelected}
                size={36}
                width={dims.SCREEN_WIDTH*.6}
                fontWeight="500"
                startColor="#320edc"
                endColor="#19baf7"
              />
              */}
              <Icon
                name="chevron-down"
                size={30}
                style={{ left: 10 }}
                color="#19baf7"
              />
            </ModalToggler>
            <Text style={styles.upperTitle}>
              {airportSelected}
            </Text>
          </View>
          <View style={styles.section}>
            <Text style={styles.header}>Topic</Text>
            <ScrollView horizontal>
              {this.renderTopics()}
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = ({ sessionGradient }) => ({
  isNearestAirportLoading: sessionGradient.isNearestAirportLoading,
  nearestAirportList: sessionGradient.nearestAirportList,
});

const mapDispatchToProps = dispatch => ({
  offlineActions: bindActionCreators(offlineActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchOptionsLayout);
