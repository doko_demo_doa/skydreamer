import React, { PureComponent } from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';
import {
  colors,
  dims,
} from 'skydreamer/config';
import {
  Setting,
  AgeSelector,
} from 'skydreamer/components/ProfileOptionsLayout';
import {
  Label,
} from 'skydreamer/components/SearchOptionsLayout';
import { TopBarTitle } from 'skydreamer/components/common/TopTabBar';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    marginTop: dims.topTabBar, // height of TopBar
    paddingBottom: dims.topTabBar,
  },
  logout: {
    fontFamily: 'Poppins',
    color: colors.profileColor,
    fontSize: dims.titleFontSize,
  },
  header: {
    color: '#000000',
    fontFamily: 'Poppins',
    fontSize: dims.titleFontSize,
  },
  label: {
    fontFamily: 'Poppins',
    color: '#918d9c',
    fontSize: dims.labelFontSize,
  },
  section: {
    flex: 1,
    padding: 25,
    paddingRight: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  scrollContainer: {
    backgroundColor: '#fff',
    borderRadius: 10,
    flexDirection: 'column',
  },
});

export default class ProfileOptionsLayout extends PureComponent {

  static renderNavigationBar = props => (
    <TopBarTitle title="Settings" colors={colors.gradients.profileSection} />
  );

  state = {
    men: false,
    women: false,
    ageMin: 18,
    ageMax: 40,
    buddies: true,
    messages: true,
    match: true,
    booking: true,
  }

  render() {
    return (
      <View
        style={styles.container}
      >
        <ScrollView style={styles.scrollContainer}>
          <View style={styles.section}>
            <Text style={styles.header}>Show</Text>
            <Setting text="Show me on buddies" value={this.state.buddies} />
            <Setting text="Men" value={this.state.men} />
            <Setting text="Women" value={this.state.women} />
            <AgeSelector ageMin={this.state.ageMin} ageMax={this.state.ageMax} />
          </View>
          <View style={styles.section}>
            <Text style={styles.header}>Notifications</Text>
            <Setting text="New messages" value={this.state.messages} />
            <Setting text="New Match" value={this.state.match} />
            <Setting text="Booking Notification" value={this.state.booking} />
          </View>
          <View style={styles.section}>
            <Text style={styles.header}>Contact us</Text>
            <Text style={styles.header}>Legal</Text>
            <Label text="License" />
            <Label text="Privacy" />
            <Label text="Terms of use" />
          </View>
          <View style={styles.section}>
            <TouchableOpacity style={{ flexDirection: 'row' }}>
              <Icon
                name="sign-out"
                color={colors.profileColor}
                size={20}
                style={{
                  paddingRight: 5,
                }}
              />
              <Text style={styles.logout}>Logout</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ flexDirection: 'row' }}>
              <Icon
                name="times"
                color={colors.profileColor}
                size={20}
                style={{
                  paddingRight: 5,
                }}
              />
              <Text style={styles.logout}>Delete profile</Text>
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 25 }} />
        </ScrollView>
      </View>
    );
  }
}
