import React, { Component } from 'react';
import styled from 'styled-components/native';
import { colors, dims } from 'skydreamer/config';

import { TopBarGallery } from 'skydreamer/components/common/TopTabBar';
import {
  FlightSelectorLayout,
} from 'skydreamer/components/TravelAttractionFeed';
import {
  EventList,
  HotelList,
  PanoramaList,
} from 'skydreamer/components/TravelAttractionFeed/GeneralTravel';

const Container = styled.View`
  flex: 1;
`;

export default class TravelAttractionFeed extends Component {

  static propTypes = {

  };

  static defaultProps = {

  };

  constructor(props) {
    super(props);
    console.log('TravelAttractionFeed props', props);
  }

  state = {
    layout: 'plane',
  };

  renderCorrectLayout = () => {
    switch (this.state.layout) {
      case 'plane':
        return <FlightSelectorLayout />;
      case 'mountain-range':
        return <PanoramaList />;
      case 'local_hotel':
        return <HotelList />;
      case 'local_offer':
        return <EventList />;

      default:
        console.error(`Wrong layout ${this.state.layout}
                      at renderCorrectLayout@TravelAttractionFeed`);
    }
  }

  setSelectedLayout = (layout) => {
    this.setState({
      layout,
    });
  }

  render() {
    return (
      <Container>
        <TopBarGallery setSelectedLayout={this.setSelectedLayout} />
        {this.renderCorrectLayout()}
      </Container>
    );
  }
}
