import React, { PureComponent } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import {
  colors,
  dims,
} from 'skydreamer/config';
import {
  Setting,
  AgeSelector,
} from 'skydreamer/components/ProfileOptionsLayout';
import {
  Label,
} from 'skydreamer/components/SearchOptionsLayout';
import {
  DestinationImage,
  FlightHeader,
  Hotel,
} from 'skydreamer/components/ResumeLayout';
import {
  GradientText,
  Row,
  GroupSection,
} from 'skydreamer/components/common';
import { TopBarTitle } from 'skydreamer/components/common/TopTabBar';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    marginTop: dims.topTabBar,
    marginBottom: dims.topTabBar,
  },
  deleteProfile: {
    fontFamily: 'Poppins',
    color: '#c18989',
    fontSize: dims.chatFontSize,
  },
  header: {
    color: '#000000',
    alignSelf: 'flex-start',
    fontFamily: 'Poppins',
    fontSize: dims.titleFontSize,
    paddingBottom: 5,
  },
  label: {
    fontFamily: 'Poppins',
    color: '#918d9c',
    fontSize: 15,
  },
  section: {
    flex: 1,
    padding: 25,
    paddingRight: 60,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  scrollContainer: {
    backgroundColor: '#fff',
    flexDirection: 'column',
  },
  groupContainer: {
    margin: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default class ResumeLayout extends PureComponent {

  static renderNavigationBar = props => (
    <TopBarTitle title="Resume" colors={colors.gradients.likedcardsSection} />
  );

  state = {
    destination: 'Mykonos',
    country: 'Greece',
    image: 'https://cdn.getyourguide.com/niwziy2l9cvz/GSQ6Mre2aaK42aSUyYeme/ee5c81d58448ecbf50f4818e374f4784/mykonos-1112x630__2_.jpg',
    departure: {
      airport: 'VIE',
      city: 'Vienna',
      time: '10:30 AM',
    },
    arrival: {
      airport: 'JMK',
      city: 'Mykonos',
      time: '11:30 AM',
    },
    hotel: {
      name: 'Lexington Hotel',
      rating: 4,
      image: 'https://media-cdn.tripadvisor.com/media/photo-s/08/d4/a2/2a/antusa-palace-hotel.jpg',
    },
    booked: [
      'https://randomuser.me/api/portraits/men/10.jpg',
      'https://randomuser.me/api/portraits/women/10.jpg',
    ],
    notBooked: [
      'https://randomuser.me/api/portraits/women/14.jpg',
    ],
  }

  render() {
    return (
      <View
        style={styles.container}
      >
        <ScrollView style={styles.scrollContainer}>
          <DestinationImage
            image={this.state.image}
            city={this.state.destination}
            country={this.state.country}
          />
          <FlightHeader
            departureAcronym={this.state.departure.airport}
            arrivalAcronym={this.state.arrival.airport}
            departureCity={this.state.departure.city}
            arrivalCity={this.state.arrival.city}
            departureTime={this.state.departure.time}
            arrivalTime={this.state.arrival.time}
          />
          <View style={styles.section}>
            <Text style={styles.header}>Hotel</Text>
            <Hotel
              uri={this.state.hotel.image}
              name={this.state.hotel.name}
              rating={this.state.hotel.rating}
              onPress={() => Actions.bookingDetail()}
              onPhotoPress={() => Actions.gallery()}
            />

          </View>
          <View style={styles.section}>
            <Text style={styles.header}>Booked</Text>
            <GroupSection style={styles.groupContainer} size="medium" avatarUris={this.state.booked} />
          </View>
          <View style={styles.section}>
            <Text style={styles.header}>Not Booked</Text>
            <GroupSection style={styles.groupContainer} size="medium" avatarUris={this.state.notBooked} />
          </View>

        </ScrollView>
      </View>
    );
  }
}
