import React from 'react';
import MapView from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome';
import { InfoButton } from 'skydreamer/components/SwipeCardLayout';
import {
  TopBarTitle,
} from 'skydreamer/components/common/TopTabBar';
import {
    View,
    Text,
    TouchableOpacity,
    Animated,
    Dimensions,
    ScrollView,
} from 'react-native';
import Scaling from 'skydreamer/Scaling';
import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';
import {
  TextHeader,
  MenuHeader,
  ImageWrapper,
}
from 'skydreamer/components/TravelAttractionFeed/GeneralTravel';
import menu from 'skydreamer/components/TravelAttractionFeed/GeneralTravel/menu';
import {
  colors,
  dims,
} from 'skydreamer/config';
const TopShadowGradient = styled(LinearGradient)`
  position: absolute;
  top: 0;
  width: ${dims.SCREEN_WIDTH};
  height: ${dims.SCREEN_WIDTH};
  padding-left: 15;
  padding-right: 15;
  border-radius: 5;
`;
const Description = styled.Text`
  flex: 1;
  padding-left: 30;
  padding-right: 30;
  color: #aca9b4;
  padding-bottom: 20;
  font-family: Poppins-Regular;
  font-size: ${dims.simpleLabelFontSize};
`;

const MenuWrapper = styled.View`
  position: absolute;
  top: 20;
  left: 22.5;
  right: 22.5;
`;

export default class GrouponOfferLayout extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ImageWrapper uris="http://www.accorhotels.com/photos/7540_hombhpa_00_p_1000x355.jpg" />
        {/* Todo: render shadow at the very top */}
        <TopShadowGradient colors={['rgba(30,30,30,0.5)', 'rgba(0,0,0,0)']} />

        <MenuWrapper>
          <MenuHeader
            onPressMenu={this.toggleContextMenu}
          />
          {/* remove this
                this.state.toggleMenu &&
                  <ContextMenu
                    menu={menu}
                  />
              */}
        </MenuWrapper>

        <TextHeader
          title="Getaway at a Glance: Sybaris - Downers Grove, IL"
          price="129"
          moneySymbol="$"
          type="events"
        />

        <Description>{'Sybaris is all about romance. This adult-couples-only getaway resort in the Chicagoland area features suites with whirlpools inside them, as well as a romance-focused atmosphere. Named for the ancient Greek city of Sybaris, where luxury and pleasure abounded, this off-the-beaten-path getaway has been a Chicago-area favorite for couples since 1974.'}</Description>

      </View>
    );
  }
}

const TravelInfo = ({ label, value }) => (
  <View style={styles.travelInfoContainer}>
    <View style={{ flex: 21, justifyContent: 'center' }}>
      <Text style={{ marginLeft: Scaling.vertical(10), fontSize: Scaling.vertical(16), fontWeight: 'bold', color: 'black' }}>{label}</Text>
    </View>
    <View style={{ flex: 79, justifyContent: 'center' }}>
      <Text style={{ fontSize: Scaling.vertical(16) }}>{value}</Text>
    </View>
  </View>
);


const { width, height } = Dimensions.get('window');

const styles = Scaling.newStylesheet({
  container: {
    flex: 1,
  },
  headerContainer: {
    backgroundColor: 'rgb(249, 236, 233)',
  },
  travelInfoContainer: {
    marginBottom: 5,
    backgroundColor: 'white',
    borderRadius: 14,
    height: 36,
    marginHorizontal: 40,
    flexDirection: 'row',
  },
  infoContainerSpacer: {
    marginTop: height * 0.4,
  },
  infoContainer: {
    padding: 20,
    marginLeft: width * ((1 - 0.85) / 2),
    width: width * 0.85,
    borderRadius: 12,
    backgroundColor: 'white',
  },
  headerLabel: {
    fontSize: 30,
    color: 'black',
    fontWeight: 'bold',
  },
  description: {
    fontSize: 15,
    lineHeight: 22,
    color: 'rgb(183, 181, 191)',
  },
});
