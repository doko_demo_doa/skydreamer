import React, { Component } from 'react';
import {
  Animated,
  Easing,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import styled from 'styled-components/native';
import { Actions } from 'react-native-router-flux';

import {
  Row,
} from 'skydreamer/components/common';
import {
  ImageGradient,
  MatchedFriends,
  PulseLoader,
} from 'skydreamer/components/MatchLayout';
import Scaling from 'skydreamer/Scaling';
import {
  Container,
} from '../common';


const Overlay = styled.View`
  flex: 1;
  align-items: center;
  z-index: 2;
  bottom: 0;
`;

const BottomRow = styled.View`
  width: 100%;
  height: 50;
  flex-direction: row;
  justify-content: center;
`;

const styles = Scaling.newStylesheet({
  container: {
    position: 'absolute',
    top: 0,
  },
  animationContainer: {
    position: 'absolute',
    top: 0,
    zIndex: 1,
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    /*
    borderColor: '#ffffff',
    borderTopWidth: 2,
    borderRightWidth: 1,
    */
  },
  buttonText: {
    color: '#FFF',
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'Poppins',
  },
  text: {
    color: '#FFF',
    paddingTop: 20,
    paddingBottom: 20,
    fontSize: 32,
    fontWeight: 'bold',
    fontFamily: 'Poppins',
    textAlign: 'center',
  },
  destinationMatchText: {
    position: 'absolute',
    color: '#FFF',
    paddingTop: 20,
    paddingBottom: 20,
    fontSize: 32,
    fontWeight: 'bold',
    fontFamily: 'Poppins',
    bottom: 0,
    paddingHorizontal: 100,
    textAlign: 'center',
  },
});

export default class MatchLayout extends Component {

  scale = new Animated.Value(0);

  state = {
    scale: 200,
    city: 'Palermo',
    avatarUris: [
      'https://scontent-mxp1-1.xx.fbcdn.net/v/t1.0-9/13428505_10153896162989055_371299808813733366_n.jpg',
      'https://scontent-mxp1-1.xx.fbcdn.net/v/t1.0-9/15781065_1061916943917128_8507023061097029582_n.jpg',
      'https://scontent-mxp1-1.xx.fbcdn.net/v/t31.0-8/s960x960/17212160_10208362369118743_3871134872990748355_o.jpg',
    ],
    backgroundUri: 'http://top10travel.online/wp-content/uploads/thumbs_dir/terrace-1030758_1280-n26efitt5qh74xgnubr00ss1xeud64mh3ceq5c61fa.jpg',
  }

  componentWillMount() {
    this.animateCircle();
  }

  animateCircle = () => {
    Animated.timing(
      this.scale,
      {
        toValue: 200,
        duration: 2000,
        easing: Easing.linear,
        useNativeDriver: true,
      },
    ).start((animation) => {
      if (animation.finished) {
        this.scale.setValue(0);
        this.animateCircle();
      }
    });
  }

  onPressChat = () => {
    alert('onPressChat');
  }

  onPressSwipeMore = () => {
    Actions.pop();
  }

  render() {
    const {
      city,
      avatarUris,
      backgroundUri,
    } = this.state;
    return (
      <Container styles={{ flex: 1 }}>
        <View style={styles.container}>
          <ImageGradient backgroundUri={backgroundUri} />
        </View>

        <Overlay>
          <PulseLoader
            avatar={backgroundUri}
          />
          <Text style={styles.text}>{city}</Text>
          <MatchedFriends avatarUris={avatarUris} />

          <Text style={styles.destinationMatchText}>Destination Match!</Text>
        </Overlay>

        <BottomRow>
          <TouchableOpacity style={styles.button} onPress={this.onPressChat}>
            <Text style={styles.buttonText}>Chat </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button} onPress={this.onPressSwipeMore}>
            <Text style={styles.buttonText}>Swipe more</Text>
          </TouchableOpacity>
        </BottomRow>
      </Container>
    );
  }
}
