import React, { PureComponent } from 'react';
import {
  View,
  Text,
  Image,
  Dimensions,
  Animated,
  Easing,
  TouchableWithoutFeedback,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import {
  GradientText,
  Row,
  Column,
  GroupSection,
} from 'skydreamer/components/common';
import { TopBarProfileBuddies } from 'skydreamer/components/common/TopTabBar';
import {
  ImageProfile,
  ActionButton,
  ToolBar,
  Tags,
  SettingsButton,
} from 'skydreamer/components/ProfileLayout';
import {
  colors,
  dims,
} from 'skydreamer/config';
import { Container } from '../common';
import Scaling from 'skydreamer/Scaling';


const styles = Scaling.newStylesheet({
  scrollView: {
    flex: 1,
    paddingHorizontal: 20,
    backgroundColor: '#fbf8f7',
    paddingTop: dims.topTabBar,
  },
  scrollViewContentContainer: {
    paddingTop: dims.topTabBar,
    paddingBottom: dims.topTabBar * 2,
  },
  name: {
    fontFamily: 'Poppins-Medium',
    fontSize: 25,
    color: '#000000',
  },
  settings: {
    fontFamily: 'Poppins',
    fontSize: 15,
    color: '#39b25b',
  },
  secondaryText: {
    fontFamily: 'Poppins',
    color: '#a9a8b5',
  },
  groupContainer: {
    padding: 2,
    flex: 1,
  },
  section: {
    flex: 1,
    alignItems: 'flex-start',
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  secondarySection: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e9f3',
  },
  groupSection: {
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scrollContainer: {
    backgroundColor: '#fff',
    borderRadius: 10,
    flexDirection: 'column',
  },
});

// Dummy data, please replace it using state later on.
const tempData = [
  { name: 'John Doe', type: 1, image: 'https://randomuser.me/api/portraits/men/10.jpg' },
  { name: 'John Cena', type: 1, image: 'https://randomuser.me/api/portraits/men/11.jpg' },
  { name: 'David Rock', type: 2, image: 'https://randomuser.me/api/portraits/men/12.jpg' },
  { name: 'Chris Cumberbatch', type: 2, image: 'https://randomuser.me/api/portraits/men/13.jpg' },
  { name: 'Peter Redfield', type: 2, image: 'https://randomuser.me/api/portraits/men/14.jpg' },
  { name: 'Tom Hawkins', type: 3, image: 'https://randomuser.me/api/portraits/men/15.jpg' },
  { name: 'Dominic Torretto', type: 3, image: 'https://randomuser.me/api/portraits/men/16.jpg' },
  { name: 'Leon Copperfield', type: 4, image: 'https://randomuser.me/api/portraits/men/17.jpg' },
  { name: 'Nathan Drake', type: 4, image: 'https://randomuser.me/api/portraits/men/18.jpg' },
  /*{ name: 'Melissa Arc', type: 4, image: 'https://randomuser.me/api/portraits/women/63.jpg' },
  { name: 'Harry Layton', type: 4, image: 'https://randomuser.me/api/portraits/men/20.jpg' },
  { name: 'Luke Trions', type: 3, image: 'https://randomuser.me/api/portraits/men/21.jpg' },*/
];

export default class BuddiesLayout extends PureComponent {
  static renderNavigationBar = (props) => {
    console.log('ProfileLayout.renderNavigationBar', props);
    return (
      <TopBarProfileBuddies />
    );
  }

  constructor(props) {
    super(props);
    this.state = {
      showDraggable: true,
      dropZoneValues: null,
      pan: new Animated.ValueXY(),
    };
  }
  render() {
    return (
      <Container>
        <View style={{ marginTop: 20, marginHorizontal: 8, flex: 1, flexDirection: 'row', backgroundColor: '#fff' }}>
          {tempData.map((data, index) => <Bubble data={data} index={index} />)}
        </View>
      </Container>
    );
  }
}

class Bubble extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      animating: true,
    };

    const { index } = this.props;
    console.log(index);
    this.initX = Dimensions.get('window').width + 80 + (index * 80); // this.getRandomNumber(Dimensions.get('window').width + 30, Dimensions.get('window').width + 200);
    this.initY = this.getRandomNumber(30, Dimensions.get('window').height - 150);
    this.pos = new Animated.ValueXY({ x: this.initX, y: this.initY });

    this.anim = Animated.timing(this.pos, {
      toValue: { x: -120 - (Dimensions.get('window').width + ((tempData.length - index) * 30)), y: this.getRandomNumber(50, Dimensions.get('window').height - 300) },
      duration: 9000 - ((tempData.length - index) * 200), // this.getRandomNumber(5000, 9000),
      easing: Easing.ease,
    });
  }

  componentDidMount() {
    this.move();
  }

  getRandomNumber = (min, max) => {
    return Math.random() * (max - min) + min;
  }

  move = () => {
    const { index } = this.props;
    //const initX = Dimensions.get('window').width + 80 + (index * 40);// this.getRandomNumber(Dimensions.get('window').width + 30, Dimensions.get('window').width + 200);
    //const initY = this.getRandomNumber(30, Dimensions.get('window').height - 120);
    this.pos.setValue({ x: this.initX, y: this.initY });
    this.anim.start(() => this.state.animating ? this.move() : null);
  }

  stop = () => {
    this.pos.stopAnimation();
  }

  render() {
    const { data } = this.props;
    let color = '#C62828';
    switch (data.type) {
      case 1:
        color = '#C62828';
        break;
      case 2:
        color = '#1976D2';
        break;
      case 3:
        color = '#C0CA33';
        break;
      case 4:
        color = '#00C853';
        break;
      default:
        color = '#C62828';
        break;
    }

    return (
      <LinearGradient
        start={{ x: 0.0, y: 0.25 }} end={{ x: 0.5, y: 1.0 }}
        locations={[0, 0.5, 0.6]}
        colors={['#4c669f', '#3b5998', '#192f6a']}
      >
        <TouchableWithoutFeedback onPress={() => Actions.profile()}>
        <Animated.View style={{ padding: 2, position: 'absolute', top: this.pos.y, left: this.pos.x }}>
          <Image
            style={{ width: 100, height: 100, borderRadius: 50, borderWidth: 4, borderColor: color }}
            source={{ uri: data.image }}
          />
        </Animated.View>
        </TouchableWithoutFeedback>
      </LinearGradient>
    );
  }
}