import React, { PropTypes } from 'react';
import {
  View,
  StyleSheet,
  Text,
} from 'react-native';
import Scaling from 'skydreamer/Scaling';
import {
  colors,
  dims
} from 'skydreamer/config';

const styles = Scaling.newStylesheet({
  container: {
    paddingHorizontal: 5,
    flexDirection: 'row',
    alignSelf: 'flex-start',
  },
  text: {
    fontFamily: 'Poppins',
    color: '#918d9c',
    fontSize: dims.labelFontSize,
  },
});

const Label = ({ text }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}> {text} </Text>
    </View>
  );
};
Label.propTypes = {
  text: PropTypes.string.isRequired,
};


export default Label;
