import React, { PropTypes } from 'react';
import {
  View,
} from 'react-native';

import { Avatar, AddGroupButton } from 'skydreamer/components/common';
import Scaling from 'skydreamer/Scaling';

const styles = Scaling.newStylesheet({
  container: {
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const GroupSection = ({ avatarUris }) => {
  console.log('avatarUris@GroupSection', avatarUris);
  return (
    <View style={styles.container}>
      {
        avatarUris.map((uri, i) => (
          <Avatar
            key={i}
            uri={uri}
            size="small"
          />
        ))
      }
      <AddGroupButton
        size={15}
        onTouch={() => {}}
      />

    </View>
  );
}

GroupSection.propTypes = {
  avatarUris: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default GroupSection;
