import React, { Component } from 'react';
import {

} from 'react-native';

export default class AirportSection extends Component {
  render() {
    return (
      <Container>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.textValue}>AMS</Text>
          <Icon
            name="chevron-down"
            size={30}
            onPress={this.toggleModal}
            style={{ bottom: -40, left: 20, color: 'white' }}
          />
        </View>
        <Text style={styles.upperTitle}>
          Amsterdam
        </Text>
      <Container>
    );
  }
}
