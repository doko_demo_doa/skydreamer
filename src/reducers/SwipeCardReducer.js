import {
  WAITING_GET_CARDS_RESPONSE,
  RESPONSE_GET_CARDS,
  ERROR_GET_CARDS,

  WAITING_GET_MATCH_RESPONSE,
  RESPONSE_MATCH_CARDS,
  ERROR_MATCH_CARDS,
} from 'skydreamer/actions/types';
import { Image } from 'react-native';

cardList = [{
  "card_id": 6,
  "title": "Paris",
  "subtitle": "France",
  "type": "flight",
  "lat": "48.866667",
  "long": "2.333333",
  "destAirLat": "27.69722200",
  "destAirLong": "85.35777800",
  "distanceToLocation": 0,
  "image": "https://skydreamer-cards.s3.amazonaws.com/754720d2b8dbeb4df6b1e26094da79c9",
  "flight_details": {
    "id": "f42183cc4c9faefe2abdc945648dca79",
    "price": 175,
    "departDate": "13/10",
    "returnDate": "15/10"
  },
  "userVotes": {
    "count": 7,
    "friend_pic": "https://scontent.xx.fbcdn.net/v/t1.0-1/p100x100/17342499_1793460577639983_4289694647197547059_n.jpg?oh=1b6856c2f784aaef84086caa78f8b8a8&oe=59835753"
  },
  "isMatch": false,
  "homeaway": {
    "headline": "Studio Paris-Montmartre. Parisian romanticism.",
    "description": "Idealy located, Tastefully decorated, Completely renovated",
    "images": [
      "http://placehold.it/128",
      "http://placehold.it/256",
      "http://placehold.it/512"
    ]
  }
},
{
  "card_id": 18,
  "title": "Kamppi",
  "subtitle": "Finland",
  "type": "flight",
  "lat": "60.168333",
  "long": "24.9325",
  "destAirLat": "48.85666700",
  "destAirLong": "2.35083300",
  "distanceToLocation": 0.50092254039527,
  "image": "https://skydreamer-cards.s3.amazonaws.com/8d57eb8edecf340b61725eb286cd04f3",
  "flight_details": {
    "id": "9460fd02acc6e61b0b8656a22578d42a",
    "price": 185,
    "departDate": "1/10",
    "returnDate": "9/10"
  },
  "userVotes": null,
  "isMatch": false,
  "homeaway": {
    "headline": "Studio Paris-Montmartre. Parisian romanticism.",
    "description": "Idealy located, Tastefully decorated, Completely renovated",
    "images": [
      "http://placehold.it/128",
      "http://placehold.it/256",
      "http://placehold.it/512"
    ]
  }
},
{
  "card_id": 20,
  "title": "Murcia",
  "subtitle": "Spain",
  "type": "flight",
  "lat": "37.987041",
  "long": "-1.130042",
  "destAirLat": "60.17083300",
  "destAirLong": "24.93750000",
  "distanceToLocation": 0.85101203634773,
  "image": "https://skydreamer-cards.s3.amazonaws.com/c68897f06b8f2ff0c9c6353522545ae5",
  "flight_details": {
    "id": "fe0c63e4bf469396de5cb8a04e7750d9",
    "price": 182,
    "departDate": "2/10",
    "returnDate": "5/10"
  },
  "userVotes": {
    "count": 2,
    "friend_pic": "https://scontent.xx.fbcdn.net/v/t1.0-1/p100x100/17342499_1793460577639983_4289694647197547059_n.jpg?oh=1b6856c2f784aaef84086caa78f8b8a8&oe=59835753"
  },
  "isMatch": false,
  "homeaway": {
    "headline": "Studio Paris-Montmartre. Parisian romanticism.",
    "description": "Idealy located, Tastefully decorated, Completely renovated",
    "images": [
      "http://placehold.it/128",
      "http://placehold.it/256",
      "http://placehold.it/512"
    ]
  }
},
{
  "card_id": 23,
  "title": "Sint-Joost",
  "subtitle": "Belgium",
  "type": "flight",
  "lat": "50.85",
  "long": "4.35",
  "destAirLat": "38.34527800",
  "destAirLong": "-0.48305600",
  "distanceToLocation": 2.5873323214132,
  "image": "https://skydreamer-cards.s3.amazonaws.com/0075eacb7e71fe7e337c39dfc923743b",
  "flight_details": {
    "id": "1db2b38b7ffa24dcaa8555f865826f1c",
    "price": 186,
    "departDate": "21/10",
    "returnDate": "23/10"
  },
  "userVotes": null,
  "isMatch": false,
  "homeaway": {
    "headline": "Studio Paris-Montmartre. Parisian romanticism.",
    "description": "Idealy located, Tastefully decorated, Completely renovated",
    "images": [
      "http://placehold.it/128",
      "http://placehold.it/256",
      "http://placehold.it/512"
    ]
  }
},
{
  "card_id": 27,
  "title": "Athens",
  "subtitle": "Greece",
  "type": "flight",
  "lat": "37.983333",
  "long": "23.733333",
  "destAirLat": "50.85017400",
  "destAirLong": "4.36431900",
  "distanceToLocation": 0,
  "image": "https://skydreamer-cards.s3.amazonaws.com/cfb9895222e85246e77a86d72861076c",
  "flight_details": {
    "id": "51a312412557e190b9b189a715931a75",
    "price": 166,
    "departDate": "20/10",
    "returnDate": "27/10"
  },
  "userVotes": {
    "count": 4,
    "friend_pic": "https://scontent.xx.fbcdn.net/v/t1.0-1/p100x100/17342499_1793460577639983_4289694647197547059_n.jpg?oh=1b6856c2f784aaef84086caa78f8b8a8&oe=59835753"
  },
  "isMatch": false,
  "homeaway": {
    "headline": "Studio Paris-Montmartre. Parisian romanticism.",
    "description": "Idealy located, Tastefully decorated, Completely renovated",
    "images": [
      "http://placehold.it/128",
      "http://placehold.it/256",
      "http://placehold.it/512"
    ]
  }
},
{
  "card_id": 30,
  "title": "Oslo",
  "subtitle": "Norway",
  "type": "flight",
  "lat": "59.916667",
  "long": "10.75",
  "destAirLat": "37.89791600",
  "destAirLong": "23.73055500",
  "distanceToLocation": 0,
  "image": "https://skydreamer-cards.s3.amazonaws.com/218d475e854dbf90296a5d0dd8470a1d",
  "flight_details": {
    "id": "9f436d817afdfd293f1482d08a3b34e3",
    "price": 165,
    "departDate": "19/10",
    "returnDate": "24/10"
  },
  "userVotes": null,
  "isMatch": false,
  "homeaway": {
    "headline": "Studio Paris-Montmartre. Parisian romanticism.",
    "description": "Idealy located, Tastefully decorated, Completely renovated",
    "images": [
      "http://placehold.it/128",
      "http://placehold.it/256",
      "http://placehold.it/512"
    ]
  }
},
{
  "card_id": 31,
  "title": "Garbatella",
  "subtitle": "Italy",
  "type": "flight",
  "lat": "41.866667",
  "long": "12.483333",
  "destAirLat": "59.91273000",
  "destAirLong": "10.74609000",
  "distanceToLocation": 0,
  "image": "https://skydreamer-cards.s3.amazonaws.com/8286e85a0163162e03d237988be5b16f",
  "flight_details": {
    "id": "95cba8bcea129630de67a5a330dfc076",
    "price": 154,
    "departDate": "21/10",
    "returnDate": "23/10"
  },
  "userVotes": null,
  "isMatch": false,
  "homeaway": {
    "headline": "Studio Paris-Montmartre. Parisian romanticism.",
    "description": "Idealy located, Tastefully decorated, Completely renovated",
    "images": [
      "http://placehold.it/128",
      "http://placehold.it/256",
      "http://placehold.it/512"
    ]
  }
},
{
  "card_id": 2697,
  "title": "Rincón de Loix",
  "subtitle": "Spain",
  "type": "flight",
  "lat": "38.535261",
  "long": "-0.106109",
  "destAirLat": "41.89474000",
  "destAirLong": "12.48390000",
  "distanceToLocation": 0.26683690218172,
  "image": "https://skydreamer-cards.s3.amazonaws.com/5e412c5b84fd0283e97e1155a8fa02ef",
  "flight_details": {
    "id": "fe0c63e4bf469396de5cb8a04e7750d9",
    "price": 182,
    "departDate": "2/10",
    "returnDate": "5/10"
  },
  "userVotes": null,
  "isMatch": false,
  "homeaway": {
    "headline": "Studio Paris-Montmartre. Parisian romanticism.",
    "description": "Idealy located, Tastefully decorated, Completely renovated",
    "images": [
      "http://placehold.it/128",
      "http://placehold.it/256",
      "http://placehold.it/512"
    ]
  }
},
{
  "card_id": 1,
  "title": "Bucharest",
  "subtitle": "Romania",
  "type": "flight",
  "lat": "44.433333",
  "long": "26.1",
  "destAirLat": "38.34527800",
  "destAirLong": "-0.48305600",
  "distanceToLocation": 0.22902013222257,
  "image": "https://skydreamer-cards.s3.amazonaws.com/c152483758aa9a18f3388fb3000c2c9f",
  "flight_details": {
    "id": "be33d8bd89d93dd7f730118ed29be68f",
    "price": 50,
    "departDate": "11/10",
    "returnDate": "18/10"
  },
  "userVotes": null,
  "isMatch": false,
  "homeaway": {
    "headline": "Studio Paris-Montmartre. Parisian romanticism.",
    "description": "Idealy located, Tastefully decorated, Completely renovated",
    "images": [
      "http://placehold.it/128",
      "http://placehold.it/256",
      "http://placehold.it/512"
    ]
  }
}];

cardList.map(card => {
  Image.prefetch(card.image);
})

const initialState = {
  isCardListLoading: false,
  cardList,
  cardError: null,
  isMatchListLoading: false,
  matchList: null,
  matchError: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case WAITING_GET_CARDS_RESPONSE:
      return Object.assign({}, state, {
        isCardListLoading: true,
      });
    case RESPONSE_GET_CARDS:
      return Object.assign({}, state, {
        cardList: action.data,
        isCardListLoading: false,
      });
    case ERROR_GET_CARDS:
      return Object.assign({}, state, {
        cardError: action.error,
        isCardListLoading: false,
      });

    case WAITING_GET_MATCH_RESPONSE:
      return Object.assign({}, state, {
        isMatchListLoading: true,
      });
    case RESPONSE_MATCH_CARDS:
      return Object.assign({}, state, {
        matchList: action.data,
        isMatchListLoading: false,
      });
    case ERROR_MATCH_CARDS:
      return Object.assign({}, state, {
        matchError: action.error,
        isMatchListLoading: false,
      });
    default:
      return state;
  }
};
