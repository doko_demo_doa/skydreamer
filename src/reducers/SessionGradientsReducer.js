import {
  WAITING_NEAREST_AIRPORT_RESPONSE,
  RESPONSE_NEAREST_AIRPORT,
  ERROR_NEAREST_AIRPORT,

  WAITING_TRAVEL_INTENT_RESPONSE,
  RESPONSE_TRAVEL_INTENT,
  ERROR_TRAVEL_INTENT,
} from 'skydreamer/actions/types';

const initialState = {
  isNearestAirportLoading: false,
  nearestAirportList: null,
  nearestAirportError: null,
  isTravelIntentLoading: false,
  travelIntent: null,
  travenIntentError: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case WAITING_NEAREST_AIRPORT_RESPONSE:
      return Object.assign({}, state, {
        isNearestAirportLoading: true,
      });
    case RESPONSE_NEAREST_AIRPORT:
      return Object.assign({}, state, {
        nearestAirportList: action.data,
        isNearestAirportLoading: false,
      });
    case ERROR_NEAREST_AIRPORT:
      return Object.assign({}, state, {
        nearestAirportError: action.error,
        isNearestAirportLoading: false,
      });

    case WAITING_TRAVEL_INTENT_RESPONSE:
      return Object.assign({}, state, {
        isTravelIntentLoading: true,
      });
    case RESPONSE_TRAVEL_INTENT:
      return Object.assign({}, state, {
        travelIntent: action.data,
        isTravelIntentLoading: false,
      });
    case ERROR_TRAVEL_INTENT:
      return Object.assign({}, state, {
        travenIntentError: action.error,
        isTravelIntentLoading: false,
      });
    default:
      return state;
  }
};
