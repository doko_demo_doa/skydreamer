import {
  WAITING_GET_USER_GROUPS_LIST_RESPONSE,
  RESPONSE_GET_USER_GROUPS_LIST,
  ERROR_GET_USER_GROUPS_LIST,
} from 'skydreamer/actions/types';

const initialState = {
  isUserGroupsListLoading: false,
  userGroupsList: null,
  userGroupsError: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case WAITING_GET_USER_GROUPS_LIST_RESPONSE:
      return Object.assign({}, state, {
        isUserGroupsListLoading: true,
      });
    case RESPONSE_GET_USER_GROUPS_LIST:
      return Object.assign({}, state, {
        userGroupsList: action.data,
        isUserGroupsListLoading: false,
      });
    case ERROR_GET_USER_GROUPS_LIST:
      return Object.assign({}, state, {
        userGroupsError: action.error,
        isUserGroupsListLoading: false,
      });

    default:
      return state;
  }
};
