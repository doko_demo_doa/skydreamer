import {
  SET_SESSION_GRADIENT_OFFLINE,
} from 'skydreamer/actions/types';

const initialState = {
  sessionGradient: {
    group_name: null,
    is_group_travel: true,
    price_amount: 220,
    departure: Date.now(),
    departure_airport_id: null,
    interested_topic_ids_arr: [],
    type: 'month',
    currency: 'EUR',
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_SESSION_GRADIENT_OFFLINE:
      return Object.assign({}, state, {
        sessionGradient: Object.assign({}, state.sessionGradient, action.data),
      });
    default:
      return state;
  }
};
