import {
  WAITING_GET_GALLERY_PANORAMA_POSTS_RESPONSE,
  RESPONSE_GET_GALLERY_PANORAMA_POSTS,
  ERROR_GET_GALLERY_PANORAMA_POSTS,
} from 'skydreaer/actions/types';

const initialState = {
  isPanoramaListLoading: false,
  panoramaList: null,
  panoramaError: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case WAITING_GET_CARDS_RESPONSE:
      return Object.assign({}, state, {
        isPanoramaListLoading: true,
      });
    case RESPONSE_GET_CARDS:
      return Object.assign({}, state, {
        panoramaList: action.data,
        isPanoramaListLoading: false,
      });
    case ERROR_GET_CARDS:
      return Object.assign({}, state, {
        panoramaError: action.error,
        isPanoramaListLoading: false,
      });

    default:
      return state;
  }
};
