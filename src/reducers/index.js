/*
 * Created on Wed Feb 15 2017
 *
 * Description: Combine every reducer of the application
 * Notes:
 * Author:      Guilherme Borges Bastos
 * Revisions:
 *     Date:
 *     Name:
 *     Description:
 *
 * Copyright (c) 2017 Skydreamer B.V -Salamancapad 297 3584, Utrecht (NL)
 */

/**
* @providesModule skydreamer/redux/reducers
*/

import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import FriendSelectionReducer from './FriendSelectionReducer';
import FacebookApiReducer from './FacebookApiReducer';
import OfflineReducer from './OfflineReducer';
import SessionGradientsReducer from './SessionGradientsReducer';
import ChannelReducer from './ChannelReducer';
import SwipeCardReducer from './SwipeCardReducer';
import GroupReducer from './GroupReducer';

export default combineReducers({
  auth: AuthReducer,
  selectedFriendId: FriendSelectionReducer,
  facebook: FacebookApiReducer,
  offline: OfflineReducer,
  sessionGradient: SessionGradientsReducer,
  channels: ChannelReducer,
  swipeCard: SwipeCardReducer,
  group: GroupReducer,
});
