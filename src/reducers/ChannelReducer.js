// TODO: refactor this mess
import {
    SELECT_CHANNEL,
    SET_CHANNEL,
    REMOVE_CHANNEL,
    SET_CHANNEL_SYNC_COMPLETE,
    SET_CHANNEL_CURSOR,
    NEW_MESSAGE_GROUP,
    NEW_MESSAGE,
    FETCH_MESSAGES_START,
    FETCH_MESSAGES_END,
} from 'skydreamer/actions/types';

const initialState = {
  selected: null,
  channels: {},
  channelCursors: {},   // <Key, String>  The cursor for obtaining older messages.
  channelsLoading: {},  // <Key, Boolean> if the chnnael is initially loading
  channelsFetching: {}, // <Key, Boolean> if the channel is fetching older messages
  channelsSynced: {},   // <Key, Boolean> if the channel has loaded all previous messages.
  channelMessages: {},
};

const utilizedTypes = [SELECT_CHANNEL, SET_CHANNEL, REMOVE_CHANNEL, SET_CHANNEL_SYNC_COMPLETE, SET_CHANNEL_CURSOR, NEW_MESSAGE_GROUP, NEW_MESSAGE, FETCH_MESSAGES_START, FETCH_MESSAGES_END];

export default reducer = (prevState = initialState, action) => {
  console.log('action@ChannelReducer', action);
  const { type } = action;
  if (!utilizedTypes.includes(type)) return prevState;

    // Create a complete copy of the redux store.
  const newState = Object.assign({}, prevState);
  const newChannels = Object.assign({}, prevState.channels);
  const newChannelCursors = Object.assign({}, prevState.channelCursors);
  const newChannelsLoading = Object.assign({}, prevState.channelsLoading);
  const newChannelsFetching = Object.assign({}, prevState.channelsFetching);
  const newChannelsSynced = Object.assign({}, prevState.channelsSynced);
  const newChannelMessages = Object.assign({}, prevState.channelMessages);

    // Create a reference to the channel for the key given via the action.
  const prevChannel = prevState.channels[action.key] || {};

    // Extract the channel from the action.
  const { channel } = action;

    // A simple helper function to return the complete new state.
    // Note that the whole state should be new, not just the modified information.
  const applyState = () => {
    newState.channels = newChannels;
    newState.channelCursors = newChannelCursors;
    newState.channelsLoading = newChannelsLoading;
    newState.channelsFetching = newChannelsFetching;
    newState.channelsSynced = newChannelsSynced;
    newState.channelMessages = newChannelMessages;
    return newState;
  };

  switch (type) {
    case SELECT_CHANNEL:
      console.warn('Selected:', action.channel);
      newState.selected = action.channel;
      return applyState();

    case SET_CHANNEL:
      newChannels[action.key] = { ...prevChannel, ...channel, key: action.key };
      if (Object.keys(prevChannel).length === 0 && prevChannel.constructor === Object) {
        console.warn('SET CHANNEL CALLED AND CREATED NEW DATA');
        newChannelsSynced[action.key] = false;
        newChannelsFetching[action.key] = false;
        newChannelsLoading[action.key] = false;
        newChannelMessages[action.key] = [];
      }
      return applyState();

    case REMOVE_CHANNEL:
      delete newChannels[action.key];
      delete newChannelMessages[action.key];
      delete newChannelsFetching[action.key];
      delete newChannelsLoading[action.key];
      return applyState();

    case SET_CHANNEL_SYNC_COMPLETE:
      newChannelsSynced[action.key] = true;
      return applyState();

    case SET_CHANNEL_CURSOR:
      newChannelCursors[action.key] = action.cursor;
      return applyState();

    case NEW_MESSAGE_GROUP:
      for (key in action.messages) {
        newChannelMessages[action.key] = newChannelMessages[action.key].concat(action.messages[key]);
      }
      return applyState();

    case NEW_MESSAGE:
      newChannelMessages[action.key] = newChannelMessages[action.key].concat(action.message);
      return applyState();

    case FETCH_MESSAGES_START:
      newChannelsFetching[action.key] = true;
      return applyState();

    case FETCH_MESSAGES_END:
      newChannelsFetching[action.key] = false;
      return applyState();

  }
};
