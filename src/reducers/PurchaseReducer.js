import {
  WAITING_GET_GROUPON_DEALS_RESPONSE,
  RESPONSE_GROUPON_DEALS_CARDS,
  ERROR_GROUPON_DEALS_CARDS,
  WAITING_GET_PAYMENT_INTENT_RESPONSE,
  RESPONSE_PAYMENT_INTENT_CARDS,
  ERROR_PAYMENT_INTENT_CARDS,
} from 'skydreamer/actions/types';

const initialState = {
  isGrouponDealsLoading: false,
  grouponDealsData: null,
  grouponDealsError: null,
  isPaymentIntentLoading: false,
  paymentIntentData: null,
  paymentIntentError: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case WAITING_GET_GROUPON_DEALS_RESPONSE:
      return Object.assign({}, state, {
        isGrouponDealsLoading: true,
      });
    case RESPONSE_GROUPON_DEALS_CARDS:
      return Object.assign({}, state, {
        grouponDealsData: action.data,
        isGrouponDealsLoading: false,
      });
    case ERROR_GROUPON_DEALS_CARDS:
      return Object.assign({}, state, {
        grouponDealsError: action.error,
        isGrouponDealsLoading: false,
      });

    case WAITING_GET_PAYMENT_INTENT_RESPONSE:
      return Object.assign({}, state, {
        isPaymentIntentLoading: true,
      });
    case RESPONSE_PAYMENT_INTENT_CARDS:
      return Object.assign({}, state, {
        paymentIntentData: action.data,
        isPaymentIntentLoading: false,
      });
    case ERROR_PAYMENT_INTENT_CARDS:
      return Object.assign({}, state, {
        paymentIntentError: action.error,
        isPaymentIntentLoading: false,
      });
    default:
      return state;
  }
};
