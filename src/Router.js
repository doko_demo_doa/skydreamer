import React, { Component } from 'react';
import { Scene, Router } from 'react-native-router-flux';
import { connect } from 'react-redux';
import firebase from 'skydreamer/firebase';
import LoginForm from 'skydreamer/components/LoginForm';
import InternalChat from 'skydreamer/components/pages/Chat/Internal';
import SessionList from 'skydreamer/components/pages/Chat/sessions';
import { TabIcon } from 'skydreamer/components/Router';
import {
  bindActionCreators,
} from 'redux';
import {
  channelActions,
  authActions,
  sessionGradientActions,
} from 'skydreamer/actions';
import {
  ProfileLayout,
  ProfileEditLayout,
  SessionGradient,
  SwipeCardLayout,
  ProfileOptionsLayout,
  TravelAttractionFeed,
  LikedCardsLayout,
  InfoLayout,
  SearchOptionsLayout,
  ResumeLayout,
  BuddiesLayout,
  MatchLayout,
  BookingDetailsLayout,
  LikedUnlikedLayout,
  GroupOptionsLayout,
} from 'skydreamer/components/containers';
import {
  View,
  AsyncStorage,
} from 'react-native';
import { colors } from 'skydreamer/config';

class RouterComponent extends Component {

  state = { isLoading: true, isLoggedIn: false };

  async componentDidMount() {
    const { channelActions, sessionGradientActions, authActions } = this.props;

    firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        this._user = user;
        const currToken = await firebase.auth().currentUser.getToken(false);
        const oldToken = await AsyncStorage.getItem('idToken');
        if (currToken !== oldToken) {
          await authActions.saveTokenToRedux(currToken);
          await authActions.upSertToken(user.uid, currToken);
          // await sessionGradientActions.startNearestAirportFetch();
          channelActions.syncChannels();
          this.setState({ isLoggedIn: true, isLoading: false });
        }
      } else if (this._user) {
          // User was logged out, or deauthorized.
      } else {
          // User does not exist
        // sessionGradientActions.startNearestAirportFetch();
        this.setState({ isLoggedIn: false, isLoading: false });
      }
    });
  }

  render() {
    const { isLoading, isLoggedIn } = this.state;
    if (isLoading) return <View />;

    return (
      <Router key="root">
        <Scene
          key="login"
          component={LoginForm}
          hideNavBar
          initial={!isLoggedIn}
        />

        <Scene
          key="sessionGradient"
          component={SessionGradient}
          initial={isLoggedIn}
          hideNavBar
        />

        <Scene
          key="internalChat"
          component={InternalChat}
          hideNavBar
        />
        <Scene
          key="LikedUnlikedLayout"
          component={LikedUnlikedLayout}
        />

        <Scene
          key="tabbar"
          tabs
          tabBarStyle={{ backgroundColor: '#ffffff', height: 60, paddingTop: 10, elevation: 10 }}
          tabBarSelectedItemStyle={{ backgroundColor: '#ffffff' }}
          initial={isLoggedIn}
        >

          <Scene
            key="tabOne"
            iconName="swipe"
            borderColor={colors.mainColor}
            icon={TabIcon}
            initial
          >
            { /* Scenes that belong to this tab */ }
            <Scene key="cards" component={SwipeCardLayout} initial />
            <Scene key="info" component={InfoLayout} />
            <Scene key="searchOptions" component={SearchOptionsLayout} />
            <Scene
              key="match"
              component={MatchLayout}
              hideNavBar
              hideTabBar
            />
            <Scene key="gallery" component={TravelAttractionFeed} hideNavBar />
          </Scene>

          <Scene
            key="tabTwo"
            iconName="card"
            borderColor={colors.likedCardsColor}
            icon={TabIcon}
          >
            { /* Scenes that belong to this tab */ }
            <Scene key="likedcards" component={LikedCardsLayout} initial />
            <Scene key="resume" component={ResumeLayout} />
            <Scene key="bookingDetail" component={BookingDetailsLayout} />
          </Scene>

          <Scene
            key="tabThree"
            iconName="chat"
            borderColor="purple"
            icon={TabIcon}
          >
            { /* Scenes that belong to this tab */ }
            <Scene key="chat" component={SessionList} initial />
            <Scene
              key="groupOptions"
              component={GroupOptionsLayout}
            />
          </Scene>

          <Scene
            key="tabFour"
            iconName="group"
            borderColor={colors.profileColor}
            icon={TabIcon}
          >
            { /* Scenes that belong to this tab */ }
            <Scene key="profile" component={ProfileLayout} initial />
            <Scene key="profileEdit" component={ProfileEditLayout} />
            <Scene key="buddies" component={BuddiesLayout} />
            <Scene key="profileOptions" component={ProfileOptionsLayout} />
          </Scene>
        </Scene>
      </Router>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  sessionGradientActions: bindActionCreators(sessionGradientActions, dispatch),
  channelActions: bindActionCreators(channelActions, dispatch),
  authActions: bindActionCreators(authActions, dispatch),
});

export default connect(null, mapDispatchToProps)(RouterComponent);