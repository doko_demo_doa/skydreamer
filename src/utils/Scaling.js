/**
* @providesModule skydreamer/Scaling
*/
import { StyleSheet, Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

// Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + (scale(size) - size) * factor;

class Scaling {
  newStylesheet(stylesheet) {
    const obj = {};
    for (key in stylesheet) {
      const item = stylesheet[key];
      if (typeof item === 'object') {
        if (Array.isArray(item)) {
          obj[key] = [];
          for (let i = 0; i < item.length; i++) {
            if (!isNaN(item[i])) {
              obj[key][i] = item[i];
            } else {
              obj[key][i] = item[i];
            }
          }
        } else {
          obj[key] = this.newStylesheet(item);
        }
      } else if (!isNaN(item)) {
        if (key === 'flex') obj[key] = item;
        else if (key === 'lineHeight') obj[key] = Math.floor(item);
        else obj[key] = verticalScale(item);
      } else {
        obj[key] = item;
      }
    }
    return obj;
  }

  vertical(number) { return verticalScale(number); }
}

export default scaling = new Scaling();
