/**
* @providesModule skydreamer/RestClient
*/

import axios from 'axios';

const DEVELOPMENT = true;

const APPEND_DELAY_SIMULATION = () => new Promise((resolve) => {
  if(!DEVELOPMENT) return resolve();
  setTimeout(() => {
    return resolve();
  }, 200 + (Math.floor(Math.random() * 500)));
});


const instance = axios.create({
  baseURL: 'http://development.skydreamer.io',
  timeout: 1500
});

var authToken = null;

export const setAuthToken = (token) => {
  authToken = token;
};


// All API calls should use the instance above and have a function defined below.
// All API calls should use the axios instance.
// All API calls should await APPEND_DELAY_SIMULATION directly after response to simulate slow connections.

export const fetchNearestAirports = (latitude, longitude, range, limit) => new Promise((resolve, reject) => {
  instance.post('/airport/get/nearest', {
    '@Authorization': authToken,
    latitude,
    longitude,
    range,
    limit
  }).then(async (response) => {
    await APPEND_DELAY_SIMULATION();
    const { success, data, message } = response.data;
    if(!success) {
      return reject(message);
    }

    return resolve(data);
  }).catch((error) => {
    return reject(error);
  });
});
