import { Actions } from 'react-native-router-flux';
import { AsyncStorage } from 'react-native';
import firebase from 'skydreamer/firebase';
import FBSDK from 'react-native-fbsdk'; // Start Facebook initialization
// TODO:  @Alberto - Guilherme is receiving an error about this code
// I commented to keep working, probabilly I need to install this on
import { fetchTimeout, formatToUrlEncoded } from 'skydreamer/utils';
import { config } from 'skydreamer/config';
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  LOGIN_FACEBOOK_LOADING,
  LOGIN_FACEBOOK_SUCCESS,
  LOGIN_FACEBOOK_FAIL,
  UPSERT_TOKEN_LOADING,
  UPSERT_TOKEN_RESPONSE,
  UPSERT_TOKEN_ERROR,
  ID_TOKEN_SAVE,
} from './types';

const { timeout, rootUrl } = config.api;

const {
  LoginManager,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} = FBSDK;
const FACEBOOK_PERMISSIONS = ['public_profile', 'email', 'user_location'];
const FIELDS = 'name,gender,locale,location,email';
const GRAPH_REQUEST_PARAMS = {
  httpMethod: 'GET',
  version: 'v2.8',
  // appId: '251686075279620',
  appId: '24b433ce76791790109fca83faa35abe',
  parameters: {
    fields: {
      string: FIELDS,
    },
  },
};
// End Facebook initialization

export const emailChanged = text => ({
  type: EMAIL_CHANGED,
  payload: text,
});

export const passwordChanged = text => ({
  type: PASSWORD_CHANGED,
  payload: text,
});

const upSertTokenWaiting = () => ({
  type: UPSERT_TOKEN_LOADING,
});

const upSertTokenResponse = payload => ({
  type: UPSERT_TOKEN_RESPONSE,
  payload,
});

const upSertTokenError = error => ({
  type: UPSERT_TOKEN_ERROR,
  error,
});

// update the idToken of the user inside of the Slim Authentication System
export const upSertToken = (id_firebase, token) => (
  async (dispatch) => {
    dispatch(upSertTokenWaiting());

    const params = {
      id_firebase,
      token,
    };

    console.log('params@upSertToken', params);

    fetchTimeout(`${rootUrl}/auth/upSertToken`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    }, timeout)
      .then((response) => {
        const { status } = response;
        if (status !== 200) {
          console.log(`Response status: ${status}`);
          let err;
          switch (status) {
            case 401:
              err = 'Unauthorized';
              break;
            case 500:
              err = 'Internal Server Error';
              break;
            default:
              err = 'Unexpected';
          }
          throw new Error(err);
        } else {
          console.log('upSertToken response', response);
          // console.log('response.text', response.text());
          return response.json();
        }
      })
      .then((json) => {
        dispatch(upSertTokenResponse(json));
        console.log('response.json@upSertToken', json);
      })
      .catch((err) => {
        console.log('err@upSertToken', err);
        dispatch(upSertTokenError(err));
      });
  }
);

export const saveTokenToRedux = idToken =>
  async (dispatch) => {
    dispatch({
      type: ID_TOKEN_SAVE,
      idToken,
    });
  };

export const signInUser = ({ id_firebase, email, name, photo, id_facebook, first_name, last_name }, dispatch) => {
  const params = {
    id_facebook,
    id_firebase,
    email,
    first_name,
    last_name,
    photo,
  };

  console.log('params@signInUser', params);

  fetchTimeout(`${rootUrl}/users/signIn`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(params),
  }, timeout)
    .then((response) => {
      const { status } = response;
      console.log('response@signInUser', response);
      if (status !== 200) {
        console.log(`Response status: ${status}`);
        let err;
        switch (status) {
          case 401:
            err = 'Unauthorized';
            break;
          case 500:
            err = 'Internal Server Error';
            break;
          default:
            err = 'Unexpected';
        }
        throw new Error(err);
      } else {
        return response.json();
      }
    })
    .then((json) => {
      console.log('response.json@signInUser', json);
      if (json.success) {
        // const user = json.data;
        // sessionGradientActions.startNearestAirportFetch();
        dispatch(loginUserSuccess(dispatch, params));
      } else {
        dispatch(loginUserFail(dispatch));
      }
    });
};


const loginUserFail = (dispatch) => {
  dispatch({ type: LOGIN_USER_FAIL });
};

/**
 * Title: Facebook auth flow
 * Author: Alberto Schiabel
 * refactored by: Guilherme Borges Bastos -> Added Slim Framework and Firebase Signin and Auth
 */

const loginFacebookFail = error => ({
  type: LOGIN_FACEBOOK_FAIL,
  error,
});

/*
const facebookGraphRequestCallback = (error, result) => (
  (dispatch) => {
    if (error) {
      console.log('error@facebookGraphRequestCallback', error);
      dispatch(loginFacebookFail(error));
    } else {
      console.log('result@facebookGraphRequestCallback', result);
      loginFacebookSuccess(dispatch, result);
    }
  }
);
*/

export const loginUserViaFacebook = () => (
  async (dispatch) => {
    dispatch({ type: LOGIN_FACEBOOK_LOADING });
    console.log('loginUserViaFacebook dispatch', dispatch);
    LoginManager.logInWithReadPermissions(FACEBOOK_PERMISSIONS).then(
      (result) => {
        console.log('result 133:', result);
        if (result.isCancelled) {
          console.log('result isCancelled');
          dispatch(loginFacebookFail('User cancelled login'));
        } else {
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
              // Sign in with the credential from the Facebook user.
              console.log('Sign in with the credential from the Facebook user', credential),
              firebase.auth().signInWithCredential(credential).catch((error) => {
                // Handle Errors here.
                const {
                  // code,
                  message,
                  // email, // The email of the user's account used.
                } = error;
                loginFacebookFail(message);
              }).then(
                (result) => { // successfully added on Firebase
                  console.log('successfully added on Firebase', result);

                  const {
                    email,
                    photoURL: photo,
                    uid: id_firebase,
                    displayName: name,
                  } = result;

                  const id_facebook = data.userID;

                  const first_name = name.split(' ').slice(0, -1).join(' ');
                  const last_name = name.split(' ').slice(-1).join(' ');

                  signInUser({
                    id_firebase,
                    email,
                    name,
                    photo,
                    id_facebook,
                    first_name,
                    last_name,
                  }, dispatch);
                });
            });
        }
      },
      (error) => {
        console.log('handleError@loginUserViaFacebook', error);
        handleError(error);
      },
    );
  }
);

/*
const loginFacebookSuccess = (dispatch, facebookData) => {
  console.log('dispatch@loginFacebookSuccess 173', dispatch);
  console.log('facebookData@loginFacebookSuccess 174', facebookData);
  AsyncStorage.setItem('email', facebookData.email);
  AsyncStorage.setItem('gender', facebookData.gender);
  AsyncStorage.setItem('fb_id', facebookData.id);
  AsyncStorage.setItem('fb_locale', facebookData.locale);
  AsyncStorage.setItem('fb_location', facebookData.location.name);
  AsyncStorage.setItem('fb_name', facebookData.name);

  dispatch({
    type: LOGIN_FACEBOOK_SUCCESS,
    data: facebookData,
  });
};
*/

// End Facebook stuff

const loginUserSuccess = (dispatch, user) => {
  const login_user_data = JSON.stringify(user);
  console.log('login_user_data@loginUserSuccess', login_user_data);
  /*
  {
    id_facebook,
    id_firebase,
    email,
    first_name,
    last_name,
    photo,
  }
   */
  AsyncStorage.setItem('login_user_data', login_user_data);

  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user,
  });

  /**
   * Here we should check if the logged user is a returning one.
   * If yes -> call Actions.main();
   * else -> call Actions.sessionGradient();
   */

  Actions.sessionGradient();
};
