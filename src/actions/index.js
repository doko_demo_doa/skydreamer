// /-----------------------------------------------------------------
// /   Class:          index.js
// /   Description:    Merge all actions to export like a package
// /   Author:         Guilherme Borges Bastos       Date: 11/02/2017
// /   Notes:
// /   Revision History:
// /   Name:               Date:           Description:
// /   Guilherme Bastos    27/02/2017      Added FriendActions
// /   Guilherme Bastos    07/03/2017      Added ChatActions
// /   Alberto Schiabel    09/03/2017      Added FacebookApiActions
// /   Christian Tucker    23/04/2017      Added ChannelActions
// /-----------------------------------------------------------------

import * as facebookApiActions from './FacebookApiActions';
import * as authActions from './AuthActions';
import * as friendActions from './FriendActions';
import * as channelActions from './ChannelActions';
import * as offlineActions from './OfflineActions';
import * as sessionGradientActions from './SessionGradientActions';
import * as swipeCardActions from './SwipeCardActions';
import * as groupActions from './GroupActions';
import * as galleryActions from './GalleryActions';
// ---

export * from './AuthActions';
export * from './FriendActions';
export * from './ChannelActions';
export {
  facebookApiActions,
  authActions,
  friendActions,
  channelActions,
  offlineActions,
  sessionGradientActions,
  swipeCardActions,
  groupActions,
  galleryActions,
};
