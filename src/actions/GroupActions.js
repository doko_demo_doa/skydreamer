// /-----------------------------------------------------------------
// /   Class:          GroupActions.js
// /   Description:    Export all actions about Group reducer
// /   Author:         Alberto Schiabel (jkomyno)      Date: 19/05/2017
// /   Notes:
// /   Revision History:
// /   Name:               Date:           Description:
// /-----------------------------------------------------------------
import {
  fetchTimeout,
  formatToUrlEncoded,
} from 'skydreamer/utils';
import { config } from 'skydreamer/config';
import {
  WAITING_GET_USER_GROUPS_LIST_RESPONSE,
  RESPONSE_GET_USER_GROUPS_LIST,
  ERROR_GET_USER_GROUPS_LIST,
} from './types';
const {
  timeout,
  rootUrl,
} = config.api;

/**
 * Get User's Groups List
 */
const setUserGroupsListWaiting = () => ({
  type: WAITING_GET_USER_GROUPS_LIST_RESPONSE,
});

const onUserGroupsListSuccess = data => ({
  type: RESPONSE_GET_USER_GROUPS_LIST,
  data,
});

const onUserGroupsListFailure = error => ({
  type: ERROR_GET_USER_GROUPS_LIST,
  error,
});

// Method to find the user's groups list
export const getUserGroupsListFetch = () =>
  async (dispatch, getState) => {
    dispatch(setUserGroupsListWaiting());

    const { idToken } = getState().auth;
    console.log('idToken from getState', idToken);
    console.log('getState()', getState());

    const params = {};

    fetchTimeout(`${rootUrl}/users/groups`, {
      method: 'POST',
      headers: {
        Authorization: idToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    }, timeout)
    .then((response) => {
      const { status } = response;
      if (status !== 200) {
        console.log(`Response status: ${status}`);
        let err;
        switch (status) {
          case 401:
            err = 'Unauthorized';
            break;
          case 500:
            err = 'Internal Server Error';
            break;
          default:
            err = 'Unexpected';
        }
        throw new Error(err);
      } else {
        return response.json();
      }
    })
    .then((json) => {
      console.log('getUserGroupsListFetch json', json);
      if (json.success) {
        const data = json.data;
        dispatch(onUserGroupsListSuccess(data));
      } else {
        dispatch(onUserGroupsListFailure('Unexpected response while retrieving the cards'));
      }
    })
    .catch((error) => {
      console.error('getUserGroupsListFetch error', error);
      dispatch(onUserGroupsListFailure(error));
    });
  };
