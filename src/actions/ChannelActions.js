import firebase from 'skydreamer/firebase';
import {
    SELECT_CHANNEL,
    SET_CHANNEL,
    REMOVE_CHANNEL,
    SET_CHANNEL_SYNC_COMPLETE,
    SET_CHANNEL_CURSOR,
    NEW_MESSAGE_GROUP,
    NEW_MESSAGE,
    FETCH_MESSAGES_START,
    FETCH_MESSAGES_END,
} from 'skydreamer/actions/types';
import { Actions } from 'react-native-router-flux';

// A collection of firebase listeners used to prevent multiple instantiation.
const channelListeners = {};

export const syncChannels = () => (dispatch) => {
  const { uid } = firebase.auth().currentUser;
  if (!uid) return;

    // Create a database reference to the user's channel list
  const ref = firebase.database().ref(`user-channels/${uid}`);

    // Listen for child_added events.
  ref.on('child_added', (snapshot) => {
        // Get the channel key from the snapshot.
    const channelKey = snapshot.val();

        // Obtain the channel listener from the cache.
    const listener = channelListeners[channelKey];

        // If the listener does not exist, create one.
    if (!listener) {
      const channelRef = firebase.database().ref(`channels/${channelKey}`);
      channelListeners[channelKey] = channelRef.on('value', (snapshot) => {
        const channel = snapshot.val();
        console.log('channel', channel);
        if (channel && channel.type === 'single') {
          for (key in channel.members) {
            if (key === uid) continue;
            syncFriendDetails(key, channel)
                            .then((channel) => {
                              dispatch({ type: SET_CHANNEL, key: snapshot.key, channel });
                            });
          }
        } else {
          dispatch({ type: SET_CHANNEL, key: snapshot.key, channel: snapshot.val() });
        }
      });
    }
  });

    // Listen for child_removed events.
  ref.on('child_removed', (snapshot) => {
        // Obtain the listener for the removed item from the cache.
    const listener = channelListeners[snapshot.key];

        // if the item existed in the cache, ubsubscribe from the listener.
    if (listner) {
      listener();
      delete channelListeners[snapshot.key];
    }

        // Inform redux that the channel is gone.
    dispatch({ type: REMOVE_CHANNEL, key: snapshot.key });
  });
};

// A collection of firebase listeners used to prevent multiple instantiation
const userMessageListeners = {};

import * as webfirebase from 'firebase';

const config = {
  apiKey: 'AIzaSyDcz_GnSEJzuW915_N9ibq2sTObuOF1b1g',
  authDomain: 'skydreamer-6cb0d.firebaseapp.com',
  databaseURL: 'https://skydreamer-6cb0d.firebaseio.com',
  projectId: 'skydreamer-6cb0d',
  storageBucket: 'skydreamer-6cb0d.appspot.com',
  messagingSenderId: '321890545274',
};

webfirebase.initializeApp(config);

export const fetchMessagesFirebase = (channel, cursor, amount = 21) => (dispatch) => {
        // Update the state so we can't issue another request until this one is complete.
  dispatch({ type: FETCH_MESSAGES_START, key: channel });

        // Create a database reference to the channel's message group.
  let ref = firebase.database().ref(`user-messages/${channel}`);

        // Firebase native keys are generated with a built-in timestamp,
        // So sorting the keys sorts the messages by date.
  ref = ref.orderByKey(); // .orderByChild('orderValue');

        // Get the amount of messages from the database we want, plus one for
        // the new cursor.
  ref = ref.limitToLast(amount);

  if (cursor) {
    console.warn(`::pagination-debug:: Cursor used: ${cursor}`);
    ref = ref.endAt(cursor);
  } else {
    console.warn('::pagination-debug:: No cursor used. ');
  }

        // Keep the ref synced for the call to once.
  ref.keepSynced(true);

        // Obtain the messages from the database based on the generated query.
  ref.once('value').then((groupSnapshot) => {
    if (groupSnapshot.val() == null) {
      console.warn('::pagination-debug:: The snapshot was null');
                // Update the state so we can issue another request.
      return dispatch({ type: FETCH_MESSAGES_END, key: channel });
    }

    const messages = groupSnapshot.val();
    const keys = Object.keys(messages).sort((a, b) => +a >= +b);
            // const keys Object.keys(messages.sort((a, b) => messages[a].timestamp > messages[b].toimestamp))

            // const newCursor = keys[0];
    const newCursor = keys[keys.length - 1];
    console.warn(`::pagination-debug:: The new cursor is: ${newCursor}`);

    if (newCursor.length > 1) {
      delete messages[newCursor];
    }

    const isFullyLoaded = keys.length !== (amount);

    console.warn('Is fully loaded:', isFullyLoaded);

    if (isFullyLoaded) {
                // Inform redux that the channel is fully loaded so the load component
                // can be hidden from the <FlatList />
      dispatch({ type: SET_CHANNEL_SYNC_COMPLETE, key: channel });
    } else {
                // Set the cursor for the provided channel so we can fetch older messages.
      dispatch({ type: SET_CHANNEL_CURSOR, key: channel, cursor: newCursor });
    }

            // Update the state with the newly downloaded messages.
    dispatch({ type: NEW_MESSAGE_GROUP, key: channel, messages });

            // Update the state so we can issue another request.
    dispatch({ type: FETCH_MESSAGES_END, key: channel });
  });

        // Get the message listener from the cache
  const listener = userMessageListeners[channel];

  if (!listener) {
            // Redeclare the ref as a sanity check.
    ref = firebase.database().ref(`user-messages/${channel}`);

            // Order the keys so we can get the latest message
    ref = ref.orderByKey();

            // Set the reference cursor the the most recent message.
    ref = ref.limitToLast(1);

            // Keep the ref synced for the call to once.
    ref.keepSynced(true);

            // A hack required to ignore the most recent message when the handler
            // is initialized
    let hack = true;

    userMessageListeners[channel] = ref.on('child_added', (snapshot) => {
                // Ignore the first child_added event.
      if (hack) { return hack = false; }

                // Apply the key to the message object for duplication sanity.
      const message = snapshot.val();
      message.key = snapshot.key;

                // Send the received message to the reducer.
      dispatch({ type: NEW_MESSAGE, key: channel, message });
    });
  }
};


export const createMessageFirebase = (channel, type, value) => {
  const { uid } = firebase.auth().currentUser;
  if (!uid) return;

  console.log('type@createMessageFirebase', type);

  firebase.database().ref(`/user-messages/${channel}`).push({
    timestamp: { '.sv': 'timestamp' }, // Firebase generated timestamp, never use Date.now()
    userid: uid,
    channel,
    objData: {
      type,
      value,
    },
  });
};

export const openChat = (id, fullname, folder, photo, lastLogin) => {
  Actions.internalChat({ id, fullname, folder, photo, lastLogin });
  return (dispatch) => {
    dispatch({ type: SELECT_CHANNEL, channel: id });
  };
};

// Keeps friend information up to date and synced to the offline database.
const syncFriendDetails = (memberKey, channelData) => new Promise((resolve, reject) => {
  firebase.database()
        .ref(`/users/${memberKey}`)
        .orderByKey()
        .on('value', (snapshot) => {
          if (!snapshot.val()) return reject(`User snapshot for id(${memberKey}) was null`);
          const friendInfo = snapshot.val();
          channelData.first_name = friendInfo.first_name;
          channelData.last_name = friendInfo.last_name;
          channelData.photo = `${memberKey}.jpg`;
          return resolve(channelData);
        });
});
