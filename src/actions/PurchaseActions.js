import {
  fetchTimeout,
  formatToUrlEncoded,
  handleResponse,
} from 'skydreamer/utils';
import { config } from 'skydreamer/config';
import {
  WAITING_GET_GROUPON_DEALS_RESPONSE,
  RESPONSE_GROUPON_DEALS_CARDS,
  ERROR_GROUPON_DEALS_CARDS,
  WAITING_GET_PAYMENT_INTENT_RESPONSE,
  RESPONSE_PAYMENT_INTENT_CARDS,
  ERROR_PAYMENT_INTENT_CARDS,
} from './types';

const {
  timeout,
  rootUrl,
} = config.api;

/**
 * Get groupon deals
 */
const setGetGrouponDealsWaiting = () => ({
  type: WAITING_GET_GROUPON_DEALS_RESPONSE,
});

const onGetGrouponDealsSuccess = data => ({
  type: RESPONSE_GROUPON_DEALS_CARDS,
  data,
});

const onGetGrouponDealsFailure = error => ({
  type: ERROR_GROUPON_DEALS_CARDS,
  error,
});

// @TODO: update parameters to new version

/**
 * Method to get the deals using filters.
 * @param {String} arg.country_code country code for search
 * @param {String} arg.category category code for search
 * @param {float} arg.lat latitude for a geolocation query
 * @param {float} arg.lng longitude for a geolocation query
 * @param {String} arg.offset integer param the pagination system (>=0)
 * @param {String} arg.limit the integer number to limit the number of deals
 */
export const startGetGrouponDealsFetch = ({
  country_code,
  category,
  lat,
  lng,
  offset,
  limit,
}) =>
  async (dispatch, getState) => {
    dispatch(setGetGrouponDealsWaiting());

    const { idToken } = getState().auth;
    console.log('idToken from getState', idToken);
    console.log('getState()', getState());

    const params = {
      api_uuid,
      travel_id,
      category_id,
    };

    fetchTimeout(`${rootUrl}/groupon/get/deals`, {
      method: 'POST',
      headers: {
        Authorization: idToken,
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatToUrlEncoded(params),
    }, timeout)
    .then(handleResponse)
    .then((json) => {
      console.log('startGetGrouponDealsesFetch json', json);
      if (json.success) {
        const data = json.data;
        dispatch(onGetGrouponDealsSuccess(data));
      } else {
        dispatch(onGetGrouponDealsFailure('Unexpected response while retrieving the matches'));
      }
    })
    .catch((error) => {
      console.error('startGetGrouponDealsesFetch error', error);
      dispatch(onGetGrouponDealsFailure(error));
    });
  };

/**
 * Get payment intent
 */
const setGetPaymentIntentWaiting = () => ({
  type: WAITING_GET_PAYMENT_INTENT_RESPONSE,
});

const onGetPaymentIntentSuccess = data => ({
  type: RESPONSE_PAYMENT_INTENT_CARDS,
  data,
});

const onGetPaymentIntentFailure = error => ({
  type: ERROR_PAYMENT_INTENT_CARDS,
  error,
});

/**
 * Method to read the purchase intent.
 * @param {number} arg.purchase_id id of the purchase intent
 */
export const startGetPaymentIntentFetch = ({
  purchase_id,
}) =>
  async (dispatch, getState) => {
    dispatch(setGetPaymentIntentWaiting());

    const { idToken } = getState().auth;
    console.log('idToken from getState', idToken);
    console.log('getState()', getState());

    const params = {
      api_uuid,
      travel_id,
      category_id,
    };

    fetchTimeout(`${rootUrl}/payment/get`, {
      method: 'POST',
      headers: {
        Authorization: idToken,
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatToUrlEncoded(params),
    }, timeout)
    .then(handleResponse)
    .then((json) => {
      console.log('startGetPaymentIntentesFetch json', json);
      if (json.success) {
        const data = json.data;
        dispatch(onGetPaymentIntentSuccess(data));
      } else {
        dispatch(onGetPaymentIntentFailure('Unexpected response while retrieving the matches'));
      }
    })
    .catch((error) => {
      console.error('startGetPaymentIntentesFetch error', error);
      dispatch(onGetPaymentIntentFailure(error));
    });
  };

/**
 * Get User's Purchase List
 */
const setGetUserPurchaseWaiting = () => ({
  type: WAITING_GET_USER_PURCHASE_RESPONSE,
});

const onGetUserPurchaseSuccess = data => ({
  type: RESPONSE_USER_PURCHASE_CARDS,
  data,
});

const onGetUserPurchaseFailure = error => ({
  type: ERROR_USER_PURCHASE_CARDS,
  error,
});

/**
 * Method to read all the purchases intent of the user.
 */
export const startGetUserPurchaseFetch = () =>
  async (dispatch, getState) => {
    dispatch(setGetUserPurchaseWaiting());

    const { idToken } = getState().auth;
    console.log('idToken from getState', idToken);
    console.log('getState()', getState());

    fetchTimeout(`${rootUrl}/payment/getUserPurchases`, {
      method: 'GET',
      headers: {
        Authorization: idToken,
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    }, timeout)
    .then(handleResponse)
    .then((json) => {
      console.log('startGetUserPurchaseesFetch json', json);
      if (json.success) {
        dispatch(onGetUserPurchaseSuccess(json.data));
      } else {
        dispatch(onGetUserPurchaseFailure('Unexpected response while retrieving the matches'));
      }
    })
    .catch((error) => {
      console.error('startGetUserPurchaseesFetch error', error);
      dispatch(onGetUserPurchaseFailure(error));
    });
  };
