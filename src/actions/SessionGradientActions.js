import {
  fetchTimeout,
  formatToUrlEncoded,
} from 'skydreamer/utils';
import { config } from 'skydreamer/config';
import {
  WAITING_NEAREST_AIRPORT_RESPONSE,
  RESPONSE_NEAREST_AIRPORT,
  ERROR_NEAREST_AIRPORT,

  WAITING_TRAVEL_INTENT_RESPONSE,
  RESPONSE_TRAVEL_INTENT,
  ERROR_TRAVEL_INTENT,
} from './types';

const {
  timeout,
  rootUrl,
  token,
} = config.api;

/**
 * Nearest airport
 */
export const setNearestAirportWaiting = () => ({
  type: WAITING_NEAREST_AIRPORT_RESPONSE,
});

export const onNearestAirportSuccess = data => ({
  type: RESPONSE_NEAREST_AIRPORT,
  data,
});

export const onNearestAirportFailure = error => ({
  type: ERROR_NEAREST_AIRPORT,
  error,
});

export const startNearestAirportFetch = () =>
  async (dispatch) => {
    dispatch(setNearestAirportWaiting());
    const params = {
      latitude: 45.4627124,
      longitude: 9.1076929,
      range: 75,
      limit: 5,
    };

    fetchTimeout(`${rootUrl}/airport/get/nearest`, {
      method: 'POST',
      headers: {
        Authorization: token,
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatToUrlEncoded(params),
    }, timeout)
    .then((response) => {
      const { status } = response;
      if (status !== 200) {
        console.log(`Response status: ${status}`);
        let err;
        switch (status) {
          case 401:
            err = 'Unauthorized';
            break;
          case 500:
            err = 'Internal Server Error';
            break;
          default:
            err = 'Unexpected';
        }
        throw new Error(err);
      } else {
        return response.json();
      }
    })
    .then((json) => {
      console.log('startNearestAirportFetch json', json);
      if (json.success) {
        const data = json.data.sort((a, b) => (a.distance_km > b.distance_km));
        data.map((item) => {
          const {
            id,
            name,
          } = item;
          item.label = `${id} - ${name}`;
          return item;
        });
        dispatch(onNearestAirportSuccess(data));
      } else {
        dispatch(onNearestAirportFailure(json.message));
      }
    })
    .catch((error) => {
      console.warn('startNearestAirportFetch error', error);
      onNearestAirportFailure(error);
    });
  };

/**
 * Travel intent
 */

export const setTravelIntentWaiting = () => ({
  type: WAITING_TRAVEL_INTENT_RESPONSE,
});

export const onTravelIntentSuccess = data => ({
  type: RESPONSE_TRAVEL_INTENT,
  data,
});

export const onTravelIntentFailure = error => ({
  type: ERROR_TRAVEL_INTENT,
  error,
});

export const startTravelIntentFetch = () =>
  (dispatch, getState) => {
    dispatch(setTravelIntentWaiting());

    const {
      group_name,
      is_group_travel,
      price_amount,
      departure_day,
      departure_month,
      departure_airport_id,
      interested_topic_ids_arr,
    } = getState().offline.sessionGradient;

    fetchTimeout(`${rootUrl}/travel/create`, {
      method: 'POST',
      headers: {
        Authorization: token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        group_name,
        is_group_travel,
        price_amount,
        departure_day,
        departure_month,
        departure_airport_id,
        interested_topic_ids_arr,
      }),
    }, timeout)
    .then((response) => {
      const { status } = response;
      if (status !== 200) {
        console.log(`Response status: ${status}`);
        let err;
        switch (status) {
          case 401:
            err = 'Unauthorized';
            break;
          case 500:
            err = 'Internal Server Error';
            break;
          default:
            err = 'Unexpected';
        }
        throw new Error(err);
      } else {
        return response.json();
      }
    })
    .then((json) => {
      console.log('startTravelIntentFetch json', json);
      dispatch(onTravelIntentSuccess(json));
    })
    .catch((error) => {
      console.log('startTravelIntentFetch error', error);
      onTravelIntentFailure(error);
    });
  };
