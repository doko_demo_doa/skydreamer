import {
  fetchTimeout,
  formatToUrlEncoded,

  makeSimulatedNetworkRequest, // handle mockup requests
} from 'skydreamer/utils';
import { config } from 'skydreamer/config';
import {
  WAITING_GET_GALLERY_PANORAMA_POSTS_RESPONSE,
  RESPONSE_GET_GALLERY_PANORAMA_POSTS,
  ERROR_GET_GALLERY_PANORAMA_POSTS,
} from './types';
const {
  timeout,
  rootUrl,
} = config.api;

// https://api.skydreamer.io/groupon/get/deals

/**
 * We should decide if we need to return an object with some additional parameter with
 * "error check" properties, but here's what I need in a nutshell atm.
 * Surely we should add some particular links regarding Groupon, but atm I'm not sure
 * what exactly we should show to the user.
 */
const mockupState = {
  panoramaResponse: {
    success: true,
    offset: 1, // check parameter
    data: [
      {
        title: 'Addadura Beach',
        price: 15,
        uris: ['https://r.bstatic.com/images/hotel/max1024x768/793/79344997.jpg'],
        description: 'The Addaura beach, which takes its name from the dialect of "bay", is located along the northern side of Mount Pellegrino, besides Punta Priola. A lovely beach which looks toward the beginning of the Mondello Gulf. Beautiful beach resorts which has a lot of huge villas.',
      }, {
        title: 'Vondelpark',
        price: 0,
        uris: ['http://www.amsterdam-travel-guide.net/images/attractions/vondelpark-amsterdam-cyclists.jpg'],
        description: 'Vondelpark can be quietly defined as the green heart of Amsterdam! Designed in 1864 by the architect L.D. Zocher on behalf of an association founded by wealthy citizens with the aim of creating a green area that was fun for the many workers in that area (at times it was on the outskirts of Amsterdam, while it is now in Full center right near Leidseplein) and as a park where you can walk on horseback, the "New Park" (later renamed Vondelpark in the following year, when the statue depicting the eminent poet and writer Joost van den Vondel) popular.',
      }, {
        title: 'Dam Square',
        price: 36,
        uris: ['https://www.viviamsterdam.it/images/viviamsterdam/Attrazioni/obelisco-piazza-dam.JPG'],
        description: 'The streets around Dam Square are packed with quaint shops where you can do some healthy "wild shopping" and buy something extremely Dutch. There is also De Bijenkorf, Amsterdam\'s most luxurious shopping center, housed in a Gothic palace. Instead, for the "coffee break", we find Abraxas, with hand-drawn interiors, and the Green House Centrum, the Cannabis Cup winner.',
      },
    ],
  },
};

/**
 * Get data to populate PanoramaList in gallery
 */
const setGetGalleryPanoramaPostsWaiting = () => ({
  type: WAITING_GET_GALLERY_PANORAMA_POSTS_RESPONSE,
});

const onGetGalleryPanoramaPostsSuccess = data => ({
  type: RESPONSE_GET_GALLERY_PANORAMA_POSTS,
  data,
});

const onGetGalleryPanoramaPostsFailure = error => ({
  type: ERROR_GET_GALLERY_PANORAMA_POSTS,
  error,
});


/**
 * MOCKUP
 * Fetch Panorama's list
 */
const _fetchGetGalleryPanoramaPosts = () => makeSimulatedNetworkRequest((resolve, reject) => {
  resolve(mockupState.panoramaResponse);
});


/**
 * Method to get a list of posts regarding panorams to populate the gallery section
 *
 * Given the fact that I should send you the card_id, I guess this should be a POST call.
 * Basically, what I would do is taking advantage of the callback event that I access when
 * I reach the end of the vertical list, thus making it easy to handle some sort of pagination
 * and loading new posts in an incremental way.
 *
 * @param {String} arg.card_id id of the selected card
 * @param {number} arg.number number of posts that I expect to receive as response
 * @param {number} arg.offset value that is added incrementally by the frontend,
 * to ask for new posts everytime. However, I'm not completely sure this is the best way to
 * achiave it. I would like to have the same behaviour that Facebook and Instagram have:
 * load more posts when you reach the end of the previously loaded list.
 */
export const startGetGalleryPanoramaPostsFetch = ({
  card_id,
  number,
  offset,
}) =>
  async (dispatch, getState) => {
    dispatch(setGetGalleryPanoramaPostsWaiting());

    const { idToken } = getState().auth;
    console.log('idToken from getState', idToken);
    console.log('getState()', getState());

    const params = {
      card_id,
      number,
      offset,
    };

    _fetchGetGalleryPanoramaPosts()
    /*
    fetchTimeout(`${rootUrl}/gallery/get/panoramaPosts/`, {
      method: 'POST',
      headers: {
        Authorization: idToken,
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatToUrlEncoded(params),
    }, timeout)
    .then((response) => {
      const { status } = response;
      if (status !== 200) {
        console.log(`Response status: ${status}`);
        let err;
        switch (status) {
          case 401:
            err = 'Unauthorized';
            break;
          case 500:
            err = 'Internal Server Error';
            break;
          default:
            err = 'Unexpected';
        }
        throw new Error(err);
      } else {
        return response.json();
      }
    })
    */
    .then((json) => {
      console.log('startGetGalleryPanoramaPostsFetch json', json);
      if (json.success) {
        const data = json.data;
        dispatch(onGetGalleryPanoramaPostsSuccess(data));
      } else {
        const error = 'Unexpected response while list of posts regarding panorams';
        dispatch(onGetGalleryPanoramaPostsFailure(error));
      }
    })
    .catch((error) => {
      console.error('startGetGalleryPanoramaPostsFetch error', error);
      dispatch(onGetGalleryPanoramaPostsFailure(error));
    });
  };
