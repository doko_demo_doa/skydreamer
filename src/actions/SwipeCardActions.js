import moment from 'moment';
import {
  fetchTimeout,
  formatToUrlEncoded,
} from 'skydreamer/utils';
import { config } from 'skydreamer/config';
import {
  WAITING_GET_CARDS_RESPONSE,
  RESPONSE_GET_CARDS,
  ERROR_GET_CARDS,

  WAITING_GET_MATCH_RESPONSE,
  RESPONSE_MATCH_CARDS,
  ERROR_MATCH_CARDS,
} from './types';

const {
  timeout,
  rootUrl,
} = config.api;

/**
 * Get match
 */
const setGetMatchWaiting = () => ({
  type: WAITING_GET_MATCH_RESPONSE,
});

const onGetMatchSuccess = data => ({
  type: RESPONSE_MATCH_CARDS,
  data,
});

const onGetMatchFailure = error => ({
  type: ERROR_MATCH_CARDS,
  error,
});

/**
 * @param {String} arg.api_uuid unique identifier of the suggestion
 * @param {String} arg.travel_id id of the travel
 * @param {String} arg.category_id id of category
 */
export const startGetMatchesFetch = ({
  api_uuid,
  travel_id,
  category_id,
}) =>
  async (dispatch, getState) => {
    dispatch(setGetMatchWaiting());

    const { idToken } = getState().auth;
    console.log('idToken from getState', idToken);
    console.log('getState()', getState());

    const params = {
      api_uuid,
      travel_id,
      category_id,
    };

    fetchTimeout(`${rootUrl}/matches/get`, {
      method: 'POST',
      headers: {
        Authorization: idToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    }, timeout)
    .then((response) => {
      const { status } = response;
      if (status !== 200) {
        console.log(`Response status: ${status}`);
        let err;
        switch (status) {
          case 401:
            err = 'Unauthorized';
            break;
          case 500:
            err = 'Internal Server Error';
            break;
          default:
            err = 'Unexpected';
        }
        throw new Error(err);
      } else {
        return response.json();
      }
    })
    .then((json) => {
      console.log('startGetMatchesFetch json', json);
      if (json.success) {
        const data = json.data;
        dispatch(onGetMatchSuccess(data));
      } else {
        dispatch(onGetMatchFailure('Unexpected response while retrieving the matches'));
      }
    })
    .catch((error) => {
      console.error('startGetMatchesFetch error', error);
      dispatch(onGetMatchFailure(error));
    });
  };

/**
 * Get cards
 */
const setGetCardsWaiting = () => ({
  type: WAITING_GET_CARDS_RESPONSE,
});

const onGetCardsSuccess = data => ({
  type: RESPONSE_GET_CARDS,
  data,
});

const onGetCardsFailure = error => ({
  type: ERROR_GET_CARDS,
  error,
});

export const startCardsFetch = ({
  country,
  origin,
  destination,
  outbound_date,
  currency,
  budget,
  travel_id,
}) =>
  async (dispatch, getState) => {
    dispatch(setGetCardsWaiting());
    /*
    country: 'it',
    origin: 'BLQ'
    destination: 'AMS'
    outbound_date: '2017-06'
    currency: 'EUR'
    budget: 300
    travel_id: 5
    */

    // response: uuid: '84cdcd328abd0ca6d3897b754d428441'

    /*
    const idToken = await AsyncStorage.getItem('idToken');
    console.log('idToken@startCardsFetch', idToken);
    */

    const { idToken } = getState().auth;
    console.log('idToken from getState', idToken);
    console.log('getState()', getState());

    const params = {
      country,
      origin,
      destination,
      outbound_date: moment(outbound_date).format('YYYY-MM'),
      currency,
      budget,
      travel_id,
    };

    fetchTimeout(`${rootUrl}/cards/get`, {
      method: 'POST',
      headers: {
        Authorization: idToken,
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatToUrlEncoded(params),
    }, timeout)
    .then((response) => {
      const { status } = response;
      if (status !== 200) {
        console.log(`Response status: ${status}`);
        let err;
        switch (status) {
          case 401:
            err = 'Unauthorized';
            break;
          case 500:
            err = 'Internal Server Error';
            break;
          default:
            err = 'Unexpected';
        }
        throw new Error(err);
      } else {
        return response.json();
      }
    })
    .then((json) => {
      console.log('startGetCards json', json);
      if (json.success) {
        const data = json.data;
        dispatch(onGetCardsSuccess(data));
      } else {
        dispatch(onGetCardsFailure('Unexpected response while retrieving the cards'));
      }
    })
    .catch((error) => {
      console.error('startGetCards error', error);
      dispatch(onGetCardsFailure(error));
    });
  };

/**
 * Search airport by city
 */
const setGetAirportsByCityWaiting = () => ({
  type: WAITING_GET_AIRPORTS_BY_CITY_RESPONSE,
});

const onGetAirportsByCitySuccess = data => ({
  type: RESPONSE_GET_AIRPORTS_BY_CITY,
  data,
});

const onGetAirportsByCityFailure = error => ({
  type: ERROR_GET_AIRPORTS_BY_CITY,
  error,
});

/**
 * Method to get a airport list from a specific city, using our database 'airport' records.
 * Use the inner city database.
 * @param {String} arg.city name of the city
 */
export const startGetAirportsByCityesFetch = ({
  city,
}) =>
  async (dispatch, getState) => {
    dispatch(setGetAirportsByCityWaiting());

    const { idToken } = getState().auth;
    console.log('idToken from getState', idToken);
    console.log('getState()', getState());

    const params = {
      city,
    };

    fetchTimeout(`${rootUrl}/airport/search/byCity`, {
      method: 'POST',
      headers: {
        Authorization: idToken,
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatToUrlEncoded(params),
    }, timeout)
    .then((response) => {
      const { status } = response;
      if (status !== 200) {
        console.log(`Response status: ${status}`);
        let err;
        switch (status) {
          case 401:
            err = 'Unauthorized';
            break;
          case 500:
            err = 'Internal Server Error';
            break;
          default:
            err = 'Unexpected';
        }
        throw new Error(err);
      } else {
        return response.json();
      }
    })
    .then((json) => {
      console.log('startGetAirportsByCityesFetch json', json);
      if (json.success) {
        const data = json.data;
        dispatch(onGetAirportsByCitySuccess(data));
      } else {
        const error = 'Unexpected response while getting airports by city name';
        dispatch(onGetAirportsByCityFailure(error));
      }
    })
    .catch((error) => {
      console.error('startGetAirportsByCityesFetch error', error);
      dispatch(onGetAirportsByCityFailure(error));
    });
  };
